/*
 * Copyright Camunda Services GmbH and/or licensed to Camunda Services GmbH
 * under one or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information regarding copyright
 * ownership. Camunda licenses this file to you under the Apache License,
 * Version 3.0; you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fi.jyu.vasara;

import static org.camunda.bpm.engine.authorization.Authorization.ANY;
import static org.camunda.bpm.engine.authorization.Authorization.AUTH_TYPE_GLOBAL;
import static org.camunda.bpm.engine.authorization.Permissions.READ;
import static org.camunda.bpm.engine.authorization.Resources.GROUP;
import static org.camunda.bpm.engine.authorization.Resources.USER;

import org.camunda.bpm.engine.impl.persistence.entity.AuthorizationEntity;
import org.camunda.bpm.engine.AuthorizationService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.camunda.bpm.spring.boot.starter.event.PostDeployEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.event.EventListener;

@SpringBootApplication(scanBasePackages = {
        "org.camunda.bpm.extension.graphql",
        "fi.jyu.vasara",
})
@EnableProcessApplication
public class CamundaApplication {

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private AuthorizationService authorizationService;

    public static void main(String... args) {
        SpringApplication.run(CamundaApplication.class, args);
    }

    @EventListener
    private void processPostDeploy(PostDeployEvent event) {
        if (authorizationService
                .createAuthorizationQuery()
                .authorizationType(AUTH_TYPE_GLOBAL)
                .resourceType(GROUP)
                .resourceId(ANY).count() == 0) {
            AuthorizationEntity groupsAuth = new AuthorizationEntity(AUTH_TYPE_GLOBAL);
            groupsAuth.setResource(GROUP);
            groupsAuth.addPermission(READ);
            groupsAuth.setResourceId(ANY);
            authorizationService.saveAuthorization(groupsAuth);
        }

        if (authorizationService
                .createAuthorizationQuery()
                .authorizationType(AUTH_TYPE_GLOBAL)
                .resourceType(USER)
                .resourceId(ANY).count() == 0) {
            AuthorizationEntity usersAuth = new AuthorizationEntity(AUTH_TYPE_GLOBAL);
            usersAuth.setResource(USER);
            usersAuth.addPermission(READ);
            usersAuth.setResourceId(ANY);
            authorizationService.saveAuthorization(usersAuth);
        }

    }
}
