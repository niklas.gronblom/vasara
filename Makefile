.PHONY: all
all: up

.PHONY: clean
clean: purge

.PHONY: pull
pull:
	git pull

.PHONY: build
build: update
	docker-compose build

.PHONY: update
update: pull build

.PHONY: up
up:
	docker-compose up

.PHONY: down
down:
	docker-compose down

.PHONY: export
export: hasura-cli
ifndef SCHEMA
	$(error "Usage: make export SCHEMA=myname")
endif
	mkdir -p graphql-engine/drafts/$(SCHEMA)
	touch graphql-engine/drafts/$(SCHEMA)/config.yaml
	./hasura-cli migrate create "schema" --from-server \
	  --endpoint http://localhost:8900 --admin-secret admin \
	  --schema '"$(SCHEMA)"' --project "graphql-engine/drafts/$(SCHEMA)"

.PHONY: export
purge:
	docker-compose down
	docker volume rm vasara_db_data
	$(RM) ./hasura-cli

hasura-cli:
	@version=v1.2.0-beta.2; \
	if [[ "$$(uname)" == 'Linux' ]]; then \
	    platform='linux'; \
	else \
	    platform='darwin'; \
	fi; \
	if [[ "$$(uname -m)" == 'x86_64' ]]; then \
	    arch='amd64'; \
	else \
	    arch='386'; \
	fi; \
	suffix="-$${platform}-$${arch}"; \
	echo Downloading "https://github.com/hasura/graphql-engine/releases/download/$$version/cli-hasura$$suffix"; \
	curl -L "https://github.com/hasura/graphql-engine/releases/download/$$version/cli-hasura$$suffix" -o ./hasura-cli; \
	chmod u+x ./hasura-cli
