import React, { useState } from 'react';
import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import SlideshowIcon from '@material-ui/icons/Slideshow';

import Form from './Form';

const ModalStyle: React.CSSProperties = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  zIndex: 2, // To be able to see dropdown suggestions since RaAutocompleteSuggestionList-suggestionsContainer has z-index: 2
};

const PaperStyle: React.CSSProperties = {
  padding: '2rem 1rem',
  minWidth: '50%',
};

const Preview: React.FC = () => {
  const [openModal, setOpenModal] = useState(false);

  const handleOpen = () => {
    setOpenModal(true);
  };

  const handleClose = () => {
    setOpenModal(false);
  };

  return (
    <>
      <Button onClick={handleOpen} startIcon={<SlideshowIcon />}>
        Preview
      </Button>
      <Modal style={ModalStyle} open={openModal} onClose={handleClose}>
        <Paper style={PaperStyle} elevation={3}>
          <Form />
        </Paper>
      </Modal>
    </>
  );
};

export default Preview;
