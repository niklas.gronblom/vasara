// Related: https://docs.camunda.org/manual/latest/user-guide/process-engine/variables/#supported-variable-values

export const InputFieldTypes = {
  ARRAY: 'array',
  BOOLEAN: 'boolean',
  DATE: 'date',
  FILE: 'file',
  HELP: 'help',
  INTEGER: 'integer',
  SELECT: 'select',
  STRING: 'string',
};

export const DecorativeFieldTypes = {
  HELP: 'help',
};

export const FieldTypes = {
  ...InputFieldTypes,
  ...DecorativeFieldTypes,
};
