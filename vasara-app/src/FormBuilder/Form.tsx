import React from 'react';
import { FieldArray, useFieldArray } from 'react-final-form-arrays';
import { Field } from 'react-final-form';
import { FieldComponent } from './fields';

const fsStyle: React.CSSProperties = {
  padding: '1rem',
  border: '1px solid #eee',
};

const Form: React.FC = () => {
  const { fields: fieldsets } = useFieldArray('schema');

  return (
    <>
      {fieldsets.map(name => (
        <FieldArray key={name} name={`${name}.fields`}>
          {({ fields }) => (
            <div style={fsStyle}>
              {fields.map(name => (
                <Field key={name} name={name}>
                  {props => {
                    const FormComponent = FieldComponent[props.input.value.type];
                    return <FormComponent {...props} />;
                  }}
                </Field>
              ))}
            </div>
          )}
        </FieldArray>
      ))}
    </>
  );
};

export default Form;
