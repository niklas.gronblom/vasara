import { IndexableObject } from '../util/types';
import ArrayField from './Fields/ArrayField';
import { ArrayInput } from './Fields/ArrayField';
import BooleanField, { BooleanInput } from './Fields/BooleanField';
import DateField, { DateInput } from './Fields/DateField';
import FileField, { FileInput } from './Fields/FileField';
import HelpField, { HelpDisplay } from './Fields/HelpField';
import IntegerField, { IntegerInput } from './Fields/IntegerField';
import SelectField, { SelectInput } from './Fields/SelectField';
import StringField, { StringInput } from './Fields/StringField';
import { FieldTypes } from './constants';

export const EditableFieldComponent: IndexableObject = {};

EditableFieldComponent[FieldTypes.ARRAY] = ArrayField;
EditableFieldComponent[FieldTypes.BOOLEAN] = BooleanField;
EditableFieldComponent[FieldTypes.DATE] = DateField;
EditableFieldComponent[FieldTypes.FILE] = FileField;
EditableFieldComponent[FieldTypes.HELP] = HelpField;
EditableFieldComponent[FieldTypes.INTEGER] = IntegerField;
EditableFieldComponent[FieldTypes.SELECT] = SelectField;
EditableFieldComponent[FieldTypes.STRING] = StringField;

export const FieldDefaultProps: Record<string, any> = {};

FieldDefaultProps[FieldTypes.STRING] = {
  label: 'String field',
  sources: [],
  multiline: false,
  required: false,
  type: FieldTypes.STRING,
};

FieldDefaultProps[FieldTypes.INTEGER] = {
  label: 'Integer field',
  sources: [],
  required: false,
  type: FieldTypes.INTEGER,
};

FieldDefaultProps[FieldTypes.BOOLEAN] = {
  label: '',
  sources: [],
  required: false,
  type: FieldTypes.BOOLEAN,
};

FieldDefaultProps[FieldTypes.DATE] = {
  label: '',
  sources: [],
  required: false,
  type: FieldTypes.DATE,
};

FieldDefaultProps[FieldTypes.FILE] = {
  label: '',
  sources: [],
  required: false,
  type: FieldTypes.FILE,
};

FieldDefaultProps[FieldTypes.SELECT] = {
  label: '',
  sources: [],
  required: false,
  type: FieldTypes.SELECT,
};

FieldDefaultProps[FieldTypes.ARRAY] = {
  label: '',
  sources: [],
  required: false,
  type: FieldTypes.ARRAY,
};

FieldDefaultProps[FieldTypes.HELP] = {
  label: '',
  content: '',
  required: false,
  type: FieldTypes.HELP,
};

export const FieldComponent: IndexableObject = {};

FieldComponent[FieldTypes.ARRAY] = ArrayInput;
FieldComponent[FieldTypes.BOOLEAN] = BooleanInput;
FieldComponent[FieldTypes.DATE] = DateInput;
FieldComponent[FieldTypes.FILE] = FileInput;
FieldComponent[FieldTypes.HELP] = HelpDisplay;
FieldComponent[FieldTypes.INTEGER] = IntegerInput;
FieldComponent[FieldTypes.SELECT] = SelectInput;
FieldComponent[FieldTypes.STRING] = StringInput;
