export interface Choice {
  id: string;
  name: string;
}
