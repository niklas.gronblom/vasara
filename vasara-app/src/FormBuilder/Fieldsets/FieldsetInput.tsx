import React, { useState } from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { v4 as uuid } from 'uuid';

type Props = {
  fields: any;
};

const FieldsetInput: React.FC<Props> = props => {
  const [fieldsetName, setFieldsetName] = useState<string>('');

  const addNewFieldset = () => {
    if (fieldsetName !== '') {
      props.fields.push({ title: fieldsetName, fields: [], id: uuid() });
      setFieldsetName('');
    }
  };

  const handleNewFieldsetNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFieldsetName(event.target.value);
  };

  return (
    <FormControl fullWidth size="small" variant="outlined" style={{ marginBottom: '1rem' }}>
      <InputLabel>Add new Fieldset</InputLabel>
      <OutlinedInput
        type="text"
        value={fieldsetName}
        onChange={handleNewFieldsetNameChange}
        onKeyPress={e => e.key === 'Enter' && addNewFieldset()}
        labelWidth={130}
        endAdornment={
          <InputAdornment position="end">
            <IconButton aria-label="add fieldset" onClick={addNewFieldset} disabled={fieldsetName === ''}>
              <AddCircleIcon />
            </IconButton>
          </InputAdornment>
        }
      />
    </FormControl>
  );
};

export default FieldsetInput;
