import React, { ChangeEvent, useState } from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { v4 as uuid } from 'uuid';
import { FieldTypes } from '../constants';
import { useTranslate } from 'react-admin';
import { FieldDefaultProps } from '../fields';

type Props = {
  fields: any;
};

interface WidgetType {
  name?: string | undefined;
  value: unknown;
}

const EnabledFieldTypes = [
  FieldTypes.HELP,
  FieldTypes.STRING,
  FieldTypes.BOOLEAN,
  FieldTypes.INTEGER,
  FieldTypes.DATE,
  FieldTypes.FILE,
  FieldTypes.SELECT,
  // FieldTypes.ARRAY,
];

const FieldInput: React.FC<Props> = props => {
  const translate = useTranslate();
  const [widgetType, setWidgetType] = useState<WidgetType>({ value: '' });

  const handleChange = (event: ChangeEvent<WidgetType>) => {
    setWidgetType({ value: event.target.value });
  };

  const addNewWidget = () => {
    if (widgetType.value !== '') {
      props.fields.push({ id: uuid(), ...FieldDefaultProps[widgetType.value as string] });
      setWidgetType({ name: '', value: '' });
    }
  };

  return (
    <Grid container spacing={3} justify="flex-start" alignContent="center" alignItems="center">
      <Grid item xs={7} md={3}>
        <FormControl fullWidth variant="filled">
          <InputLabel htmlFor="none">Add widget</InputLabel>
          <Select native value={widgetType.name} onChange={handleChange} fullWidth={true}>
            <option aria-label="None" value="" />
            {EnabledFieldTypes.map(type => (
              <option key={type} value={type}>
                {translate(`ra.form.${type}`)}
              </option>
            ))}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={3}>
        <Button variant="contained" color="primary" startIcon={<AddCircleIcon />} onClick={addNewWidget}>
          Add
        </Button>
      </Grid>
    </Grid>
  );
};

export default FieldInput;
