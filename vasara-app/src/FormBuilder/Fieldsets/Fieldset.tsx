import React, { useCallback, useState } from 'react';

import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import DragIndicatorIcon from '@material-ui/icons/DragIndicator';
import Typography from '@material-ui/core/Typography';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import IconButton from '@material-ui/core/IconButton';

import { Choice } from '../types';
import {
  DragDropContext,
  Draggable,
  DraggableProvided,
  DraggableStateSnapshot,
  Droppable,
  DroppableStateSnapshot,
} from 'react-beautiful-dnd';
import { useFieldArray } from 'react-final-form-arrays';
import { Field } from 'react-final-form';
import { toLabel } from '../../util/helpers';
import { TextInput } from 'react-admin';
import Grid from '@material-ui/core/Grid';
import makeStyles from '@material-ui/styles/makeStyles/makeStyles';
import FieldInput from './FieldInput';
import DeleteIcon from '@material-ui/icons/DeleteForeverOutlined';
import { EditableFieldComponent } from '../fields';

interface Props {
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  fieldName: string;
  sourceChoices: Choice[];
}

const useAccordionStyles = makeStyles({
  root: {
    borderRadius: '4px !important',
    marginBottom: '5px',
  },
});

const useSummaryStylesCalm = makeStyles({
  root: {
    backgroundColor: '#e8e8e8 !important',
    borderRadius: '4px !important',
    filter: 'drop-shadow(0px 1px 1px grey)',
  },
});

const useSummaryStylesDragging = makeStyles({
  root: {
    backgroundColor: 'lightgreen !important',
    borderRadius: '4px !important',
    filter: 'drop-shadow(0px 1px 1px grey)',
  },
});

const useSummaryStylesOutside = makeStyles({
  root: {
    backgroundColor: '#red !important',
    borderRadius: '4px !important',
    filter: 'drop-shadow(0px 1px 1px grey)',
  },
});

const getFieldStyle = (snapshot: DraggableStateSnapshot, draggableStyles: any) => ({
  userSelect: 'none',
  background:
    !snapshot.isDragging && !snapshot.draggingOver
      ? 'inherit'
      : snapshot.isDragging && snapshot.draggingOver
      ? 'lightgreen'
      : 'red',
  ...draggableStyles,
});

const getFieldsDropStyle = (snapshot: DroppableStateSnapshot) => ({
  background: snapshot.isDraggingOver ? '#e7e7e7' : snapshot.draggingFromThisWith ? '#ecc0c0' : 'inherit',
  borderRadius: '4px',
  padding: '5px',
  minHeight: '50px',
  width: 'auto',
  marginBottom: '1rem',
});

const Fieldset: React.FC<Props> = ({ provided, snapshot, fieldName, sourceChoices }) => {
  const [expanded, setExpanded] = useState<boolean>(false);
  const { fields } = useFieldArray(`${fieldName}.fields`);
  const accordionStyles = useAccordionStyles();
  const summaryStylesCalm = useSummaryStylesCalm();
  const summaryStylesDragging = useSummaryStylesDragging();
  const summaryStylesOutside = useSummaryStylesOutside();

  const onDragEnd = useCallback(
    (result: any) => {
      if (!result.destination) {
        fields.remove(result.source.index);
      } else if (result.source.index === result.destination.index) {
        return;
      } else {
        fields.move(result.source.index, result.destination.index);
      }
    },
    [fields]
  );

  return (
    <Accordion
      ref={provided.innerRef}
      {...provided.draggableProps}
      expanded={expanded}
      onChange={() => setExpanded(!expanded)}
      classes={{ ...accordionStyles }}
      style={getFieldStyle(snapshot, provided.draggableProps.style)}
    >
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls={`${fieldName}-content`}
        classes={
          !snapshot.isDragging && !snapshot.draggingOver
            ? { ...summaryStylesCalm }
            : snapshot.isDragging && snapshot.draggingOver
            ? { ...summaryStylesDragging }
            : { ...summaryStylesOutside }
        }
      >
        <Typography>
          <IconButton
            {...provided.dragHandleProps}
            disableRipple={false}
            aria-label="move"
            onFocus={event => event.stopPropagation()}
            onMouseDown={event => event.stopPropagation()}
            onClick={event => event.stopPropagation()}
          >
            <DragIndicatorIcon />
          </IconButton>
          <strong>
            {snapshot.isDragging && !snapshot.draggingOver ? (
              <span style={{ display: 'inline-flex', verticalAlign: 'middle' }}>
                <DeleteIcon />
                &nbsp;
              </span>
            ) : (
              toLabel('Fieldset') + ': '
            )}
          </strong>
          <Field name={`${fieldName}`}>
            {({ input }) => (
              <>
                {input.value.title + ' '}
                {fields && <span style={{ fontSize: '65%' }}>({fields.length} fields)</span>}
              </>
            )}
          </Field>
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Grid container spacing={1} justify="center" alignItems="center">
          <Grid item xs={12}>
            <TextInput source={`${fieldName}.title`} label="Fieldset Title" fullWidth={true} placeholder="Set title" />
          </Grid>
          <Grid item xs={12}>
            <DragDropContext onDragEnd={onDragEnd}>
              <Droppable droppableId={`${fieldName}`} type="WIDGET">
                {(provided, snapshot) => (
                  <div ref={provided.innerRef} style={getFieldsDropStyle(snapshot)}>
                    {fields.map((name, index) => (
                      <Draggable key={name} draggableId={name} index={index}>
                        {(provided, snapshot) => (
                          <Field key={name} name={name}>
                            {({ input }) => {
                              const FormComponent = EditableFieldComponent[input.value.type];
                              return (
                                <FormComponent
                                  input={input}
                                  provided={provided}
                                  snapshot={snapshot}
                                  sourceChoices={sourceChoices}
                                />
                              );
                            }}
                          </Field>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          </Grid>
          <Grid item xs={12}>
            <FieldInput fields={fields} />
          </Grid>
        </Grid>
      </AccordionDetails>
    </Accordion>
  );
};

export default Fieldset;
