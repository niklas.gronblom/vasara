import React, { useState } from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import DeleteIcon from '@material-ui/icons/DeleteForeverOutlined';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import DragIndicatorIcon from '@material-ui/icons/DragIndicator';
import { toLabel } from '../../util/helpers';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import { Field } from 'react-final-form';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import makeStyles from '@material-ui/styles/makeStyles/makeStyles';
import { Choice } from '../types';

interface Props {
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices?: Choice[];
}

const useAccordionStyles = makeStyles({
  root: {
    borderRadius: '4px !important',
    marginBottom: '4px',
  },
});

const useSummaryStyles = makeStyles({
  root: {
    backgroundColor: 'inherit !important',
    borderRadius: '4px !important',
    filter: 'drop-shadow(1px 1px 1px grey)',
  },
  content: {
    margin: '4px 0 !important',
  },
});

const getFieldStyle = (snapshot: DraggableStateSnapshot, draggableStyles: any) => ({
  userSelect: 'none',
  background:
    !snapshot.isDragging && !snapshot.draggingOver
      ? '#fdfcfc'
      : snapshot.isDragging && snapshot.draggingOver
      ? 'lightgreen'
      : 'red',
  ...draggableStyles,
});

const FieldsetField: React.FC<Props> = props => {
  const [expanded, setExpanded] = useState<boolean>(false);
  const field = props.input.value;
  const accordionStyles = useAccordionStyles();
  const summaryStyles = useSummaryStyles();

  return (
    <>
      {/* TODO: is there any other solution?
        Without this hack following console error will be given (TypeError: state.change is not a function)
       in case fieldset order has been changed and you will try to enter some value into some field's input.*/}
      {Object.keys(props.input.value).map((value: any) => (
        <Field key={`${props.input.name}.${value}`} name={`${props.input.name}.${value}`}>
          {({ input, meta }) => <></>}
        </Field>
      ))}
      <Accordion
        expanded={expanded}
        ref={props.provided.innerRef}
        {...props.provided.draggableProps}
        onChange={() => setExpanded(!expanded)}
        style={getFieldStyle(props.snapshot, props.provided.draggableProps.style)}
        classes={{ ...accordionStyles }}
      >
        <AccordionSummary classes={{ ...summaryStyles }} expandIcon={<ExpandMoreIcon />} aria-controls={`content`}>
          <Typography>
            <IconButton
              {...props.provided.dragHandleProps}
              disableRipple={false}
              aria-label="move"
              onFocus={event => event.stopPropagation()}
              onMouseDown={event => event.stopPropagation()}
              onClick={event => event.stopPropagation()}
            >
              <DragIndicatorIcon />
            </IconButton>
            <strong>
              {props.snapshot.isDragging && !props.snapshot.draggingOver ? (
                <div style={{ display: 'inline-flex', verticalAlign: 'middle' }}>
                  <DeleteIcon />
                  &nbsp;
                  {toLabel(field.type)}
                </div>
              ) : (
                toLabel(field.type)
              )}
            </strong>
            : {field.label} {field.required && '*'}
          </Typography>
        </AccordionSummary>
        <AccordionDetails style={{ padding: '1rem', display: 'block' }}>{props.children}</AccordionDetails>
      </Accordion>
    </>
  );
};

export default FieldsetField;
