import { createContext } from 'react';

export const FormBuilderContext = createContext({});

export const FormUserTaskContext = createContext({});
