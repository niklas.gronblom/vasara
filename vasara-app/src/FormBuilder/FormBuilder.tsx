import React, { useCallback, useContext } from 'react';

import { useFieldArray } from 'react-final-form-arrays';

import { UserTask } from 'bpmn-moddle';

import Grid from '@material-ui/core/Grid';

import { IntrospectionContext } from '../DataProviders/HasuraContext';
import { getUserTaskEntities, getUserTaskForm } from '../DataProviders/Camunda/helpers';
import { FormBuilderContext } from './FormBuilderContext';

import FieldsetInput from './Fieldsets/FieldsetInput';
import Fieldset from './Fieldsets/Fieldset';

import { getSourceChoices } from '../util/helpers';

import { Choice } from './types';
import { DragDropContext, Draggable, Droppable, DroppableStateSnapshot } from 'react-beautiful-dnd';
import Preview from './Preview';

interface Props {
  sourceChoices: Choice[];
}

const getFieldsetDropStyle = (snapshot: DroppableStateSnapshot) => ({
  background: snapshot.isDraggingOver ? '#edf2f7' : snapshot.draggingFromThisWith ? '#ecc0c0' : 'inherit',
  borderRadius: '4px',
  padding: '10px',
  width: 'auto',
});

const FormBuilderContainer: React.FC<Props> = props => {
  const { fields: fieldsets } = useFieldArray('schema', {
    subscription: { submitting: true, pristine: true },
  });

  const onDragEnd = useCallback(
    (result: any) => {
      if (!result.destination) {
        fieldsets.remove(result.source.index);
      } else if (result.source.index === result.destination.index) {
        return;
      } else {
        fieldsets.move(result.source.index, result.destination.index);
      }
    },
    [fieldsets]
  );

  return (
    <Grid container spacing={1} justify="center" alignItems="center">
      <Grid item xs={12}>
        <div style={{ overflow: 'hidden', clear: 'both', width: '100%' }}>
          <Preview />
        </div>
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId="fieldset">
            {(provided, snapshot) => (
              <div ref={provided.innerRef} style={getFieldsetDropStyle(snapshot)}>
                {(fieldsets || []).map((name, index) => (
                  <Draggable key={name} draggableId={name} index={index}>
                    {(provided, snapshot) => (
                      <Fieldset
                        fieldName={name}
                        provided={provided}
                        snapshot={snapshot}
                        sourceChoices={props.sourceChoices}
                      />
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      </Grid>
      <Grid item xs={9} md={5}>
        <FieldsetInput fields={fieldsets} />
      </Grid>
    </Grid>
  );
};

const FormBuilder = () => {
  const context = useContext(FormBuilderContext) as UserTask;
  const introspection = useContext(IntrospectionContext);
  const form = getUserTaskForm(context);
  const entities = getUserTaskEntities(context);
  const sourceChoices = form
    .map((field: any) => {
      return {
        id: `context.${field.id}`,
        name: `Task: ${field.id} (${field.type})`,
      };
    })
    .concat(getSourceChoices(introspection, entities));

  return <FormBuilderContainer sourceChoices={sourceChoices} />;
};

export default FormBuilder;
