import React from 'react';
import { AutocompleteArrayInput, BooleanInput, required, TextInput } from 'react-admin';
import { Choice } from '../types';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import FieldsetField from '../Fieldsets/FieldsetField';
import { FieldRenderProps } from 'react-final-form-hooks';

interface Props {
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices: Choice[];
}

const StringField: React.FC<Props> = props => {
  const input = props.input;

  return (
    <FieldsetField {...props}>
      <TextInput id={`${input.name}-label`} source={`${input.name}.label`} label="Label" fullWidth={true} />

      <TextInput id={`${input.name}-helperText`} source={`${input.name}.helperText`} label="Help" fullWidth={true} />

      <AutocompleteArrayInput
        id={`${input.name}-sources`}
        source={`${input.name}.sources`}
        label="Fields"
        fullWidth={true}
        choices={props.sourceChoices}
      />
      <BooleanInput
        fullWidth={true}
        label="ra.form.multiline"
        source={`${input.name}.multiline`}
        initialValue={false}
      />
      <BooleanInput
        fullWidth={true}
        label="ra.validation.required"
        source={`${input.name}.required`}
        initialValue={false}
      />
    </FieldsetField>
  );
};

export const StringInput: React.FC<FieldRenderProps> = ({ input }) => {
  const value = input.value;
  return (
    <TextInput
      source={value.id}
      label={value.label}
      helperText={value.helperText}
      fullWidth={true}
      placeholder={value.placeholder}
      validate={value.required ? [required()] : []}
      multiline={value.multiline}
      initialValue=""
    />
  );
};

export default StringField;
