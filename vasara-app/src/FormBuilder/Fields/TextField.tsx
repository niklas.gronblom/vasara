import React from 'react';
import { AutocompleteArrayInput, BooleanInput, required, TextInput as RATextInput } from 'react-admin';
import { Choice } from '../types';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import FieldsetField from '../Fieldsets/FieldsetField';
import { FieldRenderProps } from 'react-final-form-hooks';

interface Props {
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices: Choice[];
}

const TextField: React.FC<Props> = props => {
  const input = props.input;

  return (
    <FieldsetField {...props}>
      <RATextInput id={`${input.name}-label`} source={`${input.name}.label`} label="Label" fullWidth={true} />

      <AutocompleteArrayInput
        id={`${input.name}-sources`}
        source={`${input.name}.sources`}
        label="Fields"
        fullWidth={true}
        choices={props.sourceChoices}
      />

      <BooleanInput
        fullWidth={true}
        label="ra.validation.required"
        source={`${input.name}.required`}
        initialValue={false}
      />
    </FieldsetField>
  );
};

export const TextInput: React.FC<FieldRenderProps> = ({ input }) => {
  const value = input.value;
  return (
    <RATextInput
      source={value.id}
      label={value.label}
      fullWidth={true}
      placeholder={value.placeholder}
      validate={value.required ? [required()] : []}
      initialValue=""
    />
  );
};

export default TextField;
