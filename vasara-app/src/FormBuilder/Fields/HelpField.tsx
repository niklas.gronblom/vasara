import React from 'react';
import RichTextInput from 'ra-input-rich-text';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import FieldsetField from '../Fieldsets/FieldsetField';
import { FieldRenderProps } from 'react-final-form-hooks';
import { BooleanInput, TextInput } from 'react-admin';

interface Props {
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
}

const HelpField: React.FC<Props> = (props: any) => {
  const input = props.input;

  return (
    <FieldsetField {...props}>
      <TextInput
        source={`${input.name}.label`}
        label="Label"
        fullWidth={true}
        placeholder="Set label"
        initialValue=""
      />
      <RichTextInput
        source={`${input.name}.content`}
        label="Help"
        fullWidth={true}
        placeholder="Set help"
        initialValue=""
      />
      <BooleanInput fullWidth={true} label="ra.form.label" source={`${input.name}.showLabel`} initialValue={false} />
    </FieldsetField>
  );
};

export const HelpDisplay: React.FC<FieldRenderProps> = ({ input }) => {
  const value = input.value;
  return value.showLabel ? (
    <>
      <h2>{value.label}</h2>
      <div dangerouslySetInnerHTML={{ __html: value.content }} />
    </>
  ) : (
    <div dangerouslySetInnerHTML={{ __html: value.content }} />
  );
};

export default HelpField;
