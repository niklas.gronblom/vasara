import React from 'react';
import { TextInput } from 'react-admin';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import FieldsetField from '../Fieldsets/FieldsetField';
import { FieldRenderProps } from 'react-final-form-hooks';

interface Props {
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
}

const LabelField: React.FC<Props> = (props: any) => {
  const input = props.input;

  return (
    <FieldsetField {...props}>
      <TextInput
        source={`${input.name}.content`}
        label="Label"
        fullWidth={true}
        placeholder="Set label"
        initialValue=""
      />
    </FieldsetField>
  );
};

export const LabelDisplay: React.FC<FieldRenderProps> = ({ input }) => {
  const value = input.value;
  return <h2>{value.content}</h2>;
};

export default LabelField;
