import { ArrayInput, AutocompleteInput, RadioButtonGroupInput, SimpleFormIterator, TextInput } from 'react-admin';
import React from 'react';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import FieldsetField from '../Fieldsets/FieldsetField';
import { FieldRenderProps } from 'react-final-form-hooks';

interface Props {
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
}

const SelectField: React.FC<Props> = (props: any) => {
  const input = props.input;

  return (
    <FieldsetField {...props}>
      <TextInput id={`${input.name}-label`} source={`${input.name}.label`} label="Label" fullWidth={true} />

      <ArrayInput source={`${input.name}.options`} label="Options">
        <SimpleFormIterator>
          <TextInput source={`id`} label="Value" />
          <TextInput source={`name`} label="Label" />
        </SimpleFormIterator>
      </ArrayInput>

      <RadioButtonGroupInput
        source={`${input.name}.inputType`}
        label="Type of element"
        defaultValue="radio"
        choices={[
          { id: 'radio', name: 'Radio' },
          { id: 'dropdown', name: 'Dropdown' },
        ]}
      />
    </FieldsetField>
  );
};

export const SelectInput: React.FC<FieldRenderProps> = ({ input }) => {
  const value = input.value;
  return (
    <>
      {value.inputType === 'radio' ? (
        <RadioButtonGroupInput source="selectRadio" label={value.label} choices={value.options} row={false} />
      ) : (
        <AutocompleteInput source="select" label={value.label} choices={value.options} />
      )}
    </>
  );
};

export default SelectField;
