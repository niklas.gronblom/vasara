import {
  ArrayInput as RAArrayInput,
  AutocompleteArrayInput,
  CheckboxGroupInput,
  NumberInput,
  RadioButtonGroupInput,
  SimpleFormIterator,
  TextInput,
} from 'react-admin';
import React from 'react';
import { FieldRenderProps } from 'react-final-form-hooks';
import FieldsetField from '../Fieldsets/FieldsetField';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';

interface Props {
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
}

const ArrayField: React.FC<Props> = props => {
  const input = props.input;

  return (
    <FieldsetField {...props}>
      <TextInput id={`${input.name}-label`} source={`${input.name}.label`} label="Label" fullWidth={true} />
      <span>
        <NumberInput source={`${input.name}.min`} label="Min required" defaultValue={1} />
        <NumberInput source={`${input.name}.max`} label="Max required" defaultValue={1} />
      </span>
      <RAArrayInput source={`${input.name}.options`} label="Options">
        <SimpleFormIterator>
          <TextInput source={`id`} label="Value" />
          <TextInput source={`name`} label="Label" />
        </SimpleFormIterator>
      </RAArrayInput>
      <RadioButtonGroupInput
        source={`${input.name}.inputType`}
        label="Type of element"
        initialValue="checkbox"
        choices={[
          { id: 'checkbox', name: 'Checkbox' },
          { id: 'dropdown', name: 'Dropdown' },
        ]}
      />
    </FieldsetField>
  );
};

const maxRequired = (max: number = 1) => (value: any) => {
  return value && value.length > max ? `You've checked more than ${max} option(s)` : undefined;
};

const minRequired = (min: number = 1) => (value: any) => {
  return value && value < min ? `Please check at least ${min} option(s)` : undefined;
};

export const ArrayInput: React.FC<FieldRenderProps> = ({ input }) => {
  const value = input.value;
  return (
    <>
      {value.inputType === 'checkbox' ? (
        <CheckboxGroupInput
          source="checkbox"
          label={value.label}
          choices={value.options}
          row={false}
          validate={[minRequired(value.min), maxRequired(value.max)]}
        />
      ) : (
        <AutocompleteArrayInput
          source="array"
          label={value.label}
          choices={value.options}
          validate={[minRequired(value.min), maxRequired(value.max)]}
        />
      )}
    </>
  );
};

export default React.memo(ArrayField);
