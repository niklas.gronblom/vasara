import React from 'react';
import { AutocompleteArrayInput, BooleanInput as RABooleanInput, required, TextInput } from 'react-admin';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import FieldsetField from '../Fieldsets/FieldsetField';
import { Choice } from '../types';
import { FieldRenderProps } from 'react-final-form-hooks';

interface Props {
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices: Choice[];
}

const BooleanField: React.FC<Props> = props => {
  const input = props.input;

  return (
    <FieldsetField {...props}>
      <TextInput
        source={`${input.name}.label`}
        label="Label"
        fullWidth={true}
        placeholder="Set label"
        initialValue=""
      />

      <AutocompleteArrayInput
        id={`${input.name}-sources`}
        source={`${input.name}.sources`}
        label="Fields"
        fullWidth={true}
        choices={props.sourceChoices}
      />
    </FieldsetField>
  );
};

export const BooleanInput: React.FC<FieldRenderProps> = ({ input }) => {
  const value = input.value;
  return (
    <RABooleanInput
      source={value.id}
      label={value.label}
      fullWidth={true}
      placeholder={value.placeholder}
      validate={value.required ? [required()] : []}
      initialValue=""
    />
  );
};

export default BooleanField;
