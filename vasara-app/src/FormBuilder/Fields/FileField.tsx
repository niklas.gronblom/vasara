import React from 'react';
import {
  AutocompleteArrayInput,
  BooleanInput,
  FileField as RAFileField,
  FileInput as RAFileInput,
  required,
  TextInput,
} from 'react-admin';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import { Choice } from '../types';
import FieldsetField from '../Fieldsets/FieldsetField';
import { FieldRenderProps } from 'react-final-form-hooks';

interface Props {
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices: Choice[];
}

const FileField: React.FC<Props> = (props: any) => {
  const input = props.input;

  return (
    <FieldsetField {...props}>
      <TextInput
        source={`${input.name}.label`}
        label="Label"
        fullWidth={true}
        placeholder="Set label"
        initialValue=""
      />

      <AutocompleteArrayInput
        id={`${input.name}-sources`}
        source={`${input.name}.sources`}
        label="Fields"
        fullWidth={true}
        choices={props.sourceChoices}
      />

      <BooleanInput label="ra.validation.required" source={`${input.name}.required`} initialValue={false} />
    </FieldsetField>
  );
};

export const FileInput: React.FC<FieldRenderProps> = ({ input }) => {
  const value = input.value;
  return (
    <RAFileInput
      source={value.id}
      label={value.label}
      placeholder={value.placeholder}
      validate={value.required ? [required()] : []}
      initialValue=""
    >
      <RAFileField source="src" title="title" />
    </RAFileInput>
  );
};

export default FileField;
