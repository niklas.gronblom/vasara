import { createMuiTheme, Theme } from '@material-ui/core/styles';

export const theme: Theme = createMuiTheme({
  palette: {
    primary: {
      main: '#002957', // main: "#f1563f"
    },
    secondary: {
      main: '#002957',
    },
  },
});
