export const getEnv = (key: string): string | undefined => {
  // TODO: Refactor as needed
  try {
    return process.env[key];
  } catch (e) {
    return undefined;
  }
};
