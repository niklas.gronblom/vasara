import { IndexableObject } from './types';
import { ByteaFileField } from '../Form/Fields/FileField';
import { BooleanField, DateField, NumberField, TextField } from 'react-admin';
import JSONBField from '../Form/Fields/JSONBField';

// Default items per page
export const PAGE_SIZE = 25;

export const SCALAR = 'SCALAR';

export const RE_AGGREGATE = /_aggregate$/gi;
export const RE_READONLY = /^readonly_/gi;
export const RE_VASARA = /^vasara_/gi;
export const RE_REFERENCE_ATTR = /_(arr|obj)_rel_insert_input/gi;

// Types of scalars
export const SCALAR_TYPES = {
  BOOLEAN: 'Boolean',
  BYTEA: 'bytea',
  DATE: 'date',
  FLOAT8: 'float8',
  INT: 'Int',
  STRING: 'String',
  TIMESTAMP: 'timestamp',
  JSONB: 'jsonb',
};

// Map SCALAR-types to correct RA Field
export const SCALARCOMPONENTS_MAP: IndexableObject = {
  [SCALAR_TYPES.BOOLEAN]: BooleanField,
  [SCALAR_TYPES.BYTEA]: ByteaFileField,
  [SCALAR_TYPES.DATE]: DateField,
  [SCALAR_TYPES.FLOAT8]: NumberField,
  [SCALAR_TYPES.INT]: NumberField,
  [SCALAR_TYPES.STRING]: TextField,
  [SCALAR_TYPES.TIMESTAMP]: DateField,
  [SCALAR_TYPES.JSONB]: JSONBField,
};

// The main "identifier" columns used to display item in list
export const MAIN_IDENTIFIER_COLUMNS = ['Name', 'Description', 'Comment'];

// Table columns which won't be displayed in UI (ID is usually something we don't want the user to have access to)
export const SKIP_COLUMNS = ['id', 'metadata', 'ProcessInstances', 'allowedRolesAndUsers'];

// Skip also columns which end with "Id" => Don't display FK:s directly
export const SKIP_COLUMN_SUFFIXES = ['Id'];

export const ONE_TO_MANY_SUFFIX = '_obj_rel_insert_input';
export const MANY_TO_ONE_SUFFIX = '_arr_rel_insert_input';

// Resources which should be hidden from main menu
export const HIDE_FROM_MAIN_MENU = [/^readonly_/gi, /^vasara_/gi];

// Resources which are added manually (to use different component than Entitylist & EntityShow)
export const MANUALLY_ADDED_RESOURCES = [/^vasara_/gi];

// Resources which are linked
export const OPPOSITE_SPECIFIER_MAPPING: Record<string, string> = {
  Child: 'Parent',
  Parent: 'Child',
};
