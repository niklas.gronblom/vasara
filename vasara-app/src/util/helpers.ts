import {
  HIDE_FROM_MAIN_MENU,
  MAIN_IDENTIFIER_COLUMNS,
  MANUALLY_ADDED_RESOURCES,
  MANY_TO_ONE_SUFFIX,
  ONE_TO_MANY_SUFFIX,
  OPPOSITE_SPECIFIER_MAPPING,
  RE_AGGREGATE,
  RE_READONLY,
  RE_REFERENCE_ATTR,
  RE_VASARA,
  SCALAR,
  SKIP_COLUMN_SUFFIXES,
  SKIP_COLUMNS,
} from './constants';
import { ResourceField, ResourceFieldType, ResourceFieldTypeKind, ResourceSchema } from './types';
import { Choice } from '../FormBuilder/types';

export const toLabel = (label: string): string => {
  const parts = label ? label.split(/(?=[A-Z])/) : [];
  return parts
    .slice(0, 1)
    .map((s: string) => s.substring(0, 1).toUpperCase() + s.substring(1))
    .concat(parts.slice(1).map(s => s.toLowerCase()))
    .join(' ');
};

/**
 * Convert a `File` object returned by the upload input into a base 64 string.
 * That's not the most optimized way to store images in production, but it's
 * enough to illustrate the idea of data provider decoration.
 */
export const convertFileToBase64 = (file: any): Promise<string> =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () => resolve((reader.result as string).split(';base64,')[1]);
    reader.onerror = reject;
    reader.readAsDataURL(file.rawFile);
  });

export const convertFileToText = (file: any) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () => resolve(reader.result);
    reader.onerror = reject;
    reader.readAsText(file.rawFile);
  });

export const getSourceChoices = (introspection: any, entities: string[]): Choice[] => {
  const choices: Choice[] = [];
  const types = introspection.types.filter((type_: any) => type_.name.match(/_set_input$/));
  const candidates = entities.concat(['Attachment']);
  for (let type_ of types) {
    const resource = type_.name.substring(0, type_.name.indexOf('_set_input'));
    if (candidates.includes(resource) || candidates.includes(stripHasuraSchemaPrefix(resource))) {
      for (let field of type_.inputFields) {
        if (!['id', 'metadata'].includes(field.name)) {
          choices.push(
            field.type.name === 'jsonb'
              ? {
                  id: `${resource}.${field.name}.{}`,
                  name: `${resource}: ${field.name} (${field.type.name})`,
                }
              : {
                  id: `${resource}.${field.name}`,
                  name: `${resource}: ${field.name} (${field.type.name})`,
                }
          );
        }
      }
    }
  }
  return choices;
};

export const arraysEqual = (a: Array<any>, b: Array<any>): boolean => {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length !== b.length) return false;
  for (let i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
};

export const getFieldTypeKindList = (type: ResourceFieldType): ResourceFieldTypeKind[] => {
  if (type.ofType) {
    return [type.kind].concat(getFieldTypeKindList(type.ofType));
  } else {
    return [type.kind];
  }
};

export const getFieldTypeKind = (type: ResourceFieldType): ResourceFieldTypeKind => {
  return getFieldTypeKindList(type).splice(-1)[0];
};

export const getFieldTypeNameList = (type: ResourceFieldType): string[] => {
  if (type.ofType) {
    return [type.name].concat(getFieldTypeNameList(type.ofType));
  } else {
    return [type.name];
  }
};

export const getFieldTypeName = (type: ResourceFieldType): string => {
  return getFieldTypeNameList(type).splice(-1)[0];
};

export const isFkField = (field: ResourceField): boolean => {
  return SKIP_COLUMN_SUFFIXES.some((suffix: string) => field.name.endsWith(suffix));
};

export const isM2MField = (field: ResourceField): boolean => {
  return isO2MField(field) && stripHasuraSchemaPrefix(getReferenceResourceName(field)).startsWith('_');
};

export const isM2OField = (field: ResourceField): boolean => {
  const list = getFieldTypeKindList(field.type);
  return !list.includes('LIST') && list.includes('OBJECT');
};

export const isM2OInputField = (field: ResourceField): boolean => {
  return field.type.name.endsWith(ONE_TO_MANY_SUFFIX);
};

export const isO2MField = (field: ResourceField): boolean => {
  const list = getFieldTypeKindList(field.type);
  return list.includes('LIST') && list.includes('OBJECT');
};

export const isO2MInputField = (field: ResourceField): boolean => {
  return field.type.name.endsWith(MANY_TO_ONE_SUFFIX);
};

export const isAggregateFieldName = (resourceName: string): Boolean => {
  return resourceName.match(RE_AGGREGATE) !== null || false;
};

export const isReadonlyResource = (resourceName: string): Boolean => {
  return resourceName.match(RE_READONLY) !== null || false;
};

export const isVasaraResource = (resourceName: string): Boolean => {
  return resourceName.match(RE_VASARA) !== null || false;
};

export const isAllowedInMainMenu = (resourceName: string): Boolean => {
  return !HIDE_FROM_MAIN_MENU.some(regex => resourceName.match(regex) !== null);
};

export const isManuallyAdded = (resourceName: string): Boolean => {
  return MANUALLY_ADDED_RESOURCES.some(regex => resourceName.match(regex) !== null);
};

export const isScalarField = (field: ResourceField): boolean => {
  return getFieldTypeKind(field.type) === SCALAR;
};

export const isSkippableField = (field: ResourceField): Boolean => {
  return SKIP_COLUMNS.some(regex => field.name.match(regex) !== null);
};

export const parseInputFieldReferenceAttribute = (field: ResourceField) => {
  return field.type.name.replace(RE_REFERENCE_ATTR, '');
};

export const getSingularName = (name: string, resourceName: string): string => {
  return name.endsWith(`${resourceName}s`) ? name.substring(0, name.length - 1) : name;
};

export const getRelationSpecifier = (name: string, resourceName: string): string => {
  return name.endsWith(resourceName) ? name.substring(0, name.length - resourceName.length).split(/(?=[A-Z])/)[0] : '';
};

export const getOppositeSpecifier = (relationSpecifier: string): string => {
  return OPPOSITE_SPECIFIER_MAPPING[relationSpecifier] || '';
};

export const getReferenceFields = (resourceName: string, schema: ResourceSchema): ResourceField[] => {
  return schema.fields.filter(
    field => getFieldTypeKindList(field.type).includes('OBJECT') && getFieldTypeName(field.type) === resourceName
  );
};

export const getReferenceStorageFields = (resourceName: string, schema: ResourceSchema): ResourceField[] => {
  return schema.fields.filter(
    field =>
      !getFieldTypeKindList(field.type).includes('OBJECT') &&
      (field.name.endsWith(`${resourceName}Id`) || field.name === 'id')
  );
};

export const getOtherReferenceFields = (resourceName: string, schema: ResourceSchema): ResourceField[] => {
  return schema.fields.filter(
    field => getFieldTypeKindList(field.type).includes('OBJECT') && getFieldTypeName(field.type) !== resourceName
  );
};

export const stripHasuraSchemaPrefix = (resourceName: string): string => {
  return resourceName.startsWith('_') || resourceName.indexOf('_') === -1
    ? resourceName
    : resourceName.split('_').splice(1).join('_');
};

export const getReferenceResourceName = (field: ResourceField) => {
  return getFieldTypeNameList(field.type).splice(-1)[0];
};

export const getTargetResourceFieldName = (
  resourceField: ResourceField,
  resourceName: string,
  schemata: Map<string, ResourceSchema>
) => {
  const referenceResourceName = getReferenceResourceName(resourceField);
  const referenceResourceSchema = schemata.get(referenceResourceName) as ResourceSchema;
  const referenceResourceNameWithoutPrefix = stripHasuraSchemaPrefix(referenceResourceName);
  const resourceNameWithoutPrefix = stripHasuraSchemaPrefix(resourceName);

  const relationSpecifier = getRelationSpecifier(
    getSingularName(resourceField.name, referenceResourceNameWithoutPrefix),
    referenceResourceNameWithoutPrefix
  );
  const oppositeSpecifier = getOppositeSpecifier(relationSpecifier);

  const candidateFields = getReferenceStorageFields(resourceNameWithoutPrefix, referenceResourceSchema);
  const matches =
    oppositeSpecifier || relationSpecifier
      ? candidateFields
          .filter(field => field.name.startsWith(oppositeSpecifier || relationSpecifier))
          .concat(
            candidateFields.filter(field =>
              field.name.startsWith((oppositeSpecifier || relationSpecifier).substring(0, 3))
            )
          )
      : candidateFields;

  return matches.length ? matches[0].name : null;
};

export const getSourceResourceFieldName = (
  resourceField: ResourceField,
  resourceName: string,
  schemata: Map<string, ResourceSchema>
) => {
  const referenceResourceName = getReferenceResourceName(resourceField);
  const resourceSchema = schemata.get(resourceName) as ResourceSchema;
  const referenceResourceNameWithoutPrefix = stripHasuraSchemaPrefix(referenceResourceName);

  const relationSpecifier = getRelationSpecifier(
    getSingularName(resourceField.name, referenceResourceNameWithoutPrefix),
    referenceResourceNameWithoutPrefix
  );

  const candidateFields = getReferenceStorageFields(referenceResourceNameWithoutPrefix, resourceSchema);
  const matches = relationSpecifier
    ? candidateFields
        .filter(field => field.name.startsWith(relationSpecifier))
        .concat(candidateFields.filter(field => field.name.startsWith(relationSpecifier.substring(0, 3))))
    : candidateFields;

  return matches.length ? matches[0].name : null;
};

export const getMainIdentifierColumn = (resourceSchemaName: string, schemata: Map<string, ResourceSchema>): string => {
  const schema = schemata.get(resourceSchemaName) as ResourceSchema;
  const names = schema.fields.map(field => field.name);
  for (const name of MAIN_IDENTIFIER_COLUMNS) {
    if (names.includes(name)) {
      return name;
    }
  }
  return 'id';
};

export function byteaToBase64(hexstring: string): string {
  // PostgreSQL bytea stores binaries hex encoded prefixed with \x
  return btoa((hexstring.substring(2).match(/\w{2}/g) || []).map(a => String.fromCharCode(parseInt(a, 16))).join(''));
}

export function base64ToBytea(a: string): string {
  // PostgreSQL bytea stores binaries hex encoded prefixed with \x
  return (
    '\\x' +
    Array.from(atob(a))
      .map(c => c.charCodeAt(0).toString(16).padStart(2, '0'))
      .join('')
  );
}
