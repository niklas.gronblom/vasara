export type ResourceFieldTypeKind = 'OBJECT' | 'INPUT_OBJECT' | 'SCALAR' | 'ENUM' | 'NON_NULL' | 'LIST';

export type ResourceFieldType = {
  kind: ResourceFieldTypeKind;
  name: string;
  ofType?: ResourceFieldType;
};

export type ResourceField = {
  name: string;
  description: string | null;
  defaultValue?: string | null;
  type: ResourceFieldType;
};

export type ResourceSchema = {
  name: string;
  kind: ResourceFieldTypeKind;
  fields: ResourceField[];
};

export type IndexableObject = {
  [index: string]: any;
};
