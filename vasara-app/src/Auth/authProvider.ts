import { Log, User, UserManager } from 'oidc-client';
import { getEnv } from '../util/env';

Log.logger = console;

const AUTHORITY = getEnv('REACT_APP_AUTHORITY') || 'http://keycloak:8080/auth/realms/vasara';
const CLIENT_ID = getEnv('REACT_APP_CLIENT_ID') || 'react-admin';
const REDIRECT_URI = getEnv('REACT_APP_REDIRECT_URI') || 'http://localhost:3000/';
const POST_LOGOUT_REDIRECT_URI = getEnv('REACT_APP_URI') || 'http://localhost:3000/';
const SESSION_KEY = `oidc.user:${AUTHORITY}:${CLIENT_ID}`;

const clientSettings = {
  authority: AUTHORITY,
  client_id: CLIENT_ID,
  redirect_uri: REDIRECT_URI,
  post_logout_redirect_uri: POST_LOGOUT_REDIRECT_URI,
  response_type: 'code',
  scope: 'openid email profile groups roles',
  filterProtocolClaims: true,
  loadUserInfo: true,
  automaticSilentRenew: true,
  accessTokenExpiringNotificationTime: 30,
};

export const userManager = new UserManager(clientSettings);

const sleep = (ms: number) => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

const mutex = {
  free: true,
  acquire: async () => {
    while (!mutex.free) {
      await sleep(100);
    }
    mutex.free = false;
  },
  release: async () => {
    mutex.free = true;
  },
};

export const getUser = (): [User, null] => {
  return sessionStorage.getItem(SESSION_KEY) ? JSON.parse(sessionStorage.getItem(SESSION_KEY) || '"{}"') : null;
};

export const getUserName = (): string | null => {
  const userInfo: [User, null] = getUser();
  const userName = userInfo ? ((userInfo as unknown) as User).profile.preferred_username || null : null;
  const defaultTenant = getEnv('REACT_APP_DEFAULT_TENANT') || '';
  return userName && userName.match('@') ? userName : userName + defaultTenant;
};

const authProvider = {
  login: async () => {
    try {
      await mutex.acquire();
      await userManager.signinRedirect();
    } finally {
      await mutex.release();
    }
  },
  logout: async () => {
    const logoutRoute: string = getEnv('REACT_APP_LOGOUT_URI') || '/login';
    try {
      await mutex.acquire();
      sessionStorage.clear();
      if (logoutRoute.startsWith('http://') || logoutRoute.startsWith('https://')) {
        window.location.href = logoutRoute;
      } else {
        return getEnv('REACT_APP_LOGOUT_URI') || '/login';
      }
    } finally {
      await mutex.release();
    }
  },
  checkAuth: async () => {
    try {
      await mutex.acquire();
      const user = getUser();
      if (user && new Date(((user as unknown) as User).expires_at * 1000) > new Date()) {
        return Promise.resolve();
      } else {
        const qs = window.location.search;
        /* TODO: Use custom state as IdP returning flag*/
        if (qs.match(/^\?state=/) || qs.match(/^\?code=/)) {
          await userManager.signinRedirectCallback();
          const href = window.location.href;
          const search = window.location.search;
          window.history.replaceState(
            null,
            '',
            href.substr(0, href.indexOf(search)) + href.substr(href.indexOf(search) + search.length)
          );
          return Promise.resolve();
        }
      }
      throw await Promise.reject();
    } finally {
      await mutex.release();
    }
  },
  getPermissions: async () => {},

  checkError: async (error: any) => {
    const user = getUser();
    if (error.status === 401 || error.status === 403) {
      // API unauthorized
      sessionStorage.clear();
      return Promise.reject();
    } else if (user && new Date(((user as unknown) as User).expires_at * 1000) < new Date()) {
      // Token expired
      sessionStorage.clear();
      await authProvider.login();
      return Promise.resolve();
    }
    return Promise.resolve();
  },
};

export default authProvider;
