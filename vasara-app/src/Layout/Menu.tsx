import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Divider, Theme, useMediaQuery } from '@material-ui/core';
import { getResources, MenuItemLink, useTranslate } from 'react-admin';
import { Inbox, PlayCircleFilled, Storage, ViewList } from '@material-ui/icons';
import { isAllowedInMainMenu } from '../util/helpers';
import SubMenu from './SubMenu';

type MenuName = 'menuWorkflow' | 'menuDatabase';

const Menu = ({ onMenuClick, logout }: any) => {
  const translate = useTranslate();
  const [state, setState] = useState({
    menuWorkflow: false,
    menuDatabase: false,
  });

  const isXSmall = useMediaQuery((theme: Theme) => theme.breakpoints.down('xs'));
  const open = useSelector((state: any) => state.admin.ui.sidebarOpen);
  const defaultResources: any = useSelector(getResources);
  const resources = defaultResources.filter(
    (resource: any) => resource && isAllowedInMainMenu(resource.name) && !resource.options.hideFromMenu
  );

  const handleToggle = (menu: MenuName) => {
    setState(state => ({ ...state, [menu]: !state[menu] }));
  };

  return (
    <>
      {resources
        .filter((resource: any) => ['UserTask'].includes(resource.name))
        .map((resource: any) => {
          return (
            <MenuItemLink
              key={resource.name}
              to={`/${resource.name}`}
              primaryText={(resource.options && resource.options.label) || resource.name}
              leftIcon={resource.icon ? <resource.icon /> : <Inbox />}
              onClick={onMenuClick}
              sidebarIsOpen={open}
            />
          );
        })}
      <Divider />
      {resources
        .filter((resource: any) => ['ProcessDefinition', 'DISABLEDProcessInstance'].includes(resource.name))
        .map((resource: any) => {
          return (
            <MenuItemLink
              key={resource.name}
              to={`/${resource.name}`}
              primaryText={(resource.options && resource.options.label) || resource.name}
              leftIcon={<PlayCircleFilled />}
              onClick={onMenuClick}
              sidebarIsOpen={open}
            />
          );
        })}
      <Divider />
      <SubMenu
        handleToggle={() => handleToggle('menuDatabase')}
        isOpen={state.menuDatabase}
        sidebarIsOpen={open}
        name={translate('ra.menu.database')}
        icon={<Storage />}
        dense={true}
      >
        {resources.map((resource: any) => {
          if (
            !resource.name.match(/_/) &&
            !['UserTask', 'ProcessDefinition', 'ProcessInstance'].includes(resource.name)
          ) {
            return (
              <MenuItemLink
                key={resource.name}
                to={`/${resource.name}`}
                primaryText={(resource.options && resource.options.label) || resource.name}
                leftIcon={resource.icon ? <resource.icon /> : <ViewList />}
                onClick={onMenuClick}
                sidebarIsOpen={open}
              />
            );
          } else {
            return null;
          }
        })}
      </SubMenu>
      {isXSmall && logout}
    </>
  );
};

export default Menu;
