import React from 'react';
import { useSelector } from 'react-redux';
import { Theme, useMediaQuery } from '@material-ui/core';
import { getResources, MenuItemLink } from 'react-admin';
import { Inbox } from '@material-ui/icons';
import { isAllowedInMainMenu } from '../util/helpers';

const TaskListMenu = ({ onMenuClick, logout }: any) => {
  const isXSmall = useMediaQuery((theme: Theme) => theme.breakpoints.down('xs'));
  const open = useSelector((state: any) => state.admin.ui.sidebarOpen);
  const defaultResources: any = useSelector(getResources);
  const resources = defaultResources.filter(
    (resource: any) => resource && isAllowedInMainMenu(resource.name) && !resource.options.hideFromMenu
  );

  return (
    <>
      {resources
        .filter((resource: any) => ['UserTask'].includes(resource.name))
        .map((resource: any) => {
          return (
            <MenuItemLink
              key={resource.name}
              to={`/${resource.name}`}
              primaryText={(resource.options && resource.options.label) || resource.name}
              leftIcon={resource.icon ? <resource.icon /> : <Inbox />}
              onClick={onMenuClick}
              sidebarIsOpen={open}
            />
          );
        })}
      {isXSmall && logout}
    </>
  );
};

export default TaskListMenu;
