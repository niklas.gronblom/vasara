import { Notification } from 'react-admin';
import React from 'react';
import { useSelector } from 'react-redux';
import { getNotification } from 'ra-core';

const CustomNotification = (props: any) => {
  const notification = useSelector(getNotification);
  // Item is "lost" every time we complete a task and we want to hide it
  try {
    if (notification.message === 'ra.notification.item_doesnt_exist') {
      return null;
    }
  } catch (e) {}
  return <Notification {...props} autoHideDuration={5000} />;
};

export default CustomNotification;
