import React from 'react';
import { Layout } from 'react-admin';
import TaskListMenu from './TaskListMenu';
import CustomNotification from './Notification';

const TaskListLayout = (props: any) => <Layout {...props} menu={TaskListMenu} notification={CustomNotification} />;

export default TaskListLayout;
