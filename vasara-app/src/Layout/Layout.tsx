import React from 'react';
import { Layout } from 'react-admin';
import Menu from './Menu';
import CustomNotification from './Notification';

const CustomLayout = (props: any) => <Layout {...props} menu={Menu} notification={CustomNotification} />;

export default CustomLayout;
