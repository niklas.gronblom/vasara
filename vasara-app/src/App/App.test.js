import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

// TODO: How to properly test this since every action requires OIDC session?

test("renders ra.page.loading", () => {
  const { getByText } = render(<App />);
  const element = getByText(/Loading../i);
  expect(element).toBeInTheDocument();
});
