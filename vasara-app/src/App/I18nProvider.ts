import finnishMessages from 'ra-language-finnish';
import polyglotI18nProvider from 'ra-i18n-polyglot';

// https://github.com/Aikain/ra-language-finnish/blob/master/src/index.ts

export const i18nProvider = polyglotI18nProvider(() => {
  return {
    ...finnishMessages,
    ra: {
      ...finnishMessages.ra,
      action: {
        ...finnishMessages.ra.action,
        submit: 'Lähetä',
        start: 'Aloita',
        claim: 'Ota käsiteltäväksi',
      },
      page: {
        ...finnishMessages.ra.page,
        empty: 'Lista on tyhjä',
        invite: '',
      },
      form: {
        label: 'Otsikko',
        help: 'Ohjeteksti',
        boolean: 'Kyllä / Ei',
        string: 'Teksti',
        integer: 'Numero',
        date: 'Päivämäärä',
        file: 'Liitetiedosto',
        select: 'Valinta',
        array: 'Monivalinta',
        multiline: 'Monirivinen',
      },
      view: {
        processDefinition: 'Työnkulku',
        processDefinitionList: 'Tarjolla olevat työnkulut',
        versions: 'Versiot',
      },
      column: {
        title: 'Nimike',
        diagram: 'Kaavio',
        processDefinitionName: 'Työnkulku',
      },
      menu: {
        userTask: 'Tehtävät',
        processDefinition: 'Tarjolla',
        decisionDefinition: 'Päätöstaulut',
        processInstance: 'Kesken',
        workflows: 'Työnkulut',
        database: 'Tietokanta',
      },
      notification: {
        ...finnishMessages.ra.notification,
        created: 'Asia tallennettu',
        updated: 'Asia tallennettu |||| %{smart_count} asiaa tallennettu',
      },
      message: {
        ...finnishMessages.ra.message,
        builder_available_controls: 'Uusi kenttä:',
      },
      resource: {
        ...finnishMessages.ra.resource,
        userTask: 'Tehtävät',
        processDefinition: 'Tarjolla',
        decisionDefinition: 'Päätöstaulut',
        processInstance: 'Kesken',
        workflows: 'Työnkulut',
        database: 'Tietokanta',
      },
    },
  };
}, 'fi');
