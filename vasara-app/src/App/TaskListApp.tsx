import React, { Component } from 'react';
import { Admin, Loading, Resource } from 'react-admin';
import { Build, Timeline } from '@material-ui/icons';

import buildCamundaProvider from '../DataProviders/Camunda';
import { IntrospectionContext } from '../DataProviders/HasuraContext';
import { buildHasuraProviderWithIntrospection } from '../DataProviders/HasuraProvider';
import authProvider from '../Auth/authProvider';

import { UserTaskCreate, UserTaskEdit, UserTaskList, UserTaskShow } from '../Entities/Camunda/UserTask';

import {
  ProcessDefinitionCreate,
  ProcessDefinitionList,
  ProcessDefinitionShow,
} from '../Entities/Camunda/ProcessDefinition';
import { DecisionDefinitionCreate, DecisionDefinitionList } from '../Entities/Camunda/DecisionDefinition';
import { ProcessInstanceList } from '../Entities/Camunda/ProcessInstance';
import { EntityListComponent } from '../Entities/EntityList';
import { EntityShowComponent } from '../Entities/EntityShow';

import { theme } from '../util/theme';

import { isAggregateFieldName, isManuallyAdded, stripHasuraSchemaPrefix, toLabel } from '../util/helpers';

import LoginPage from '../Layout/LoginPage';

import '../static/App.css';
import { FormCreate, FormEdit } from '../Entities/Vasara/VasaraUserTaskForm';
import { i18nProvider } from './I18nProvider';
import TaskListLayout from '../Layout/TaskListLayout';

interface IAppProps {}

interface IAppState {
  camundaDataProvider: any | null;
  hasuraDataProvider: any | null;
  hasuraIntrospectionResults: any | null;
}

class TaskListApp extends Component<IAppProps, IAppState> {
  constructor(props: IAppProps) {
    super(props);

    this.state = {
      camundaDataProvider: null,
      hasuraDataProvider: null,
      hasuraIntrospectionResults: null,
    };
  }

  async componentDidMount() {
    try {
      await authProvider.checkAuth();
    } catch {
      await authProvider.login();
    }

    const camundaDataProvider = await buildCamundaProvider();
    const { hasuraDataProvider, hasuraIntrospectionResults } = await buildHasuraProviderWithIntrospection();

    return this.setState({
      ...this.state,
      camundaDataProvider,
      hasuraDataProvider,
      hasuraIntrospectionResults,
    });
  }

  dataProvider(type: string, resource: string, params: any) {
    const { camundaDataProvider, hasuraDataProvider } = this.state;

    switch (resource) {
      case 'DecisionDefinition':
      case 'ProcessDefinition':
      case 'ProcessDefinitionUserTask':
      case 'ProcessDefinitionVersions':
      case 'ProcessInstance':
      case 'UserTask':
        return camundaDataProvider(type, resource, params);
      default:
        return hasuraDataProvider(type, resource, params);
    }
  }

  render() {
    const { camundaDataProvider, hasuraDataProvider, hasuraIntrospectionResults } = this.state;

    const resources = hasuraIntrospectionResults
      ? hasuraIntrospectionResults.resources
          .map((resource: any) => resource.type.name)
          .filter((name: string) => !isAggregateFieldName(name) && !isManuallyAdded(name))
      : [];

    return camundaDataProvider && hasuraDataProvider && hasuraIntrospectionResults ? (
      <IntrospectionContext.Provider value={hasuraIntrospectionResults}>
        <Admin
          theme={theme}
          authProvider={authProvider}
          loginPage={LoginPage}
          i18nProvider={i18nProvider}
          layout={TaskListLayout}
          dataProvider={(type: string, resource: string, params: any) => this.dataProvider(type, resource, params)}
        >
          <Resource
            name="UserTask"
            list={UserTaskList}
            show={UserTaskShow}
            edit={UserTaskEdit}
            create={UserTaskCreate}
            options={{ label: i18nProvider.translate('ra.resource.userTask') }}
          />
          <Resource
            name="ProcessDefinition"
            list={ProcessDefinitionList}
            show={ProcessDefinitionShow}
            create={ProcessDefinitionCreate}
            icon={Build}
            options={{
              label: i18nProvider.translate('ra.resource.processDefinition'),
            }}
          />
          <Resource
            name="DecisionDefinition"
            list={DecisionDefinitionList}
            create={DecisionDefinitionCreate}
            icon={Build}
            options={{
              label: i18nProvider.translate('ra.resource.decisionDefinition'),
            }}
          />
          <Resource
            name="ProcessInstance"
            list={ProcessInstanceList}
            icon={Timeline}
            options={{
              label: i18nProvider.translate('ra.resource.processInstance'),
            }}
          />
          {resources.map((name: string) => {
            return (
              <Resource
                key={name}
                name={name}
                list={EntityListComponent}
                show={EntityShowComponent(name, hasuraIntrospectionResults)}
                options={{
                  label: toLabel(stripHasuraSchemaPrefix(name)),
                  hideFromMenu: name.match(/_/),
                }}
              />
            );
          })}

          {/* Will be hidden, but required by RA to display referencefields correctly */}
          <Resource name="ProcessDefinitionUserTask" options={{ hideFromMenu: true }} />
          <Resource name="ProcessDefinitionVersions" options={{ hideFromMenu: true }} />
          <Resource name="vasara_user_task_form" create={FormCreate} edit={FormEdit} options={{ hideFromMenu: true }} />
        </Admin>
      </IntrospectionContext.Provider>
    ) : (
      <Loading loadingPrimary={''} loadingSecondary={'Loading..'} />
    );
  }
}

export default TaskListApp;
