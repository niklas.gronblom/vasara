// in src/Dashboard.js
import React from 'react';
import img from '../static/vasara.png';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  img: {
    maxWidth: '100vw',
    width: '510px',
    margin: '0 auto',
  },
});

export default () => {
  const classes = useStyles();
  return <img src={img} alt="Welcome" className={classes.img} />;
};
