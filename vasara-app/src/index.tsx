import React from 'react';
import ReactDOM from 'react-dom';
import './static/index.css';
import App from './App/App';
import * as serviceWorker from './serviceWorker';
import TaskListApp from './App/TaskListApp';
import { getEnv } from './util/env';
import 'dmn-js/dist/assets/dmn-js-shared.css';
import 'dmn-js/dist/assets/dmn-js-drd.css';
import 'dmn-js/dist/assets/dmn-js-decision-table.css';
import 'dmn-js/dist/assets/dmn-font/css/dmn.css';

if (getEnv('REACT_APP_BUILD_TARGET') === 'tasklist') {
  ReactDOM.render(<TaskListApp />, document.getElementById('root'));
} else {
  ReactDOM.render(<App />, document.getElementById('root'));
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
