import gql from 'graphql-tag';
import { CamundaDataProvider, RaFetchType } from './types';
import { CreateParams, GetListParams, GetOneParams } from 'ra-core';

export const ProcessDefinitionDataProvider: CamundaDataProvider = (
  introspectionResults,
  raFetchType,
  resource,
  params
) => {
  switch (raFetchType) {
    case RaFetchType.CREATE:
      const createParams = params as CreateParams;
      return {
        query: gql`
          mutation CreateProcessDefinition($resource: String, $diagram: String!) {
            createProcessDefinition(resource: $resource, diagram: $diagram) {
              id
            }
          }
        `,
        variables: createParams.data,
        parseResponse: (response: any) => {
          return {
            data: response.data.createProcessDefinition,
          };
        },
      };
    case RaFetchType.GET_LIST:
    case RaFetchType.GET_MANY:
    case RaFetchType.GET_MANY_REFERENCE:
      const getListParams = params as GetListParams;
      const { filter } = getListParams;

      return {
        query: gql`
          query processDefinitions($latest: Boolean) {
            processDefinitions(latest: $latest) {
              id
              contextPath
              description
              diagram
              isSuspended
              key
              name
              startFormKey
              version
              versionTag
            }
          }
        `,
        variables: {
          latest: filter.latest,
        },
        parseResponse: (response: any) => {
          return {
            data: response.data.processDefinitions.slice(
              (getListParams.pagination.page - 1) * getListParams.pagination.perPage,
              getListParams.pagination.page * getListParams.pagination.perPage
            ),
            total: response.data.processDefinitions.length,
          };
        },
      };
    case RaFetchType.GET_ONE:
      const getOneParams = params as GetOneParams;
      return {
        query: gql`
          query processDefinition($id: String!) {
            processDefinition(id: $id) {
              id
              contextPath
              description
              diagram
              isSuspended
              key
              name
              startFormKey
              version
              versionTag
            }
          }
        `,
        variables: {
          id: `${getOneParams.id}`,
        },
        parseResponse: (response: any) => {
          return {
            data: response.data.processDefinition,
          };
        },
      };
    default:
      console.error(`Unsupported fetch type ${raFetchType}`);
      return {
        data: [],
      };
  }
};
