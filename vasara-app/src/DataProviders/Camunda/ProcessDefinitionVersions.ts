import { CamundaDataProvider, RaFetchType } from './types';
import gql from 'graphql-tag';
import { GetManyReferenceParams } from 'ra-core';

export const ProcessDefinitionVersionsDataProvider: CamundaDataProvider = (
  introspectionResults,
  raFetchType,
  resource,
  params
) => {
  switch (raFetchType) {
    case RaFetchType.GET_MANY_REFERENCE:
      const getManyReferenceParams = params as GetManyReferenceParams;

      return {
        query: gql`
          query processDefinitions($id: String!) {
            allVersions: processDefinitions {
              id
              contextPath
              description
              diagram
              isSuspended
              key
              name
              startFormKey
              version
              versionTag
            }
            processDefinition(id: $id) {
              id
              contextPath
              description
              diagram
              isSuspended
              key
              name
              startFormKey
              version
              versionTag
            }
          }
        `,
        variables: {
          id: `${getManyReferenceParams.id}`,
        },
        parseResponse: (response: any) => {
          const { allVersions, processDefinition } = response.data;
          const versions = allVersions.filter(
            (version: any) => version.id !== processDefinition.id && version.key === processDefinition.key
          );
          return {
            data: versions.slice(
              (getManyReferenceParams.pagination.page - 1) * getManyReferenceParams.pagination.perPage,
              getManyReferenceParams.pagination.page * getManyReferenceParams.pagination.perPage
            ),
            total: versions.length,
          };
        },
      };
    default:
      console.error(`Unsupported fetch type ${raFetchType}`);
      return {
        data: [],
      };
  }
};
