import { CamundaDataProvider, RaFetchType } from './types';
import { CreateParams, GetListParams, GetOneParams } from 'ra-core';
import gql from 'graphql-tag';

export const ProcessInstanceDataProvider: CamundaDataProvider = (
  introspectionResults,
  raFetchType,
  resource,
  params
) => {
  switch (raFetchType) {
    case RaFetchType.CREATE:
      const createParams = params as CreateParams;
      return {
        query: gql`
          mutation StartProcessInstanceByKey($key: String!) {
            startProcessInstanceByKey(key: $key) {
              id
            }
          }
        `,
        variables: { key: createParams.data.key },
        parseResponse: (response: any) => {
          return {
            data: response.data.startProcessInstanceByKey,
          };
        },
      };
    case RaFetchType.GET_LIST:
    case RaFetchType.GET_MANY:
    case RaFetchType.GET_MANY_REFERENCE:
      const getListParams = params as GetListParams;
      return {
        query: gql`
          query processInstances {
            processInstances {
              variables {
                key
                value
                valueType
              }
              tenantId
              processInstanceId
              processDefinitionId
              isEnded
              id
              caseInstanceId
              businessKey
            }
          }
        `,
        variables: {},
        parseResponse: (response: any) => {
          return {
            data: response.data.processInstances.slice(
              (getListParams.pagination.page - 1) * getListParams.pagination.perPage,
              getListParams.pagination.page * getListParams.pagination.perPage
            ),
            total: response.data.processInstances.length,
          };
        },
      };
    case RaFetchType.GET_ONE:
      const getOneParams = params as GetOneParams;
      return {
        query: gql`
          query processInstance($id: String!) {
            processInstance(id: $id) {
              variables {
                key
                value
                valueType
              }
              tenantId
              processInstanceId
              processDefinitionId
              isEnded
              id
              caseInstanceId
              businessKey
            }
          }
        `,
        variables: {
          id: `${getOneParams.id}`,
        },
        parseResponse: (response: any) => {
          return {
            data: response.data.processInstance,
          };
        },
      };
    default:
      console.error(`Unsupported fetch type ${raFetchType}`);
      return {
        data: [],
      };
  }
};
