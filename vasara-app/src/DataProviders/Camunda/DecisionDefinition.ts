import gql from 'graphql-tag';
import { CamundaDataProvider, RaFetchType } from './types';
import { CreateParams, GetListParams, GetOneParams } from 'ra-core';

export const DecisionDefinitionDataProvider: CamundaDataProvider = (
  introspectionResults,
  raFetchType,
  resource,
  params
) => {
  switch (raFetchType) {
    case RaFetchType.CREATE:
      const createParams = params as CreateParams;
      return {
        query: gql`
          mutation CreateDecisionDefinition($resource: String, $diagram: String!) {
            createDecisionDefinition(resource: $resource, diagram: $diagram) {
              id
            }
          }
        `,
        variables: createParams.data,
        parseResponse: (response: any) => {
          return {
            data: response.data.createDecisionDefinition,
          };
        },
      };
    case RaFetchType.GET_LIST:
    case RaFetchType.GET_MANY:
    case RaFetchType.GET_MANY_REFERENCE:
      const getListParams = params as GetListParams;
      const { filter } = getListParams;

      return {
        query: gql`
          query decisionDefinitions($latest: Boolean) {
            decisionDefinitions(latest: $latest) {
              id
              contextPath
              diagram
              key
              name
              version
              versionTag
            }
          }
        `,
        variables: {
          latest: filter.latest,
        },
        parseResponse: (response: any) => {
          return {
            data: response.data.decisionDefinitions.slice(
              (getListParams.pagination.page - 1) * getListParams.pagination.perPage,
              getListParams.pagination.page * getListParams.pagination.perPage
            ),
            total: response.data.decisionDefinitions.length,
          };
        },
      };
    case RaFetchType.GET_ONE:
      const getOneParams = params as GetOneParams;
      return {
        query: gql`
          query decisionDefinition($id: String!) {
            decisionDefinition(id: $id) {
              id
              contextPath
              diagram
              key
              name
              version
              versionTag
            }
          }
        `,
        variables: {
          id: `${getOneParams.id}`,
        },
        parseResponse: (response: any) => {
          return {
            data: response.data.decisionDefinition,
          };
        },
      };
    default:
      console.error(`Unsupported fetch type ${raFetchType}`);
      return {
        data: [],
      };
  }
};
