import buildDataProvider from 'ra-data-graphql';

import {
  CreateParams,
  DeleteManyParams,
  DeleteParams,
  GetListParams,
  GetManyParams,
  GetManyReferenceParams,
  UpdateManyParams,
  UpdateParams,
} from 'ra-core';

import { client } from '../client';
import { ProcessDefinitionDataProvider } from './ProcessDefinition';
import { DecisionDefinitionDataProvider } from './DecisionDefinition';
import { ProcessDefinitionVersionsDataProvider } from './ProcessDefinitionVersions';
import { ProcessInstanceDataProvider } from './ProcessInstance';
import { UserTaskDataProvider } from './UserTask';
import { ProcessDefinitionUserTaskDataProvider } from './ProcessDefinitionUserTasks';
import { GraphQLDataProvider, RaFetchType } from './types';

const buildQuery = (introspectionResults: any) => (
  raFetchType: RaFetchType,
  resource: string,
  params:
    | GetListParams
    | GetManyParams
    | GetManyReferenceParams
    | UpdateParams
    | UpdateManyParams
    | CreateParams
    | DeleteParams
    | DeleteManyParams
) => {
  switch (resource) {
    case 'ProcessDefinition':
      return ProcessDefinitionDataProvider(introspectionResults, raFetchType, resource, params);
    case 'DecisionDefinition':
      return DecisionDefinitionDataProvider(introspectionResults, raFetchType, resource, params);
    case 'ProcessDefinitionUserTask':
      return ProcessDefinitionUserTaskDataProvider(introspectionResults, raFetchType, resource, params);
    case 'ProcessDefinitionVersions':
      return ProcessDefinitionVersionsDataProvider(introspectionResults, raFetchType, resource, params);
    case 'ProcessInstance':
      return ProcessInstanceDataProvider(introspectionResults, raFetchType, resource, params);
    case 'UserTask':
      return UserTaskDataProvider(introspectionResults, raFetchType, resource, params);
    default:
      throw new Error(`Unknown resource ${resource}.`);
  }
};

const defaultOptions = {
  buildQuery,
};

export default async () => {
  const dataProvider = await buildDataProvider({ ...defaultOptions, client });
  const dataProviderImpl: GraphQLDataProvider = (raFetchType, resource, params) => {
    return dataProvider(raFetchType, resource, params);
  };
  return dataProviderImpl;
};
