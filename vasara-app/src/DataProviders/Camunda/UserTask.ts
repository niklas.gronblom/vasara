import { CamundaDataProvider, RaFetchType } from './types';
import { GetListParams, GetOneParams, UpdateParams } from 'ra-core';
import gql from 'graphql-tag';
import { BPMN, getUserTask, getUserTaskForm } from './helpers';

export const UserTaskDataProvider: CamundaDataProvider = (introspectionResults, raFetchType, resource, params) => {
  switch (raFetchType) {
    case RaFetchType.GET_LIST:
    case RaFetchType.GET_MANY:
    case RaFetchType.GET_MANY_REFERENCE:
      const getListParams = params as GetListParams;
      return {
        query: gql`
          query userTasks {
            tasks {
              assignee {
                email
                firstName
                id
                lastName
              }
              caseDefinitionId
              caseExecutionId
              caseInstanceId
              contextPath
              createTime
              description
              executionEntity {
                id
                isEnded
                tenantId
                processInstanceId
                isSuspended
              }
              executionId
              id
              formKey
              isSuspended
              variables {
                value
                valueType
                key
              }
              processInstanceId
              taskDefinitionKey
              tenantId
              processDefinition {
                id
                name
                version
              }
              processDefinitionId
              priority
              parentTaskId
              owner
              name
            }
          }
        `,
        variables: {},
        parseResponse: (response: any) => {
          const { tasks } = response.data;

          return {
            data: tasks.slice(
              (getListParams.pagination.page - 1) * getListParams.pagination.perPage,
              getListParams.pagination.page * getListParams.pagination.perPage
            ),
            total: tasks.length,
          };
        },
      };
    case RaFetchType.GET_ONE:
      const getOneParams = params as GetOneParams;
      return {
        query: gql`
          query userTask($id: String!) {
            task(id: $id) {
              assignee {
                email
                firstName
                id
                lastName
              }
              caseDefinitionId
              caseExecutionId
              caseInstanceId
              contextPath
              createTime
              description
              executionEntity {
                id
                isEnded
                tenantId
                processInstanceId
                isSuspended
              }
              executionId
              id
              formKey
              isSuspended
              variables {
                value
                valueType
                key
              }
              processDefinition {
                id
                name
                diagram
                version
              }
              processInstanceId
              taskDefinitionKey
              tenantId
              processDefinitionId
              priority
              parentTaskId
              owner
              name
            }
          }
        `,
        variables: {
          id: `${getOneParams.id}`,
        },
        parseResponse: (response: any) =>
          new Promise(async (resolve, reject) => {
            if (response.data.task) {
              const model = await BPMN(response.data.task.processDefinition.diagram);
              const task = getUserTask(model, response.data.task.taskDefinitionKey);
              resolve({
                data: {
                  ...response.data.task,
                  taskDefinition: task,
                },
              });
            } else {
              resolve({
                data: response.data.task,
              });
            }
          }),
      };
    case RaFetchType.UPDATE:
      const updateParams = params as UpdateParams;
      const form = getUserTaskForm(updateParams.data.taskDefinition);
      return {
        query: gql`
          mutation MyMutation($id: String!, $variables: [KeyValuePairInput!]) {
            completeTask(taskId: $id, variables: $variables) {
              variables {
                key
                value
                valueType
              }
            }
          }
        `,
        variables: {
          id: `${updateParams.id}`,
          variables: form.map((field: any) => {
            const variable = {
              key: field.id,
              value:
                // ensure that boolean is boolean
                field.type === 'boolean'
                  ? !!updateParams.data[field.id]
                  : // ensure that date is in Camunda supported format
                  field.type === 'date' && updateParams.data[field.id]
                  ? updateParams.data[field.id] + 'T00:00:00'
                  : // ensure that string is never null
                  field.type === 'date'
                  ? ''
                  : // ensure that string is never null
                  field.type === 'string' && !updateParams.data[field.id]
                  ? ''
                  : // default behavior
                    updateParams.data[field.id] || null,
              valueType: field.type === 'enum' ? 'STRING' : field.type.toUpperCase(),
            };
            console.log(variable);
            return variable;
          }),
        },
        parseResponse: (response: any) => {
          return {
            data: { id: null },
          };
        },
      };
    default:
      console.error(`Unsupported fetch type ${raFetchType}`);
      return {
        data: [],
      };
  }
};
