import { UserTask } from 'bpmn-moddle';
import DmnJS from 'dmn-js';
import DmnViewer from 'dmn-js/lib/NavigatedViewer';
import BpmnJS from 'bpmn-js';
import BpmnViewer from 'bpmn-js/lib/NavigatedViewer';
import camundaDmnExtensionModule from 'camunda-dmn-moddle';
import camundaDmnModdle from 'camunda-dmn-moddle/resources/camunda.json';
import camundaExtensionModule from 'camunda-bpmn-moddle/lib';
import camundaModdle from 'camunda-bpmn-moddle/resources/camunda.json';

export const BPMN = async (diagram: string) => {
  const model = new BpmnJS({
    additionalModules: [camundaExtensionModule],
    moddleExtensions: {
      camunda: camundaModdle,
    },
  });
  try {
    await model.importXML(diagram);
  } catch (e) {
    // nothing we can do
  }
  return model;
};

export const DMN = async (diagram: string) => {
  const model = new DmnJS({
    additionalModules: [camundaDmnExtensionModule],
    moddleExtensions: {
      camunda: camundaDmnModdle,
    },
  });
  try {
    await model.importXML(diagram);
  } catch (e) {
    // nothing we can do
  }
  return model;
};

export const DMNViewer = async (diagram: string) => {
  const model = new DmnViewer({
    additionalModules: [camundaDmnExtensionModule],
    moddleExtensions: {
      camunda: camundaDmnModdle,
    },
  });
  try {
    await model.importXML(diagram);
  } catch (e) {
    // nothing we can do
  }
  return model;
};

export const BPMNViewer = async (diagram: string) => {
  const model = new BpmnViewer({
    additionalModules: [camundaExtensionModule],
    moddleExtensions: {
      camunda: camundaModdle,
    },
  });
  try {
    await model.importXML(diagram);
  } catch (e) {
    // nothing we can do
  }
  return model;
};

export const getUserTaskEntities = (task: UserTask): string[] => {
  const entities: string[] = [];
  for (let association of (task.dataInputAssociations || []).concat(task.dataOutputAssociations || [])) {
    for (let value of association.targetRef.extensionElements?.values || []) {
      if ((value as any).$type === 'camunda:Properties') {
        for (let property of (value as any).values) {
          if (property.name === 'entity') {
            entities.push(property.value);
          }
        }
      }
    }
  }
  return entities;
};

export const getUserTaskForm = (task: UserTask): any[] => {
  if (task.extensionElements) {
    for (let i = 0; i < task.extensionElements.values.length; i++) {
      const formData = task.extensionElements.values[0];
      if ((formData as any).$type === 'camunda:FormData') {
        return (formData as any).fields || [];
      }
    }
  }
  return [];
};

export const getAllUserTasks = async (diagram: string) => {
  const model = await BPMN(diagram);
  const registry = model.get('elementRegistry');
  const tasks = registry
    .filter((element: any) => {
      return element.type === 'bpmn:UserTask';
    })
    .map((element: any) => {
      return element.businessObject;
    });
  return tasks;
};

export const getUserTask = (model: any, id: string) => {
  const registry = model.get('elementRegistry');
  const element = registry.get(id);
  return element ? element.businessObject : null;
};

export const getAllDataObjectReferences = (model: any) => {
  const registry = model.get('elementRegistry');
  return registry
    .filter((element: any) => {
      return element.type === 'bpmn:DataObjectReference';
    })
    .map((element: any) => {
      return element.businessObject;
    });
};

export const getAllEntities = (model: any) => {
  const objects = getAllDataObjectReferences(model);
  const entities = [];
  for (let obj of objects) {
    for (let value of obj.extensionElements?.values || []) {
      if ((value as any).$type === 'camunda:Properties') {
        for (let property of (value as any).values) {
          if (property.name === 'entity') {
            entities.push(property.value);
          }
        }
      }
    }
  }
  return entities;
};

export const arrayMove = (array: any[], from: number, to: number) => {
  array.splice(to, 0, array.splice(from, 1)[0]);
};
