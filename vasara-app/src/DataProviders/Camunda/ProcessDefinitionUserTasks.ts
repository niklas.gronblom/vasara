import { CamundaDataProvider, RaFetchType } from './types';
import gql from 'graphql-tag';
import { GetManyReferenceParams, GetOneParams } from 'ra-core';
import { getAllUserTasks, getUserTask, BPMN, getAllEntities } from './helpers';

export const ProcessDefinitionUserTaskDataProvider: CamundaDataProvider = (
  introspectionResults,
  raFetchType,
  resource,
  params
) => {
  switch (raFetchType) {
    case RaFetchType.GET_MANY_REFERENCE:
      const params_ = params as GetManyReferenceParams;
      if (!params_.id) {
        throw new Error('Missing parameter "id".');
      }
      return {
        query: gql`
          query processDefinition($id: String!) {
            processDefinition(id: $id) {
              id
              contextPath
              description
              diagram
              isSuspended
              key
              name
              startFormKey
              version
              versionTag
            }
          }
        `,
        variables: { id: params_.id },
        parseResponse: (response: any) =>
          new Promise((resolve, reject) => {
            const { id, name, version, diagram } = response.data.processDefinition;
            getAllUserTasks(diagram).then((tasks: any) => {
              resolve({
                data: tasks.map((task: any) => {
                  return {
                    ...task,
                    processDefinition: {
                      id,
                      name,
                      version,
                    },
                  };
                }),
                total: tasks.length,
              });
            });
          }),
      };
    case RaFetchType.GET_ONE:
      const getOneParams = params as GetOneParams;
      return {
        query: gql`
          query processDefinition($id: String!) {
            processDefinition(id: $id) {
              id
              name
              version
              diagram
            }
          }
        `,
        variables: {
          id: `${getOneParams.id}`,
        },
        parseResponse: async (response: any) => {
          const { id, name, version, diagram } = response.data.processDefinition;
          const model = await BPMN(diagram);
          const task = getUserTask(model, (params as any).filter.user_task_id);
          const entities = getAllEntities(model);
          return {
            data: {
              ...task,
              processDefinition: {
                id,
                name,
                version,
                entities: entities,
              },
            },
          };
        },
      };
    default:
      console.error(`Unsupported fetch type ${raFetchType}`);
      return {
        data: [],
      };
  }
};
