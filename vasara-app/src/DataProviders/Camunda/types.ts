import {
  CreateParams,
  CreateResult,
  DeleteManyParams,
  DeleteManyResult,
  DeleteParams,
  DeleteResult,
  GetListParams,
  GetListResult,
  GetManyParams,
  GetManyReferenceParams,
  GetManyReferenceResult,
  GetManyResult,
  GetOneParams,
  GetOneResult,
  UpdateManyParams,
  UpdateManyResult,
  UpdateParams,
  UpdateResult,
} from 'ra-core';

export enum RaFetchType {
  GET_LIST = 'GET_LIST',
  GET_ONE = 'GET_ONE',
  GET_MANY = 'GET_MANY',
  GET_MANY_REFERENCE = 'GET_MANY_REFERENCE',
  CREATE = 'CREATE',
  UPDATE = 'UPDATE',
  UPDATE_MANY = 'UPDATE_MANY',
  DELETE = 'DELETE',
  DELETE_MANY = 'DELETE_MANY',
}

export type GraphQLDataProvider = (
  raFetchType: RaFetchType,
  resource: string,
  params:
    | GetOneParams
    | GetListParams
    | GetManyParams
    | GetManyReferenceParams
    | UpdateParams
    | UpdateManyParams
    | CreateParams
    | DeleteParams
    | DeleteManyParams
) =>
  | GetOneResult
  | GetListResult
  | GetManyResult
  | GetManyReferenceResult
  | UpdateResult
  | UpdateManyResult
  | CreateResult
  | DeleteResult
  | DeleteManyResult;

export type CamundaDataProvider = (
  introspectionResults: any,
  raFetchType: RaFetchType,
  resource: string,
  params:
    | GetListParams
    | GetManyParams
    | GetManyReferenceParams
    | UpdateParams
    | UpdateManyParams
    | CreateParams
    | DeleteParams
    | DeleteManyParams
) =>
  | GetListResult
  | GetManyResult
  | GetManyReferenceResult
  | UpdateResult
  | UpdateManyResult
  | CreateResult
  | DeleteResult
  | DeleteManyResult;
