import { User } from 'oidc-client';
import { ApolloLink } from 'apollo-link';
import { onError } from 'apollo-link-error';
import { HttpLink } from 'apollo-link-http';
import { ApolloClient } from 'apollo-client';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';

import { getUser } from '../Auth/authProvider';
import { getEnv } from '../util/env';

// Runs before every request so the headers are always up-to-date
export const authLink = setContext((_, { headers }) => {
  const user = getUser();
  return {
    headers: {
      ...headers,
      Authorization: user !== null ? `Bearer ${((user as unknown) as User).access_token}` : '',
    },
  };
});

export const client = new ApolloClient({
  link: ApolloLink.from([
    authLink,
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors)
        graphQLErrors.forEach(({ message, locations, path }) => {
          console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`);
        });
      if (networkError) {
        console.log(`[Network error]: ${networkError}`);
      }
    }),
    new HttpLink({
      uri: getEnv('REACT_APP_GRAPHQL_API_URL'),
    }),
  ]),
  cache: new InMemoryCache(),
});
