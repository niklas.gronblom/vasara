import buildHasuraProvider, { buildQuery } from 'ra-data-hasura-graphql';

import { client } from './client';

interface HasuraDataProviderAndIntrospection {
  hasuraIntrospectionResults: any;
  hasuraDataProvider: any;
}

export const buildHasuraProviderWithIntrospection = async (): Promise<HasuraDataProviderAndIntrospection> => {
  return new Promise((resolve, reject) => {
    let hasuraIntrospectionResults: any;
    buildHasuraProvider({
      buildQuery: (introspectionResults: any) => {
        hasuraIntrospectionResults = introspectionResults;
        return buildQuery(introspectionResults);
      },
      client,
    })
      .then((hasuraDataProvider: any) => {
        resolve({
          hasuraIntrospectionResults,
          hasuraDataProvider,
        });
      })
      .catch((error: any) => reject(error));
  });
};
