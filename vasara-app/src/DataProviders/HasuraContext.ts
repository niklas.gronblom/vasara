import { createContext } from 'react';

export const IntrospectionContext = createContext({});
