import React from 'react';

import { List, Datagrid } from 'react-admin';

import ListGuesserField from '../Form/ListGuesserField';
import { PAGE_SIZE } from '../util/constants';
import { stripHasuraSchemaPrefix, toLabel } from '../util/helpers';

export const EntityListComponent = (props: any) => {
  return (
    <List {...props} perPage={PAGE_SIZE} title={toLabel(stripHasuraSchemaPrefix(props.resource))}>
      <Datagrid rowClick="show">
        <ListGuesserField label="Identifier" />
      </Datagrid>
    </List>
  );
};
