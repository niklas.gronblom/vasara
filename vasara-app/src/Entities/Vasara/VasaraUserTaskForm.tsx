import React, { cloneElement, useEffect, useState } from 'react';
import { parse } from 'query-string';
import {
  Create,
  DeleteButton,
  Error,
  Loading,
  SaveButton,
  SimpleForm,
  Toolbar,
  useDataProvider,
  useEditController,
} from 'react-admin';

import FormBuilder from '../../FormBuilder/FormBuilder';
import { FormBuilderContext } from '../../FormBuilder/FormBuilderContext';

const FormToolbar = (props: any) => {
  return (
    <Toolbar {...props}>
      <SaveButton label="ra.action.save" redirect={props.redirect} submitOnEnter={false} />
      <DeleteButton label="ra.action.delete" redirect={props.redirect} submitOnEnter={false} />
    </Toolbar>
  );
};

export const FormCreate = (props: any) => {
  const { processDefinitionId, processDefinitionName, processDefinitionVersion, userTaskId } = parse(
    props.location.search
  );
  const redirect = processDefinitionId ? `/ProcessDefinition/${processDefinitionId}/show` : 'list';
  const initialValues = {
    process_definition_name: processDefinitionName,
    process_definition_version: processDefinitionVersion,
    user_task_id: userTaskId,
  };

  const dataProvider = useDataProvider();
  const [context, setContext] = useState();
  const [loadingContext, setLoadingContext] = useState(true);
  const [template, setTemplate] = useState({});
  const [loadingTemplate, setLoadingTemplate] = useState(true);
  const [error, setError] = useState();

  useEffect(() => {
    dataProvider
      .getOne('ProcessDefinitionUserTask', {
        id: processDefinitionId,
        filter: {
          user_task_id: userTaskId,
        },
      })
      .then(({ data }: any) => {
        setContext(data);
        setLoadingContext(false);
      })
      .catch((error: any) => {
        setError(error);
        setLoadingContext(false);
      });
    dataProvider
      .getList('vasara_user_task_form', {
        filter: {
          user_task_id: userTaskId,
          process_definition_name: processDefinitionName,
        },
        order_by: {
          process_definition_version: 'desc',
        },
      })
      .then(({ data }: any) => {
        setTemplate(data.length ? data[0] : {});
        setLoadingTemplate(false);
      })
      .catch((error: any) => {
        setError(error);
        setLoadingTemplate(false);
      });
  }, [dataProvider, processDefinitionId, processDefinitionName, userTaskId]);

  if (loadingContext || loadingTemplate) {
    return <Loading />;
  }

  if (error) {
    return <Error error={error} />;
  }

  if ((template as any)['id']) {
    delete (template as any)['id'];
  }

  return (
    <FormBuilderContext.Provider value={context as any}>
      <Create {...props}>
        <SimpleForm initialValues={{ ...template, ...initialValues }} redirect={redirect}>
          <FormBuilder />
        </SimpleForm>
      </Create>
    </FormBuilderContext.Provider>
  );
};

const EditWithContext = (props: any) => {
  const { processDefinitionId } = parse(props.location.search);
  const {
    basePath, // deduced from the location, useful for action buttons
    defaultTitle, // the translated title based on the resource, e.g. 'Post #123'
    loaded, // boolean that is false until the record is available
    loading, // boolean that is true on mount, and false once the record was fetched
    record, // record fetched via dataProvider.getOne() based on the id from the location
    resource, // the resource name, deduced from the location. e.g. 'posts'
    save, // the update callback, to be passed to the underlying form as submit handler
    saving, // boolean that becomes true when the dataProvider is called to update the record
    version, // integer used by the refresh feature
  } = useEditController(props);

  const dataProvider = useDataProvider();
  const [context, setContext] = useState();
  const [loading_, setLoading] = useState(!saving);
  const [error, setError] = useState();

  useEffect(() => {
    if (record && !saving) {
      dataProvider
        .getOne('ProcessDefinitionUserTask', {
          id: processDefinitionId,
          filter: {
            user_task_id: record.user_task_id,
          },
        })
        .then(({ data }: any) => {
          setContext(data);
          setLoading(false);
        })
        .catch((error: any) => {
          setError(error);
          setLoading(false);
        });
    }
  }, [dataProvider, processDefinitionId, record, saving]);

  if (loading || !loaded || !record || loading_) {
    return <Loading />;
  }

  if (error) {
    return <Error error={error} />;
  }

  const redirect_ = processDefinitionId ? `/ProcessDefinition/${processDefinitionId}/show` : 'list';

  return (
    <FormBuilderContext.Provider value={context as any}>
      <h1>{context ? (context as any).name : defaultTitle}</h1>
      {cloneElement(props.children, {
        basePath,
        record,
        redirect: redirect_,
        resource,
        save,
        saving,
        version,
      })}
    </FormBuilderContext.Provider>
  );
};

export const FormEdit = (props: any) => {
  const { processDefinitionId } = parse(props.location.search);
  const redirect = processDefinitionId ? `/ProcessDefinition/${processDefinitionId}/show` : 'list';

  return (
    <EditWithContext {...props} undoable={false}>
      <SimpleForm redirect={redirect} toolbar={<FormToolbar redirect={redirect} />}>
        <FormBuilder />
      </SimpleForm>
    </EditWithContext>
  );
};
