import React, { cloneElement, useCallback, useEffect, useState } from 'react';
import {
  Datagrid,
  Error,
  FileField,
  FileInput,
  List,
  Loading,
  Show,
  SimpleForm,
  SimpleShowLayout,
  TextField,
  useCreateController,
  useDataProvider,
} from 'react-admin';
import { convertFileToText, toLabel } from '../../util/helpers';
import DmnField from '../../Form/DmnField';

export const DecisionDefinitionList = (props: any) => {
  return (
    <List {...props} filterDefaultValues={{ latest: true }} title={toLabel(props.resource)} bulkActionButtons={false}>
      <Datagrid rowClick="show">
        <TextField source="name" />
      </Datagrid>
    </List>
  );
};

const DecisionDefinitionCreateController = (props: any) => {
  const {
    basePath, // deduced from the location, useful for action buttons
    defaultTitle, // the translated title based on the resource, e.g. 'Create Post'
    record, // empty object, unless some values were passed in the location state to prefill the form
    redirect, // the default redirection route. Defaults to 'edit', unless the resource has no edit view, in which case it's 'list'
    resource, // the resource name, deduced from the location. e.g. 'posts'
    save, // the create callback, to be passed to the underlying form as submit handler
    saving, // boolean that becomes true when the dataProvider is called to create the record
    version, // integer used by the refresh feature
  } = useCreateController(props);

  const saveWithFile = useCallback(
    (data: any, redirectTo: string, { onSuccess, onFailure } = {}) => {
      convertFileToText(data.diagram).then((diagram: any) => {
        const resource = data.diagram.rawFile.name;
        save(
          {
            resource,
            diagram,
          },
          redirectTo,
          { onSuccess, onFailure }
        );
      });
    },
    [save]
  );

  return (
    <div>
      <h1>{defaultTitle}</h1>
      {cloneElement(props.children, {
        basePath,
        record,
        redirect,
        resource,
        save: saveWithFile,
        saving,
        version,
      })}
    </div>
  );
};

export const DecisionDefinitionCreate = (props: any) => {
  return (
    <DecisionDefinitionCreateController {...props}>
      <SimpleForm>
        <FileInput source="diagram" label="Diagram">
          <FileField source="src" title="title" />
        </FileInput>
      </SimpleForm>
    </DecisionDefinitionCreateController>
  );
};

export const DecisionDefinitionShow = (props: any) => {
  const { id } = props;
  const dataProvider = useDataProvider();
  const [context, setContext] = useState();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState();

  useEffect(() => {
    dataProvider
      .getOne('DecisionDefinition', { id: id })
      .then((data: any) => {
        setContext(data);
        setLoading(false);
      })
      .catch((error: any) => {
        setError(error);
        setLoading(false);
      });
  }, [dataProvider, id]);

  if (loading || !context) {
    return <Loading />;
  }

  if (error) {
    return <Error error={error} />;
  }

  return (
    <Show {...props} title={`${toLabel(props.resource)} #${props.id}`}>
      <SimpleShowLayout>
        <TextField source="name" />
        <DmnField source="diagram" />
      </SimpleShowLayout>
    </Show>
  );
};
