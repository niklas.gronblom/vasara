import React from 'react';
import { parse } from 'query-string';
import { Create, Datagrid, List, SimpleForm, TextField, TextInput } from 'react-admin';
import { toLabel } from '../../util/helpers';

export const ProcessInstanceList = (props: any) => {
  return (
    <List {...props} title={toLabel(props.resource)} bulkActionButtons={false}>
      <Datagrid>
        <TextField source="id" />
        <TextField source="name" />
      </Datagrid>
    </List>
  );
};

export const ProcessInstanceCreate = (props: any) => {
  const { processDefinitionId } = parse(props.location.search);
  return (
    <Create {...props}>
      <SimpleForm defaultValue={{ key: processDefinitionId }}>
        <TextInput source="key" label="Process definition" />
      </SimpleForm>
    </Create>
  );
};
