import React, { cloneElement, useCallback, useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Datagrid,
  EditButton,
  Error,
  List,
  Loading,
  SaveButton,
  Show,
  ShowButton,
  SimpleForm,
  TextField,
  Toolbar,
  TopToolbar,
  useDataProvider,
  useEditController,
  useMutation,
  useRedirect,
  useTranslate,
} from 'react-admin';
import { base64ToBytea, convertFileToBase64 } from '../../util/helpers';
import Form from '../../FormBuilder/Form';
import { FormUserTaskContext } from '../../FormBuilder/FormBuilderContext';
import { AccordionBpmnField } from '../../Form/Fields/Accordion';
import { getUserName } from '../../Auth/authProvider';

export const UserTaskList = (props: any) => {
  return (
    <List {...props} title="ra.menu.userTask" bulkActionButtons={false} exporter={false}>
      <Datagrid rowClick="edit">
        <TextField source="processDefinition.name" label="Process" />
        <TextField source="name" label="Task" />
      </Datagrid>
    </List>
  );
};

export const UserTaskCreate = () => {
  const redirect = useRedirect();
  setTimeout(() => {
    redirect('/ProcessDefinition');
  });
  return null;
};

const UserTaskActions = ({ basePath, data, resource }: any) => {
  const translate = useTranslate();
  return (
    <TopToolbar>
      <EditButton basePath={basePath} record={data} label={translate('ra.action.claim')} />
    </TopToolbar>
  );
};

export const UserTaskTitle = ({ record }: any) => {
  return <span>{record.name}</span>;
};

export const UserTaskShow = (props: any) => {
  return (
    <Show {...props} title={<UserTaskTitle />} actions={<UserTaskActions />}>
      {/*<SimpleShowLayout>*/}
      {/*  <BpmnField source="processDefinition.diagram" />*/}
      {/*</SimpleShowLayout>*/}
    </Show>
  );
};

const TaskEdit = (props: any) => {
  const {
    basePath, // deduced from the location, useful for action buttons
    // defaultTitle, // the translated title based on the resource, e.g. 'Post #123'
    loaded, // boolean that is false until the record is available
    loading, // boolean that is true on mount, and false once the record was fetched
    record, // record fetched via dataProvider.getOne() based on the id from the location
    redirect, // the default redirection route. Defaults to 'list'
    resource, // the resource name, deduced from the location. e.g. 'posts'
    save, // the update callback, to be passed to the underlying form as submit handler
    saving, // boolean that becomes true when the dataProvider is called to update the record
    version, // integer used by the refresh feature
  } = useEditController(props);

  const dataProvider = useDataProvider();
  const [userTask, setUserTask] = useState({});
  const [userTaskForm, setUserTaskForm] = useState({});
  const [loading_, setLoading] = useState(true);
  const [error, setError] = useState();

  useEffect(() => {
    if (!record) {
      return;
    }
    (async () => {
      // TODO: Refactor for parallel fetch
      const { taskDefinitionKey, processDefinition } = record;
      try {
        const { data: userTask } = await dataProvider.getOne('ProcessDefinitionUserTask', {
          id: processDefinition.id,
          filter: {
            user_task_id: taskDefinitionKey,
          },
        });
        const { data: userTaskForm } = await dataProvider.getOne('vasara_user_task_form', {
          filter: {
            user_task_id: taskDefinitionKey,
            process_definition_name: processDefinition.name,
            process_definition_version: processDefinition.version,
          },
        });
        // const { data: attachments } = await dataProvider.getList("Attachment", {
        //   filter: {
        //     ProcessInstances: {
        //       format: "hasura-raw-query",
        //       value: {
        //         process_instance_id: { _eq: record.processInstanceId },
        //       },
        //     },
        //   },
        // });
        // const entities: any = { Attachments: attachments };
        const entities: any = {};
        record.entities = entities;
        for (const entity of userTask.processDefinition.entities) {
          const { data: instances } = await dataProvider.getList(entity, {
            filter: {
              ProcessInstances: {
                format: 'hasura-raw-query',
                value: {
                  process_instance_id: { _eq: record.processInstanceId },
                },
              },
            },
          });
          if (instances.length) {
            entities[entity] = instances[0];
          }
        }
        const fields = ((userTaskForm as any).schema || []).reduce((acc: any, cur: any) =>
          ((acc || {}).fields || []).concat((cur || {}).fields || [])
        );
        for (const field of fields) {
          for (const source of field.sources || []) {
            const parts = source.split('.');
            if (parts.length >= 2 && parts[0] === 'context') {
              for (const variable of record.variables || {}) {
                if (variable.key === parts[1]) {
                  record[field.id] = variable.value;
                }
              }
            } else if (parts.length >= 2 && record.entities[parts[0]]) {
              if (parts.length === 2 && record.entities[parts[0]][parts[1]]) {
                record[field.id] = record.entities[parts[0]][parts[1]];
              } else if (parts.length === 3 && parts[2] === '{}') {
                for (const key of Object.keys(record.entities[parts[0]][parts[1]])) {
                  if (key === field.label) {
                    record[field.id] = record.entities[parts[0]][parts[1]][key];
                  }
                }
              }
            }
          }
        }
        setUserTaskForm(userTaskForm);
        setUserTask(userTask);
      } catch (e) {
        setError(e);
      } finally {
        setLoading(false);
      }
    })();
  }, [dataProvider, record]);

  const [mutate] = useMutation();

  const saveAll = useCallback(
    (data: any, redirectTo: string, { onSuccess, onFailure } = {}) => {
      (async () => {
        const create = (resource: any, data: any) =>
          mutate({
            type: 'create',
            resource: resource,
            payload: { data },
          });
        const update = (resource: any, id: any, data: any) =>
          mutate({
            type: 'update',
            resource: resource,
            payload: { id, data },
          });
        const userName = getUserName();
        const attachments: Array<Record<string, any>> = [];
        const entities: Record<string, Record<string, any>> = { context: {} };
        const fields = ((userTaskForm as any).schema || []).reduce((acc: any, cur: any) =>
          ((acc || {}).fields || []).concat((cur || {}).fields || [])
        );
        for (const field of fields) {
          for (const source of field.sources || []) {
            const parts = source.split('.');
            if (parts.length >= 2 && parts[0] && data[field.id]) {
              const created = new Date().toISOString();
              let current: any = {
                metadata: {
                  author: userName,
                  created: created,
                  modified: created,
                  archived: null,
                },
              };
              if (parts[0] === 'Attachment') {
                // XXX: Attachment is special for now
                current = {
                  ...current,
                  allowedRolesAndUsers: {
                    data: userName
                      ? [
                          {
                            principal: userName,
                            read: true,
                            write: true,
                          },
                        ]
                      : [],
                  },
                  ProcessInstances: {
                    data: [
                      {
                        process_instance_id: data.processInstanceId,
                      },
                    ],
                  },
                };
                attachments.push(current);
              } else if (!entities[parts[0]]) {
                current = entities[parts[0]] = record.entities[parts[0]]
                  ? {
                      id: record.entities[parts[0]].id,
                      metadata: {
                        ...record.entities[parts[0]].metadata,
                        modified: created,
                      },
                    }
                  : {
                      ...current,
                      ProcessInstances: {
                        data: [
                          {
                            process_instance_id: data.processInstanceId,
                          },
                        ],
                      },
                      allowedRolesAndUsers: {
                        data: userName
                          ? [
                              {
                                principal: userName,
                                read: true,
                                write: true,
                              },
                            ]
                          : [],
                      },
                    };
              } else {
                current = entities[parts[0]];
              }
              if (field.type === 'file') {
                const a = await convertFileToBase64(data[field.id]);
                current[parts[1]] = base64ToBytea(a);
                current['metadata'][parts[1]] = {
                  name: data[field.id].rawFile.name,
                  type: data[field.id].rawFile.type,
                  size: data[field.id].rawFile.size,
                };
              } else if (parts.length === 3 && parts[2] === '{}') {
                // JSONB
                if (!current[parts[1]]) {
                  if (parts[0] !== 'Attachment' && record.entities[parts[0]]) {
                    current[parts[1]] = record.entities[parts[0]][parts[1]] || {};
                  } else {
                    current[parts[1]] = {};
                  }
                }
                current[parts[1]][field.label] = data[field.id];
              } else {
                current[parts[1]] = data[field.id];
              }
            }
          }
        }
        // Add connections
        // https://hasura.io/docs/1.0/graphql/core/mutations/insert.html#many-to-many-relationships
        for (const resource in entities) {
          if (resource !== 'context') {
            const current = entities[resource];
            for (const attachment of attachments) {
              if (!current['Attachments']) {
                current['Attachments'] = { data: [] };
              }
              current['Attachments']['data'].push({ Attachment: { data: attachment } });
            }
          }
        }
        // Create
        for (const resource in entities) {
          if (resource !== 'context') {
            if (record.entities[resource]) {
              const id = record.entities[resource].id;
              delete record.entities[resource].id;
              update(resource, id, entities[resource]);
            } else {
              create(resource, entities[resource]);
            }
          }
        }
        // Complete
        save(
          {
            ...data,
            ...entities['context'],
          },
          redirectTo,
          { onSuccess, onFailure }
        );
      })();
    },
    [save, mutate, userTaskForm, record]
  );

  if (loading || !loaded || !record || loading_) {
    return <Loading />;
  }

  if (error) {
    return <Error error={error} />;
  }

  return (
    <FormUserTaskContext.Provider value={userTask}>
      <h1>{record.name}</h1>
      {cloneElement(props.children, {
        basePath,
        record: { ...userTaskForm, ...record },
        redirect,
        resource,
        save: saveAll,
        saving,
        version,
      })}
    </FormUserTaskContext.Provider>
  );
};

const UserTaskEditToolbar = (props: any) => {
  const translate = useTranslate();
  return (
    <Toolbar {...props}>
      <SaveButton label={translate('ra.action.submit')} redirect="show" submitOnEnter={true} />i
    </Toolbar>
  );
};

export const UserTaskEdit = (props: any) => {
  return (
    <TaskEdit {...props} undoable={false} redirect="list">
      <SimpleForm toolbar={<UserTaskEditToolbar />}>
        <AccordionBpmnField source="processDefinition.diagram" label="Työnkulku" />
        <UserTaskForm />
      </SimpleForm>
    </TaskEdit>
  );
};

const useStyles = makeStyles({
  button: {
    fontWeight: 'bold',
    marginTop: '1.5em',
    marginBottom: '1.5em',
  },
});

export const UserTaskForm = (props: any) => {
  const classes = useStyles();
  return (
    <>
      {Object.keys((props.record && props.record.entities) || {}).map((key: string) => {
        return (
          <ShowButton
            className={classes.button}
            to={{ pathname: `/${key}/${props.record.entities[key].id}/show` }}
            color="primary"
            label={`Avaa ${props.record.processDefinition.name}`}
            key={props.record.entities[key].id}
            size={'large'}
          />
        );
      })}
      <Form />
    </>
  );
};
