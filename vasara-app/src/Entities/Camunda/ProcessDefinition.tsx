import React, { cloneElement, useCallback, useEffect, useState } from 'react';
import {
  Button,
  Datagrid,
  Error,
  FileField,
  FileInput,
  Link,
  List,
  Loading,
  ReferenceManyField,
  Show,
  SimpleForm,
  SimpleShowLayout,
  TextField,
  useCreateController,
  useDataProvider,
  useMutation,
  useRedirect,
  useTranslate,
} from 'react-admin';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import PlayCircleFilledWhiteIcon from '@material-ui/icons/PlayCircleFilledWhite';
import { convertFileToText } from '../../util/helpers';
import { AccordionBpmnField, AccordionReferenceManyField } from '../../Form/Fields/Accordion';

export const AddOrEditFormButton = ({ record }: any) => {
  const { id, processDefinition } = record;
  const dataProvider = useDataProvider();
  const [context, setContext] = useState();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState();

  useEffect(() => {
    dataProvider
      .getList('vasara_user_task_form', {
        filter: {
          user_task_id: id,
          process_definition_name: processDefinition.name,
          process_definition_version: processDefinition.version,
        },
      })
      .then(({ data }: any) => {
        setContext(data);
        setLoading(false);
      })
      .catch((error: any) => {
        setError(error);
        setLoading(false);
      });
  }, [dataProvider, id, processDefinition]);

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <Error error={error} />;
  }

  // @ts-ignore
  if (context.length) {
    return (
      <Link
        to={{
          // @ts-ignore
          pathname: `/vasara_user_task_form/${context[0].id}`,
          search: `?processDefinitionId=${processDefinition.id}`,
        }}
      >
        <Button label="ra.action.edit">
          <EditIcon />
        </Button>
      </Link>
    );
  } else {
    return (
      <Link
        to={{
          pathname: '/vasara_user_task_form/create',
          search: `?processDefinitionId=${processDefinition.id}&processDefinitionName=${processDefinition.name}&processDefinitionVersion=${processDefinition.version}&userTaskId=${id}`,
        }}
      >
        <Button label="ra.action.add">
          <AddIcon />
        </Button>
      </Link>
    );
  }
};

const StartButton = ({ record }: any) => {
  const redirect = useRedirect();
  const [start, { loading }] = useMutation(
    {
      type: 'create',
      resource: 'ProcessInstance',
      payload: { data: { key: record.key } },
    },
    {
      onSuccess: ({ data }: any) => {
        redirect('/UserTask');
      },
    }
  );
  return (
    <Button label="ra.action.start" onClick={start} disabled={loading}>
      <PlayCircleFilledWhiteIcon />
    </Button>
  );
};

export const ProcessDefinitionList = (props: any) => {
  const translate = useTranslate();
  return (
    <List
      {...props}
      filterDefaultValues={{ latest: true }}
      title={translate('ra.view.processDefinitionList')}
      bulkActionButtons={false}
      exporter={false}
    >
      <Datagrid rowClick="show">
        <TextField source="name" label={translate('ra.column.processDefinitionName')} />
        <StartButton />
      </Datagrid>
    </List>
  );
};

export const ProcessDefinitionTitle = ({ record }: any) => {
  return <span>{record.name}</span>;
};

export const ProcessDefinitionShow = (props: any) => {
  const { id } = props;
  const translate = useTranslate();
  const dataProvider = useDataProvider();
  const [context, setContext] = useState();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState();

  useEffect(() => {
    dataProvider
      .getOne('ProcessDefinition', { id: id })
      .then((data: any) => {
        setContext(data);
        setLoading(false);
      })
      .catch((error: any) => {
        setError(error);
        setLoading(false);
      });
  }, [dataProvider, id]);

  if (loading || !context) {
    return <Loading />;
  }

  if (error) {
    return <Error error={error} />;
  }

  return (
    <Show {...props} title={<ProcessDefinitionTitle />}>
      <SimpleShowLayout>
        <AccordionBpmnField source="diagram" label={translate('ra.column.diagram')} />
        <ReferenceManyField label="" reference="ProcessDefinitionUserTask" target="id">
          <Datagrid>
            <TextField source="name" sortable={false} label="" />
            <AddOrEditFormButton />
          </Datagrid>
        </ReferenceManyField>
        <AccordionReferenceManyField
          label={translate('ra.view.versions')}
          reference="ProcessDefinitionVersions"
          target="key"
        >
          <Datagrid rowClick={(id: any) => `/ProcessDefinition/${id}/show`}>
            <TextField source="version" sortable={false} />
            <TextField source="id" sortable={false} />
          </Datagrid>
        </AccordionReferenceManyField>
      </SimpleShowLayout>
    </Show>
  );
};

const ProcessDefinitionCreateController = (props: any) => {
  const {
    basePath, // deduced from the location, useful for action buttons
    defaultTitle, // the translated title based on the resource, e.g. 'Create Post'
    record, // empty object, unless some values were passed in the location state to prefill the form
    redirect, // the default redirection route. Defaults to 'edit', unless the resource has no edit view, in which case it's 'list'
    resource, // the resource name, deduced from the location. e.g. 'posts'
    save, // the create callback, to be passed to the underlying form as submit handler
    saving, // boolean that becomes true when the dataProvider is called to create the record
    version, // integer used by the refresh feature
  } = useCreateController(props);

  const saveWithFile = useCallback(
    (data: any, redirectTo: string, { onSuccess, onFailure } = {}) => {
      convertFileToText(data.diagram).then((diagram: any) => {
        const resource = data.diagram.rawFile.name;
        save(
          {
            resource,
            diagram,
          },
          redirectTo,
          { onSuccess, onFailure }
        );
      });
    },
    [save]
  );

  return (
    <div>
      <h1>{defaultTitle}</h1>
      {cloneElement(props.children, {
        basePath,
        record,
        redirect,
        resource,
        save: saveWithFile,
        saving,
        version,
      })}
    </div>
  );
};

export const ProcessDefinitionCreate = (props: any) => {
  return (
    <ProcessDefinitionCreateController {...props}>
      <SimpleForm>
        <FileInput source="diagram" label="Diagram">
          <FileField source="src" title="title" />
        </FileInput>
      </SimpleForm>
    </ProcessDefinitionCreateController>
  );
};
