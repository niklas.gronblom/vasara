import React from 'react';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import {
  Button,
  ChipField,
  EditButton,
  Labeled,
  Link,
  ReferenceArrayField,
  ReferenceField,
  ReferenceManyFieldController,
  ReferenceManyField,
  Show,
  Tab,
  TabbedShowLayout,
  SingleFieldList,
  TextField,
  TopToolbar,
} from 'react-admin';

import ListGuesserField from '../Form/ListGuesserField';
import { SCALARCOMPONENTS_MAP } from '../util/constants';
import {
  isFkField,
  isM2MField,
  isM2OField,
  isO2MField,
  getTargetResourceFieldName,
  getReferenceResourceName,
  isSkippableField,
  isScalarField,
  getFieldTypeName,
  isAggregateFieldName,
  getMainIdentifierColumn,
  getOtherReferenceFields,
  getReferenceFields,
  getSourceResourceFieldName,
  toLabel,
} from '../util/helpers';
import { ResourceField, ResourceSchema } from '../util/types';

const EntityShowActions = ({ basePath, data }: any) => {
  return (
    <TopToolbar>
      <Link to={basePath}>
        <Button size="small" startIcon={<ArrowBackIcon />} label="ra.action.back" />
      </Link>
      <EditButton basePath={basePath} record={data} />
    </TopToolbar>
  );
};

export const EntityShowComponent = (resourceName: string, introspection: any) => (props: any) => {
  const schemata: Map<string, ResourceSchema> = new Map(
    introspection.types
      .filter((type: ResourceSchema) => type.kind === 'OBJECT')
      .map((type: ResourceSchema) => [type.name, type])
  );
  const schema = schemata.get(resourceName);

  return schema ? (
    <Show {...props} actions={<EntityShowActions />}>
      <TabbedShowLayout>
        <Tab label={'Default'}>
          {schema.fields.map((field: ResourceField, i: number) => {
            if (isSkippableField(field) || isFkField(field) || isAggregateFieldName(field.name)) {
              return null;
            } else if (isScalarField(field)) {
              const typeName = getFieldTypeName(field.type);
              const FieldComponent = SCALARCOMPONENTS_MAP[typeName] || TextField;
              return <FieldComponent key={i} source={field.name} label={toLabel(field.name)} />;
            } else {
              return null;
            }
          })}
        </Tab>
        <Tab label={'Relations'}>
          {schema.fields.map((field: ResourceField, i: number) => {
            if (isSkippableField(field) || isFkField(field) || isAggregateFieldName(field.name)) {
              return null;
            } else if (isM2MField(field)) {
              const referenceResourceName = getReferenceResourceName(field);
              const referenceResourceSchema = schemata.get(referenceResourceName) as ResourceSchema;
              const referenceFieldName = getTargetResourceFieldName(field, resourceName, schemata);
              const beyondReferenceResourceFieldCandidates = getOtherReferenceFields(
                schema.name,
                referenceResourceSchema
              );
              const beyondReferenceResourceName = getReferenceResourceName(beyondReferenceResourceFieldCandidates[0]);
              const beyondReferenceFieldCandidates = getReferenceFields(
                beyondReferenceResourceName,
                referenceResourceSchema
              );

              if (beyondReferenceFieldCandidates.length === 0) {
                return null;
              }

              const beyondReferenceFieldName = `${beyondReferenceFieldCandidates[0].name}Id`;
              const beyondReferenceMainIdentifierColumn = getMainIdentifierColumn(
                beyondReferenceResourceName,
                schemata
              );

              // console.log(
              //   `M2M ${field.name}: ${referenceResourceName}.${referenceFieldName} => ${referenceResourceName}.${beyondReferenceFieldName} => ${beyondReferenceResourceName}.id (${beyondReferenceMainIdentifierColumn})`
              // );

              return (
                <ReferenceManyFieldController
                  key={`${i}`}
                  reference={referenceResourceName}
                  source={'id'}
                  target={referenceFieldName}
                >
                  {(record: any) => {
                    const beyondReferenceRecord = {
                      ...record,
                      ids: Object.keys(record.data || {}).map(key => record.data[key][beyondReferenceFieldName]),
                    };
                    const referenceBasePath = record.basePath.replace(record.resource, beyondReferenceResourceName);
                    return (
                      <Labeled label={toLabel(field.name)}>
                        <ReferenceArrayField
                          key={`${i}-${referenceFieldName}`}
                          basePath={(record.referenceBasePath || record.basePath).replace(
                            referenceResourceName,
                            beyondReferenceResourceName
                          )}
                          reference={beyondReferenceResourceName}
                          resource={referenceResourceName}
                          record={beyondReferenceRecord}
                          source={'ids'}
                          target={'id'}
                        >
                          <SingleFieldList basePath={referenceBasePath} linkType="show">
                            <ChipField source={beyondReferenceMainIdentifierColumn} />
                          </SingleFieldList>
                        </ReferenceArrayField>
                      </Labeled>
                    );
                  }}
                </ReferenceManyFieldController>
              );
            } else if (isM2OField(field)) {
              const referenceResourceName = getReferenceResourceName(field);
              const sourceFieldName = getSourceResourceFieldName(field, resourceName, schemata);

              if (!sourceFieldName) {
                return null;
              }

              // console.log(`M2O ${field.name}: ${schema.name}.${sourceFieldName} => ${referenceResourceName}.id`);

              return (
                <ReferenceField
                  key={i}
                  label={toLabel(field.name)}
                  source={sourceFieldName}
                  reference={referenceResourceName}
                  link="show"
                >
                  <ListGuesserField />
                </ReferenceField>
              );
            } else if (isO2MField(field)) {
              const referenceResourceName = getReferenceResourceName(field);
              const targetFieldName = getTargetResourceFieldName(field, resourceName, schemata);
              const mainIdentifierColumn = getMainIdentifierColumn(referenceResourceName, schemata);

              if (!targetFieldName) {
                return null;
              }

              // console.log(`O2M ${field.name}: ${referenceResourceName}.${targetFieldName} (${mainIdentifierColumn})`);

              return (
                <ReferenceManyField
                  key={i}
                  label={toLabel(field.name)}
                  reference={referenceResourceName}
                  target={targetFieldName}
                >
                  <SingleFieldList linkType="show">
                    <ChipField source={mainIdentifierColumn} />
                  </SingleFieldList>
                </ReferenceManyField>
              );
            } else {
              // console.log(`Miss ${field.name}`);
              return null;
            }
          })}
        </Tab>
      </TabbedShowLayout>
    </Show>
  ) : null;
};
