import React from 'react';

import { MAIN_IDENTIFIER_COLUMNS } from '../util/constants';

type ListGuesserProps = {
  label: string;
  record?: any;
};

export default class ListGuesserField extends React.Component<ListGuesserProps, {}> {
  static defaultProps = {
    label: 'Identifier',
  };

  render() {
    const { record } = this.props;

    // The source (a.k.a column) which will be used to display the list-item
    const source = Object.keys(record).find((key: string) => MAIN_IDENTIFIER_COLUMNS.includes(key)) || 'Description';

    return <span>{record[source]}</span>;
  }
}
