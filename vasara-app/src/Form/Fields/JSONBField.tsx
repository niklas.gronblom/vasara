import * as React from 'react';
import PropTypes from 'prop-types';

const JSONBField = ({ source, record = {} }: any) => {
  return (
    <>
      {Object.keys(record[source]).map((key: string) => {
        return (
          <div className="" key={key}>
            <label
              className="MuiFormLabel-root MuiInputLabel-root RaLabeled-label-51 MuiInputLabel-animated MuiInputLabel-shrink MuiInputLabel-marginDense"
              data-shrink="true"
            >
              <span>{key}</span>
            </label>
            <div className="RaLabeled-value-52">
              <span className="MuiTypography-root MuiTypography-body2">{record[source][key]}</span>
            </div>
          </div>
        );
      })}
    </>
  );
};

JSONBField.propTypes = {
  label: PropTypes.string,
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
};

export default JSONBField;
