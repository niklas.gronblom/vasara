import { ReferenceManyField } from 'react-admin';
import BpmnField from '../BpmnField';
import React from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import lodashGet from 'lodash/get';
import JSONBField from './JSONBField';

const inAccordion = (WrappedComponent: any) => {
  return class extends React.Component<any, any> {
    render() {
      const { id, record, source } = this.props;
      const label = lodashGet(record, this.props.label) || this.props.label;
      const prefix = `${id}-${source}`;
      return (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls={`${prefix}-details`} id={`${prefix}-header`}>
            <Typography>{label}</Typography>
          </AccordionSummary>
          <AccordionDetails id={`${prefix}-details`}>
            <WrappedComponent {...this.props} />
          </AccordionDetails>
        </Accordion>
      );
    }
  };
};

export const AccordionJsonbField = inAccordion(JSONBField);
export const AccordionBpmnField = inAccordion(BpmnField);
export const AccordionReferenceManyField = inAccordion(ReferenceManyField);
