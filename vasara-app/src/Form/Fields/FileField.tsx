import React from 'react';
import { FileField, Labeled } from 'react-admin';
import { byteaToBase64, toLabel } from '../../util/helpers';

export function base64ToBytea(a: string): string {
  // PostgreSQL bytea stores binaries hex encoded prefixed with \x
  return (
    '\\x' +
    Array.from(atob(a))
      .map(c => c.charCodeAt(0).toString(16).padStart(2, '0'))
      .join('')
  );
}

export const ByteaFileField: React.FC<any> = (props: Record<any, any>) => {
  const contentType: string = props.record?.metadata[props.source]['type'];
  const filename: string = props.record?.metadata[props.source]['name'] || 'Download';
  const byteaProps = {
    ...props,
    title: '_filename',
    download: filename,
    record: {
      ...props?.record,
      _filename: filename,
    },
  };
  if (byteaProps.record) {
    byteaProps.record[props.source] =
      `data:${contentType};charset=utf-8;base64,` + byteaToBase64(props.record[props.source]);
  }
  return (
    <Labeled label={toLabel(props.source)}>
      <FileField {...byteaProps} />
    </Labeled>
  );
};
