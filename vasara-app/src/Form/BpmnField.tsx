import React, { useEffect, useRef } from 'react';
import get from 'lodash/get';
import { makeStyles } from '@material-ui/core/styles';
import { BPMNViewer as BPMN } from '../DataProviders/Camunda/helpers';

interface Props {
  diagramXML: string;
  highlight?: string[];
}

const useStyles = makeStyles({
  highlight: {
    '&:not(.djs-connection) .djs-visual > :nth-child(1)': {
      fill: 'gold !important',
    },
  },
  bpmn: {
    width: '100%',
    height: '400px',
  },
});

const ReactBpmn: React.FC<Props> = props => {
  const classes = useStyles();
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    (async () => {
      const model = await BPMN(props.diagramXML);
      if (ref.current !== null) {
        ref.current.innerHTML = '';
        (model as any).attachTo(ref.current);
        const canvas = (model as any).get('canvas');
        canvas.zoom('fit-viewport');
        for (const id of props.highlight || []) {
          try {
            canvas.addMarker(id, classes.highlight);
          } catch (e) {
            // Nothing can be done
          }
        }
      }
    })();
  }, [props.diagramXML, props.highlight, classes.highlight, ref]);

  return <div className={classes.bpmn} ref={ref} />;
};

const BpmnField = ({ record, source }: any) => {
  const value = get(record, source);
  return <ReactBpmn diagramXML={value} highlight={record.taskDefinitionKey ? [record.taskDefinitionKey] : []} />;
};

export default BpmnField;
