#!/usr/bin/env bash

docker-compose exec keycloak /opt/jboss/keycloak/bin/standalone.sh -Djboss.socket.binding.port-offset=100 -Dkeycloak.migration.action=export -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.usersExportStrategy=REALM_FILE -Dkeycloak.migration.file=/tmp/export.json -Dkeycloak.migration.realmName=vasara

docker-compose exec keycloak cat /tmp/export.json >realm-export.json
