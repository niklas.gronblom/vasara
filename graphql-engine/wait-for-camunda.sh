#!/bin/sh

set -e

log() {
    TIMESTAMP=$(date -u "+%Y-%m-%dT%H:%M:%S.000+0000")
    MESSAGE=$1
    echo "{\"timestamp\":\"$TIMESTAMP\",\"level\":\"info\",\"type\":\"startup\",\"detail\":{\"kind\":\"migration-apply\",\"info\":\"$MESSAGE\"}}"
}

if [ -z ${CAMUNDA_SERVER_HOST+x} ]; then
    log "camunda server host env var is not set, defaulting to camunda"
    CAMUNDA_SERVER_HOST=camunda
fi

if [ -z ${CAMUNDA_SERVER_PORT+x} ]; then
    log "camunda server port env var is not set, defaulting to 8080"
    CAMUNDA_SERVER_PORT=8080
fi

if [ -z ${CAMUNDA_SERVER_TIMEOUT+x} ]; then
    log "camunda server timeout env var is not set, defaulting to 300"
    CAMUNDA_SERVER_TIMEOUT=300
fi

wait_for_port() {
    local HOST=$1
    local PORT=$2
    log "waiting $CAMUNDA_SERVER_TIMEOUT for $HOST:$PORT to be ready"
    for i in `seq 1 $CAMUNDA_SERVER_TIMEOUT`;
    do
        nc -z $HOST $PORT > /dev/null 2>&1 && log "port $PORT is ready" && return
        sleep 1
    done
    log "failed waiting for $PORT" && exit 1
}

wait_for_port $CAMUNDA_SERVER_HOST $CAMUNDA_SERVER_PORT

exec docker-entrypoint.sh graphql-engine serve
