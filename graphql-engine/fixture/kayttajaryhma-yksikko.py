#!/usr/bin/env python
from public import *
from readonly import *
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound
from chameleon import PageTemplateLoader

import os
import unicodedata


def strip_accents(s):
    return "".join(
        c for c in unicodedata.normalize("NFD", s) if unicodedata.category(c) != "Mn"
    )


engine = create_engine("postgres://postgres:postgres@localhost:5432/vasara", echo=True)
session = sessionmaker(bind=engine)()

templates = PageTemplateLoader(os.path.join(os.path.dirname(__file__)))
template = templates["kayttajaryhma-yksikko.pt"]

rules = []
for user_group in session.query(PersonnelGroup).order_by(PersonnelGroup.id).all():
    rules.append(dict(
        token=strip_accents(user_group.id.replace('-', '_')),
        user_group=user_group.id,
        output=user_group.OrganisationUnit.id,
        description=user_group.OrganisationUnit.Name,
    ))

with open("kayttajaryhma-yksikko.dmn", "w") as fp:
    fp.write(template(rules=rules))
