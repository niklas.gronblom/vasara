# coding: utf-8
from sqlalchemy import Boolean, Column, Date, DateTime, Float, ForeignKey, Integer, LargeBinary, Text, text
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB, UUID
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class ContractType(Base):
    __tablename__ = 'ContractType'
    __table_args__ = {'schema': 'readonly'}

    id = Column(Text, primary_key=True)
    Description = Column(Text, nullable=False)


class OrganisationUnit(Base):
    __tablename__ = 'OrganisationUnit'
    __table_args__ = {'schema': 'readonly'}

    id = Column(Text, primary_key=True, unique=True)
    Name = Column(Text, nullable=False)
    ParentOrganisationUnitId = Column(ForeignKey('readonly.OrganisationUnit.id', ondelete='RESTRICT', onupdate='RESTRICT'))

    parent = relationship('OrganisationUnit', remote_side=[id])


class PerformanceCategory(Base):
    __tablename__ = 'PerformanceCategory'
    __table_args__ = {'schema': 'readonly'}

    id = Column(Text, primary_key=True, unique=True)
    Description = Column(Text, nullable=False)


class Person(Base):
    __tablename__ = 'Person'
    __table_args__ = {'schema': 'readonly'}

    id = Column(UUID, primary_key=True, unique=True, server_default=text("gen_random_uuid()"))
    Username = Column(Text, nullable=False)
    Name = Column(Text, nullable=False)
    Email = Column(Text, nullable=False)
    Phone = Column(Text, nullable=False)
    Position = Column(Text, nullable=False)


class RequirementLevel(Base):
    __tablename__ = 'RequirementLevel'
    __table_args__ = {'schema': 'readonly'}

    id = Column(Text, primary_key=True, unique=True)
    Description = Column(Text, nullable=False)


class WorkType(Base):
    __tablename__ = 'WorkType'
    __table_args__ = {'schema': 'readonly'}

    id = Column(Text, primary_key=True, unique=True)
    Description = Column(Text)


class Project(Base):
    __tablename__ = 'Project'
    __table_args__ = {'schema': 'readonly'}

    id = Column(UUID, primary_key=True, unique=True, server_default=text("gen_random_uuid()"))
    Name = Column(Text, nullable=False)
    OwnerPersonId = Column(ForeignKey('readonly.Person.id', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    OwnerOrganisationUnitId = Column(ForeignKey('readonly.OrganisationUnit.id', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    StartDate = Column(Date, nullable=False)
    EndDate = Column(Date, nullable=False)

    OrganisationUnit = relationship('OrganisationUnit')
    Person = relationship('Person')


class RecruitmentProposal(Base):
    __tablename__ = 'RecruitmentProposal'
    __table_args__ = {'schema': 'public'}

    id = Column(UUID, primary_key=True, unique=True, server_default=text("gen_random_uuid()"))
    metadata_ = Column('metadata', JSONB(astext_type=Text()))
    CreationDate = Column(DateTime, nullable=False)
    CreatedByPersonId = Column(ForeignKey('readonly.Person.id', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    ModificationDate = Column(DateTime, nullable=False)
    ArchiveDate = Column(DateTime, nullable=False)
    OrganisationUnitId = Column(ForeignKey('readonly.OrganisationUnit.id', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    RelatedProjectId = Column(ForeignKey('readonly.Project.id', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    Name = Column(Text, nullable=False)
    EmailOfThePerson = Column(Text, nullable=False)
    TitleOfThePosition = Column(Text, nullable=False)
    StartDateOfTheEmployment = Column(Date, nullable=False)
    EndDateOfTheEmployment = Column(Date, nullable=False)
    ContractTypeId = Column(ForeignKey('readonly.ContractType.id'), nullable=False)
    ShortDescriptionOfTheTask = Column(Text, nullable=False)
    WhatSkillsThePersonIsExpected = Column(Text, nullable=False)
    SuggestionForTheSalaryHour = Column(Float(53), nullable=False)
    SuggestionOfTheMonthlyWorkingHours = Column(Integer, nullable=False)
    SuggestionForTheSalaryRequirementLevelId = Column(ForeignKey('readonly.RequirementLevel.id'), nullable=False)
    SuggestionForTheSalaryPerformanceCategoryId = Column(ForeignKey('readonly.PerformanceCategory.id'), nullable=False)
    ReasonForFixedTermContract = Column(Text, nullable=False)
    PartTimeWork = Column(Boolean, nullable=False)
    HourlyBasedWork = Column(Boolean, nullable=False)
    WorkTypeId = Column(ForeignKey('readonly.WorkType.id'), nullable=False)
    WhyThisPerson = Column(Text, nullable=False)
    ApplicationProcedure = Column(Text, nullable=False)
    WorkingPlace = Column(Text, nullable=False)
    SupervisorPersonId = Column(ForeignKey('readonly.Person.id', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    Attachment = Column(LargeBinary, nullable=False)

    ContractType = relationship('ContractType')
    Person = relationship('Person', primaryjoin='RecruitmentProposal.CreatedByPersonId == Person.id')
    OrganisationUnit = relationship('OrganisationUnit')
    Project = relationship('Project')
    PerformanceCategory = relationship('PerformanceCategory')
    RequirementLevel = relationship('RequirementLevel')
    Person1 = relationship('Person', primaryjoin='RecruitmentProposal.SupervisorPersonId == Person.id')
    WorkType = relationship('WorkType')


class RecruitmentProposalComment(Base):
    __tablename__ = 'RecruitmentProposalComment'
    __table_args__ = {'schema': 'public'}

    id = Column(UUID, primary_key=True, unique=True, server_default=text("gen_random_uuid()"))
    metadata_ = Column('metadata', JSONB(astext_type=Text()))
    RecruitmentProposalId = Column(ForeignKey('public.RecruitmentProposal.id', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    Date = Column(DateTime, nullable=False)
    Comment = Column(Text, nullable=False)
    CreatedByPersonId = Column(ForeignKey('readonly.Person.id', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)

    Person = relationship('Person')
    RecruitmentProposal = relationship('RecruitmentProposal')
