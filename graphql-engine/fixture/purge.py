#!/usr/bin/env python
from keycloak import KeycloakAdmin
from public import *
from readonly import *
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

engine = create_engine("postgres://postgres:postgres@localhost:5432/vasara", echo=True)
session = sessionmaker(bind=engine)()


def keycloak():
    client = KeycloakAdmin(
        server_url="http://localhost:8080/auth/", username="admin", password="admin"
    )
    client.realm_name = "vasara"
    return client


k = keycloak()

for u in k.get_users():
    if u["username"] != "camunda-admin":
        k.delete_user(u["id"])


for g in k.get_groups():
    if g["name"] != "camunda-admin":
        k.delete_group(g["id"])


for table in [
    RecruitmentProposalComment,
    RecruitmentProposal,
    Project,
    PersonPersonnelGroup,
    PersonnelGroup,
    PersonOrganisationUnit,
    OrganisationUnit,
    Person,
]:
    session.query(table).delete()
session.commit()
#   while len(session.query(table).all()):
#       try:
#           for entity in session.query(table).all():
#               del entity
#       finally:
#           pass
