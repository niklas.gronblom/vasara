# coding: utf-8
from sqlalchemy import Column, Date, Float, ForeignKey, Text, text
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class ContractType(Base):
    __tablename__ = 'ContractType'
    __table_args__ = {'schema': 'readonly'}

    id = Column(Text, primary_key=True)
    Description = Column(Text, nullable=False)


class OrganisationUnit(Base):
    __tablename__ = 'OrganisationUnit'
    __table_args__ = {'schema': 'readonly'}

    id = Column(Text, primary_key=True, unique=True)
    Name = Column(Text, nullable=False)
    ParentOrganisationUnitId = Column(ForeignKey('readonly.OrganisationUnit.id', ondelete='RESTRICT', onupdate='RESTRICT'))

    parent = relationship('OrganisationUnit', remote_side=[id])


class PerformanceCategory(Base):
    __tablename__ = 'PerformanceCategory'
    __table_args__ = {'schema': 'readonly'}

    id = Column(Text, primary_key=True, unique=True)
    Description = Column(Text, nullable=False)


class Person(Base):
    __tablename__ = 'Person'
    __table_args__ = {'schema': 'readonly'}

    id = Column(UUID, primary_key=True, unique=True, server_default=text("gen_random_uuid()"))
    Username = Column(Text, nullable=False)
    Name = Column(Text, nullable=False)
    Email = Column(Text, nullable=False)
    Phone = Column(Text, nullable=False)
    Position = Column(Text, nullable=False)


class RequirementLevel(Base):
    __tablename__ = 'RequirementLevel'
    __table_args__ = {'schema': 'readonly'}

    id = Column(Text, primary_key=True, unique=True)
    Description = Column(Text, nullable=False)


class WorkType(Base):
    __tablename__ = 'WorkType'
    __table_args__ = {'schema': 'readonly'}

    id = Column(Text, primary_key=True, unique=True)
    Description = Column(Text)


class PersonnelGroup(Base):
    __tablename__ = 'PersonnelGroup'
    __table_args__ = {'schema': 'readonly'}

    id = Column(Text, primary_key=True, unique=True)
    Name = Column(Text, nullable=False)
    OrganisationUnitId = Column(ForeignKey('readonly.OrganisationUnit.id', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)

    OrganisationUnit = relationship('OrganisationUnit')


class Project(Base):
    __tablename__ = 'Project'
    __table_args__ = {'schema': 'readonly'}

    id = Column(UUID, primary_key=True, unique=True, server_default=text("gen_random_uuid()"))
    Name = Column(Text, nullable=False)
    OwnerPersonId = Column(ForeignKey('readonly.Person.id', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    OwnerOrganisationUnitId = Column(ForeignKey('readonly.OrganisationUnit.id', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    StartDate = Column(Date, nullable=False)
    EndDate = Column(Date, nullable=False)

    OrganisationUnit = relationship('OrganisationUnit')
    Person = relationship('Person')


class SalaryTable(Base):
    __tablename__ = 'SalaryTable'
    __table_args__ = {'schema': 'readonly'}

    id = Column(UUID, primary_key=True, unique=True, server_default=text("gen_random_uuid()"))
    RequirementLevelId = Column(ForeignKey('readonly.RequirementLevel.id'), nullable=False)
    PerformanceCategoryId = Column(ForeignKey('readonly.PerformanceCategory.id'), nullable=False)
    LowerLimit = Column(Float(53), nullable=False)
    UpperLimit = Column(Float(53), nullable=False)

    PerformanceCategory = relationship('PerformanceCategory')
    RequirementLevel = relationship('RequirementLevel')


class PersonOrganisationUnit(Base):
    __tablename__ = '_Person_OrganisationUnit'
    __table_args__ = {'schema': 'readonly'}

    id = Column(UUID, nullable=False, unique=True, server_default=text("gen_random_uuid()"))
    PersonId = Column(ForeignKey('readonly.Person.id', ondelete='RESTRICT', onupdate='RESTRICT'), primary_key=True, nullable=False)
    OrganisationUnitId = Column(ForeignKey('readonly.OrganisationUnit.id', ondelete='RESTRICT', onupdate='RESTRICT'), primary_key=True, nullable=False)

    OrganisationUnit = relationship('OrganisationUnit')
    Person = relationship('Person')


class PersonPersonnelGroup(Base):
    __tablename__ = '_Person_PersonnelGroup'
    __table_args__ = {'schema': 'readonly'}

    id = Column(UUID, nullable=False, unique=True, server_default=text("gen_random_uuid()"))
    PersonId = Column(ForeignKey('readonly.Person.id', ondelete='RESTRICT', onupdate='RESTRICT'), primary_key=True, nullable=False)
    PersonnelGroupId = Column(ForeignKey('readonly.PersonnelGroup.id', ondelete='RESTRICT', onupdate='RESTRICT'), primary_key=True, nullable=False)

    Person = relationship('Person')
    PersonnelGroup = relationship('PersonnelGroup')
