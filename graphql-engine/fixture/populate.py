#!/usr/bin/env python
from sqlalchemy import Column, Date, Float, ForeignKey, Table, Text, text
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
from datetime import timedelta
from docx import Document
from faker import Faker
from io import BytesIO
from public import *
from readonly import *
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from keycloak import KeycloakAdmin
from keycloak.exceptions import KeycloakGetError

import unicodedata
import json
import random

person_personnelgroup = Table(
    "readonly._Person_PersonnelGroup",
    Base.metadata,
    Column("PersonId", UUID, ForeignKey("readonly.Person.id"), primary_key=True),
    Column(
        "PersonnelGroupId",
        Text,
        ForeignKey("readonly.PersonnelGroup.id"),
        primary_key=True,
    ),
    extend_existing=True,
)

person_organisationunit = Table(
    "readonly._Person_OrganisationUnit",
    Base.metadata,
    Column("PersonId", UUID, ForeignKey("readonly.Person.id"), primary_key=True),
    Column(
        "OrganisationUnitId",
        Text,
        ForeignKey("readonly.OrganisationUnit.id"),
        primary_key=True,
    ),
    extend_existing=True,
)

Person.PersonnelGroup = relationship(
    "PersonnelGroup", secondary=person_personnelgroup, backref="People"
)

OrganisationUnit.Person = relationship(
    "Person", secondary=person_organisationunit, backref="People"
)


document = Document()
document.add_heading("This is an attachment.", 0)
attachment = BytesIO()
document.save(attachment)


def keycloak():
    client = KeycloakAdmin(
        server_url="http://localhost:8080/auth/", username="admin", password="admin"
    )
    client.realm_name = "vasara"
    return client


engine = create_engine("postgres://postgres:postgres@localhost:5432/vasara", echo=True)
session = sessionmaker(bind=engine)()


fake = Faker("fi")
Faker.seed(len(session.query(Person).all()))


def strip_accents(s):
    return "".join(
        c for c in unicodedata.normalize("NFD", s) if unicodedata.category(c) != "Mn"
    )


def populate_persons():
    persons = session.query(Person).all()
    for _ in range(max(0, 500 - len(persons))):
        profile = fake.profile()

        # Postgres
        person = Person(
            Username=profile["username"],
            Name=profile["name"],
            Email=f"{profile['username']}@test.jyu.fi",
            Phone=fake.phone_number(),
            Position=fake.job(),
        )
        session.add(person)

        # Keycloak
        user_id = keycloak().get_user_id(username=person.Username)
        if user_id is None:
            keycloak().create_user(
                dict(
                    username=person.Username,
                    email=person.Email,
                    enabled=True,
                    firstName=person.Name.split(" ", 1)[0],
                    lastName=person.Name.split(" ", 1)[-1],
                    credentials=[dict(value=person.Username, type="password")],
                )
            )
        session.commit()
        print(f"Created {person.Email}")


def get_organisation_tree():
    tree = {}
    for org in json.load(open("OrganisationUnit.json"))["data"]["OrganisationUnit"]:
        tree.setdefault(org["id"], {"data": {}, "children": []})
        tree[org["id"]]["data"].update(org)
        if org["parentId"]:
            tree.setdefault(org["parentId"], {"data": {}, "children": []})
            tree[org["parentId"]]["children"].append(org["id"])
    return tree


def populate_organisation_unit(tree, org=None, parent=None):
    if org is None:
        org = tree["jyu-university-root-id"]
    data = org["data"]
    if parent is None:
        id_ = f"jyu"
    else:
        id_ = f"{parent.id}-{data['abbreviationFi'].lower()}"
    unit = (
        session.query(OrganisationUnit).filter(OrganisationUnit.id == id_).one_or_none()
    )
    if unit is None:
        unit = OrganisationUnit(
            id=id_,
            Name=data["nameFi"],
            ParentOrganisationUnitId=parent.id if parent else None,
        )
        session.add(unit)
        session.commit()
    for child in org["children"]:
        populate_organisation_unit(tree, tree[child], unit)


def create_personnel_group(id_, name, unit, title=None, size=1):
    persons = session.query(Person).all()
    group = session.query(PersonnelGroup).filter(PersonnelGroup.id == id_).one_or_none()
    if group is None:
        group = PersonnelGroup(id=id_, OrganisationUnitId=unit.id, Name=name,)
        session.add(group)
        session.commit()

    try:
        group_id = keycloak().get_group_by_path(f"/{id_}")["id"]
    except TypeError:
        keycloak().create_group(dict(name=id_))
        group_id = keycloak().get_group_by_path(f"/{id_}")["id"]

    members = (
        session.query(Person)
        .filter(Person.PersonnelGroup.any(PersonnelGroup.id == id_))
        .all()
    )
    for _ in range(max(0, (size - len(members)))):
        person = random.choice(persons)
        while group in person.PersonnelGroup:
            person = random.choice(persons)
        if title:
            person.Position = title
        user_id = keycloak().get_user_id(username=person.Username)
        keycloak().group_user_add(group_id=group_id, user_id=user_id)
        person.PersonnelGroup += [group]
        unit.Person += [person]
        session.add(person)
        session.add(unit)
        session.commit()

    if not title:
        staff = (
            session.query(OrganisationUnit)
            .filter(OrganisationUnit.id == unit.id)
            .one()
            .Person
        )
        for member in staff:
            if group not in member.PersonnelGroup:
                user_id = keycloak().get_user_id(username=member.Username)
                keycloak().group_user_add(group_id=group_id, user_id=user_id)
                member.PersonnelGroup += [group]
                unit.Person += [member]
                session.add(member)
                session.add(unit)
                session.commit()


def populate_personnel_groups():
    units = {unit.id: unit for unit in session.query(OrganisationUnit).all()}
    for name in ["rehtori", "vararehtori", "hallintopäällikkö"]:
        create_personnel_group(
            strip_accents(f"hk-{name}"),
            f"{units['jyu'].Name} / {name.title()}",
            units["jyu"],
            name.title(),
        )
    units.pop("jyu")
    for unit in units.values():
        if "tiedekunta" in unit.Name.lower() or "korkeakoulu" in unit.Name.lower():
            create_personnel_group(
                strip_accents(f"hk-{unit.id[4:]}-dekaani"),
                f"{unit.Name} / Dekaani",
                unit,
                title="Dekaani",
            )
        else:
            create_personnel_group(
                strip_accents(f"hk-{unit.id[4:]}-johtaja"),
                f"{unit.Name} / Johtaja",
                unit,
                title="Johtaja",
            )
        for name in ["hankesihteeri", "controller", "hr-koordinaattori"]:
            create_personnel_group(
                strip_accents(f"hk-{unit.id[4:]}-{name}"),
                f"{unit.Name} / {name.title()}",
                unit,
                title=name.title(),
                size=2,
            )

        create_personnel_group(
            strip_accents(f"hk-{unit.id[4:]}"),
            f"{unit.Name} / Henkilökunta",
            unit,
            title=None,
            size=2,
        )


def populate_personnel_groups_other():
    persons = session.query(Person).filter(~Person.PersonnelGroup.any()).all()
    groups = session.query(PersonnelGroup).filter(PersonnelGroup.Name.like("%Henkilökunta")).all()
    for person in persons:
        group = random.choice(groups)
        unit = session.query(OrganisationUnit).filter(OrganisationUnit.id == group.OrganisationUnitId).one_or_none()
        user_id = keycloak().get_user_id(username=person.Username)
        group_id = keycloak().get_group_by_path(f"/{group.id}")["id"]
        keycloak().group_user_add(group_id=group_id, user_id=user_id)
        person.PersonnelGroup += [group]
        unit.Person += [person]
        session.add(person)
        session.add(unit)
        session.commit()


def populate_projects():
    units = session.query(OrganisationUnit).all()
    projects = session.query(Project).all()
    for _ in range(max(0, 50 - len(projects))):
        date = fake.date_time_this_year(after_now=True, tzinfo=None)
        unit = random.choice(units)
        staff = (
            session.query(OrganisationUnit)
            .filter(OrganisationUnit.id == unit.id)
            .one()
            .Person
        )
        person = random.choice(staff)
        project = Project(
            Name=fake.catch_phrase(),
            OwnerPersonId=person.id,
            OwnerOrganisationUnitId=unit.id,
            StartDate=date.date(),
            EndDate=(date + timedelta(100)).date(),
        )
        session.add(project)
        session.commit()


def get_projects_by_unit():
    projects_by_unit = {}
    for unit in session.query(OrganisationUnit).all():
        projects_by_unit[unit.id] = (
            session.query(Project)
            .filter(Project.OwnerOrganisationUnitId == unit.id)
            .all()
        )
        if not projects_by_unit[unit.id]:
            del projects_by_unit[unit.id]
    return projects_by_unit


def populate_proposals():
    projects_by_unit = get_projects_by_unit()
    titles = [
        "muu henkilöstö",
        "projektitutkija",
        "tutkimuskoordinaattori",
        "yliopistonlehtori",
        "yliopistonopettaja",
        "apulaisprofessori",
        "professori",
        "tohtorikoulutettava",
        "tutkimusavustaja",
        "tutkimusjohtaja",
        "tutkimusprofessori",
        "yliopistotutkija",
    ]
    proposals = session.query(RecruitmentProposal).all()
    for _ in range(max(0, 50 - len(proposals))):
        date = fake.date_time_this_year(after_now=True, tzinfo=None)
        profile = fake.profile()
        unit_id = random.choice(list(projects_by_unit.keys()))
        staff = (
            session.query(OrganisationUnit)
            .filter(OrganisationUnit.id == unit_id)
            .one()
            .Person
        )
        salary = random.choice(session.query(SalaryTable).all())
        proposal = RecruitmentProposal(
            CreationDate=date,
            CreatedByPersonId=random.choice(staff).id,
            ModificationDate=date,
            ArchiveDate=date,
            OrganisationUnitId=unit_id,
            RelatedProjectId=random.choice(projects_by_unit[unit_id]).id,
            Name=profile["name"],
            EmailOfThePerson=profile["mail"],
            TitleOfThePosition=random.choice(titles),
            StartDateOfTheEmployment=date.date() + timedelta(10),
            EndDateOfTheEmployment=date.date() + timedelta(100),
            ContractTypeId=random.choice(session.query(ContractType).all()).id,
            ShortDescriptionOfTheTask=fake.paragraph(),
            WhatSkillsThePersonIsExpected=fake.paragraph(),
            SuggestionForTheSalaryHour=(
                salary.LowerLimit + (salary.UpperLimit - salary.LowerLimit) / 2
            )
            / 160,
            SuggestionOfTheMonthlyWorkingHours=random.randint(0, 160),
            SuggestionForTheSalaryRequirementLevelId=salary.RequirementLevelId,
            SuggestionForTheSalaryPerformanceCategoryId=salary.PerformanceCategoryId,
            ReasonForFixedTermContract=fake.word(),
            PartTimeWork=fake.boolean(),
            HourlyBasedWork=fake.boolean(),
            WorkTypeId=random.choice(session.query(WorkType).all()).id,
            WhyThisPerson=fake.paragraph(),
            ApplicationProcedure=fake.paragraph(),
            WorkingPlace=fake.paragraph(),
            SupervisorPersonId=random.choice(staff).id,
            Attachment=attachment.getvalue(),
            metadata_={
                "Attachment": {
                    "filename": "cv.docx",
                    "content-type": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                    "length": len(attachment.getvalue()),
                }
            },
        )
        session.add(proposal)
        session.commit()


def populate_proposals_comments():
    comments = session.query(RecruitmentProposalComment).all()
    proposals = session.query(RecruitmentProposal).all()
    persons = session.query(Person).all()
    for _ in range(max(0, 200 - len(comments))):
        proposal = random.choice(proposals)
        person = random.choice(persons)
        comment = RecruitmentProposalComment(
            RecruitmentProposalId=proposal.id,
            Date=proposal.CreationDate + timedelta(random.randint(0, 100)),
            Comment=fake.paragraph(),
            CreatedByPersonId=person.id,
        )
        session.add(comment)
        session.commit()


populate_persons()
populate_organisation_unit(get_organisation_tree())
populate_personnel_groups()
populate_personnel_groups_other()
populate_projects()
populate_proposals()
populate_proposals_comments()
