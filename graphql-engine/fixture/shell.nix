{ pkgs ? import (builtins.fetchTarball {
    # branches nixos-20.03
    url = "https://github.com/NixOS/nixpkgs-channels/archive/99a3d7a86fce9e9c9f23b3e304d7d2b1270a12b8.tar.gz";
    sha256 = "0i40cl3n6600z2lkwrpiy28dcnv2r63fcgfswj91aaf1xfn2chql";
  }) {}
, pythonPackages ? pkgs.python3Packages
}:

with pkgs;

let
  sqlacodegen = pythonPackages.buildPythonPackage rec {
    pname = "sqlacodegen";
    version = "2.1.0";
    src = pythonPackages.fetchPypi {
      inherit pname version;
      sha256 = "0z0hh7lcfhw5n0zjn1v1anmkfkidalsa4d9xakcqvkixdkimiqpa";
    };
    nativeBuildInputs = with pythonPackages; [
      setuptools_scm
    ];
    propagatedBuildInputs = with pythonPackages; [
      inflect
      sqlalchemy
    ];
  };

  keycloak = pythonPackages.buildPythonPackage rec {
    pname = "python-keycloak";
    version = "0.20.0";
    src = pythonPackages.fetchPypi {
      inherit pname version;
      sha256 = "10z5asky68gg6p5x9n7xfixxnvq273m4jab2g7b0vzncspkqa7xp";
    };
    doCheck = false;
    nativeBuildInputs = with pythonPackages; [
    ];
    propagatedBuildInputs = with pythonPackages; [
      python-jose
      requests
    ];
  };

in

mkShell {
  buildInputs = [
    (pythonPackages.python.withPackages(ps: with ps; [
      chameleon
      black
      faker
      keycloak
      python-docx
      psycopg2
      setuptools
      sqlacodegen
      sqlalchemy
    ]))
  ];
  shellHook = ''
  '';
}
