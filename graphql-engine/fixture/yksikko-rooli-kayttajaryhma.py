#!/usr/bin/env python
from public import *
from readonly import *
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound
from chameleon import PageTemplateLoader

import os
import unicodedata


def strip_accents(s):
    return "".join(
        c for c in unicodedata.normalize("NFD", s) if unicodedata.category(c) != "Mn"
    )


engine = create_engine("postgres://postgres:postgres@localhost:5432/vasara", echo=True)
session = sessionmaker(bind=engine)()

templates = PageTemplateLoader(os.path.join(os.path.dirname(__file__)))
template = templates["yksikko-rooli-kayttajaryhma.pt"]

role_postfix = {
    "yksikönjohtaja": "-johtaja",
    "hankesihteeri": "-hankesihteeri",
    "controller": "-controller",
    "hr-koordinaattori": "-hr-koordinaattori",
    "dekaani": "-dekaani",
}

rules = []
for unit in session.query(OrganisationUnit).order_by(OrganisationUnit.id).all():
    for role, postfix in role_postfix.items():
        cursor = unit
        while cursor is not None:
            try:
                group = session.query(PersonnelGroup).filter(
                    PersonnelGroup.OrganisationUnitId == cursor.id
                ).filter(
                    PersonnelGroup.id.like(f"%{postfix}")
                ).one()
            except NoResultFound:
                group = None
            if group:
                rules.append(dict(
                    token=strip_accents(f"{unit.id}{postfix}".replace('-', '_')),
                    unit=unit.id,
                    role=role,
                    output=group.id,
                    description=unit.Name,
                ))
                break
            if cursor.ParentOrganisationUnitId:
                cursor = session.query(OrganisationUnit).filter(
                    OrganisationUnit.id == cursor.ParentOrganisationUnitId
                ).one()
            else:
                cursor = None

with open("yksikko-rooli-kayttajaryhma.dmn", "w") as fp:
    fp.write(template(rules=rules))
