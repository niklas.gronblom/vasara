CREATE SCHEMA ea;
CREATE SCHEMA pm;
CREATE SCHEMA readonly;
CREATE SCHEMA vasara;
CREATE TABLE ea."ApplicationComponent" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL
);
CREATE TABLE ea."ApplicationIntegration" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Description" text NOT NULL,
    "SourceApplicationId" uuid NOT NULL,
    "TargetApplicationId" uuid NOT NULL,
    "DataFlow" text
);
CREATE TABLE ea."ApplicationService" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "Description" text NOT NULL,
    "CreationDate" date,
    "CreatedByPersonId" uuid,
    "ModificationDate" date,
    "ModifiedByPersonId" uuid
);
CREATE TABLE ea."BusinessFunction" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "Description" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedPersonId" uuid NOT NULL,
    "IsPartOfCapabilityId" uuid NOT NULL
);
CREATE TABLE ea."BusinessProcess" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "Description" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL,
    "IsPartOfBusinessFunctionId" uuid NOT NULL
);
CREATE TABLE ea."BusinessService" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "Description" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL,
    "IsPartOfBusinessFunctionId" uuid NOT NULL
);
CREATE TABLE ea."Capability" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "Description" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL,
    "IsPartOfCapabilityId" uuid NOT NULL,
    "OwnerIsPersonId" uuid NOT NULL
);
CREATE TABLE ea."CourseOfAction" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "Description" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL
);
CREATE TABLE ea."DataObject" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "IsPartOfDataObjectId" uuid NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL
);
CREATE TABLE ea."StrategicDriver" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL
);
CREATE TABLE ea."StrategicGoal" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL,
    "IsPartOfStrategicGoalId" uuid NOT NULL
);
CREATE TABLE ea."StrategicOutcome" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreationByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL,
    "RelatedCapabilityId" uuid NOT NULL
);
CREATE TABLE ea."Strategy" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "CreationDate" timestamp with time zone NOT NULL,
    "CreatedByPersonId" uuid,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL
);
CREATE TABLE ea."_ApplicationComponent_DataObject" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "RelatedApplicationComponentId" uuid NOT NULL,
    "RelatedDataObjectId" uuid NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedPersonId" uuid NOT NULL,
    "MasterData" boolean NOT NULL
);
CREATE TABLE ea."_ApplicationIntegration_DataObject" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL,
    "ApplicationIntegrationId" uuid NOT NULL,
    "DataObjectId" uuid NOT NULL
);
CREATE TABLE ea."_ApplicationService_ApplicationComponent" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ApplicationServiceId" uuid NOT NULL,
    "ApplicationComponentId" uuid NOT NULL
);
CREATE TABLE ea."_ApplicationService_BusinessProcess" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ApplicationServiceId" uuid NOT NULL,
    "BusinessProcessId" uuid NOT NULL
);
CREATE TABLE ea."_BusinessService_BusinessProcess" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "BusinessServiceId" uuid NOT NULL,
    "BusinessProcessId" uuid NOT NULL
);
CREATE TABLE ea."_StrategicGoal_Capability" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "RelatedStrategicGoalId" uuid NOT NULL,
    "RelatedCapabilityId" uuid NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL
);
CREATE TABLE ea."_StrategicOutcome_CourseOfAction" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "RelatedStrategicOutcomeId" uuid NOT NULL,
    "RelatedCourseOfActionId" uuid NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL
);
CREATE TABLE pm."Deliverable" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "Description" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL,
    "RelatedWorkPackageId" uuid NOT NULL
);
CREATE TABLE pm."Project" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "Description" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL,
    "OwnerIsPersonId" uuid NOT NULL,
    "RelatedProjectProposalId" uuid NOT NULL,
    "RelatedProjectPlanId" uuid NOT NULL,
    "IsPartOfCourseOfActionId" uuid NOT NULL
);
CREATE TABLE pm."ProjectIdea" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "Description" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL
);
CREATE TABLE pm."ProjectPlan" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "Description" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL,
    "RelatedProjectProposalId" uuid NOT NULL
);
CREATE TABLE pm."ProjectProposal" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "Description" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL,
    "OwnerIsPersonId" uuid NOT NULL,
    "RelatedProjectIdeaId" uuid NOT NULL
);
CREATE TABLE pm."WorkPackage" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "Description" text NOT NULL,
    "CreationDate" date NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" date NOT NULL,
    "ModifiedByPersonId" uuid NOT NULL,
    "RelatedProjectId" uuid NOT NULL
);
CREATE TABLE public."RecruitmentProposal" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    metadata jsonb,
    "CreationDate" timestamp without time zone NOT NULL,
    "CreatedByPersonId" uuid NOT NULL,
    "ModificationDate" timestamp without time zone NOT NULL,
    "ArchiveDate" timestamp without time zone NOT NULL,
    "OrganisationUnitId" text NOT NULL,
    "RelatedProjectId" uuid NOT NULL,
    "Name" text NOT NULL,
    "EmailOfThePerson" text NOT NULL,
    "TitleOfThePosition" text NOT NULL,
    "StartDateOfTheEmployment" date NOT NULL,
    "EndDateOfTheEmployment" date NOT NULL,
    "ContractTypeId" text NOT NULL,
    "ShortDescriptionOfTheTask" text NOT NULL,
    "WhatSkillsThePersonIsExpected" text NOT NULL,
    "SuggestionForTheSalaryHour" double precision NOT NULL,
    "SuggestionOfTheMonthlyWorkingHours" integer NOT NULL,
    "SuggestionForTheSalaryRequirementLevelId" text NOT NULL,
    "SuggestionForTheSalaryPerformanceCategoryId" text NOT NULL,
    "ReasonForFixedTermContract" text NOT NULL,
    "PartTimeWork" boolean NOT NULL,
    "HourlyBasedWork" boolean NOT NULL,
    "WorkTypeId" text NOT NULL,
    "WhyThisPerson" text NOT NULL,
    "ApplicationProcedure" text NOT NULL,
    "WorkingPlace" text NOT NULL,
    "SupervisorPersonId" uuid NOT NULL,
    "Attachment" bytea NOT NULL
);
CREATE TABLE public."RecruitmentProposalComment" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    metadata jsonb,
    "RecruitmentProposalId" uuid NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Comment" text NOT NULL,
    "CreatedByPersonId" uuid NOT NULL
);
CREATE TABLE readonly."ContractType" (
    id text NOT NULL,
    "Description" text NOT NULL
);
CREATE TABLE readonly."OrganisationUnit" (
    id text NOT NULL,
    "Name" text NOT NULL,
    "ParentOrganisationUnitId" text
);
CREATE TABLE readonly."PerformanceCategory" (
    id text NOT NULL,
    "Description" text NOT NULL
);
CREATE TABLE readonly."Person" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Username" text NOT NULL,
    "Name" text NOT NULL,
    "Email" text NOT NULL,
    "Phone" text NOT NULL,
    "Position" text NOT NULL
);
CREATE TABLE readonly."PersonnelGroup" (
    id text NOT NULL,
    "Name" text NOT NULL,
    "OrganisationUnitId" text NOT NULL
);
CREATE TABLE readonly."Project" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "Name" text NOT NULL,
    "OwnerPersonId" uuid NOT NULL,
    "OwnerOrganisationUnitId" text NOT NULL,
    "StartDate" date NOT NULL,
    "EndDate" date NOT NULL
);
CREATE TABLE readonly."RequirementLevel" (
    id text NOT NULL,
    "Description" text NOT NULL
);
CREATE TABLE readonly."SalaryTable" (
    id text NOT NULL,
    "Name" text NOT NULL,
    "RequirementLevelId" text NOT NULL,
    "PerformanceCategoryId" text NOT NULL,
    "LowerLimit" double precision NOT NULL,
    "UpperLimit" double precision NOT NULL
);
CREATE TABLE readonly."WorkType" (
    id text NOT NULL,
    "Description" text
);
CREATE TABLE readonly."_Person_OrganisationUnit" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PersonId" uuid NOT NULL,
    "OrganisationUnitId" text NOT NULL
);
CREATE TABLE readonly."_Person_PersonnelGroup" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PersonId" uuid NOT NULL,
    "PersonnelGroupId" text NOT NULL
);
CREATE TABLE vasara.entity_crud_form (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    metadata jsonb NOT NULL,
    tablename text NOT NULL,
    method text NOT NULL,
    schema jsonb NOT NULL
);
CREATE TABLE vasara.user_task_form (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    metadata jsonb,
    process_definition_name text NOT NULL,
    process_definition_version integer NOT NULL,
    user_task_id text NOT NULL,
    schema jsonb NOT NULL
);
ALTER TABLE ONLY ea."ApplicationComponent"
    ADD CONSTRAINT "ApplicationComponent_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."ApplicationIntegration"
    ADD CONSTRAINT "ApplicationIntegration_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."ApplicationService"
    ADD CONSTRAINT "ApplicationService_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."BusinessFunction"
    ADD CONSTRAINT "BusinessFunction_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."BusinessProcess"
    ADD CONSTRAINT "BusinessProcess_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."BusinessService"
    ADD CONSTRAINT "BusinessService_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."Capability"
    ADD CONSTRAINT "Capability_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."CourseOfAction"
    ADD CONSTRAINT "CourseOfAction_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."DataObject"
    ADD CONSTRAINT "DataObject_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."StrategicDriver"
    ADD CONSTRAINT "StrategicDriver_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."_StrategicGoal_Capability"
    ADD CONSTRAINT "StrategicGoal_Capability_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."StrategicGoal"
    ADD CONSTRAINT "StrategicGoal_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."StrategicOutcome"
    ADD CONSTRAINT "StrategicOutcome_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."Strategy"
    ADD CONSTRAINT "Strategy_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."_ApplicationComponent_DataObject"
    ADD CONSTRAINT "_ApplicationComponets_DataObjects_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."_ApplicationIntegration_DataObject"
    ADD CONSTRAINT "_ApplicationIntegration_DataObject_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."_ApplicationService_ApplicationComponent"
    ADD CONSTRAINT "_ApplicationService_ApplicationComponent_Id_key" UNIQUE (id);
ALTER TABLE ONLY ea."_ApplicationService_ApplicationComponent"
    ADD CONSTRAINT "_ApplicationService_ApplicationComponent_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."_ApplicationService_BusinessProcess"
    ADD CONSTRAINT "_ApplicationService_BusinessProcess_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."_BusinessService_BusinessProcess"
    ADD CONSTRAINT "_BusinessService_BusinessProcess_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY ea."_StrategicOutcome_CourseOfAction"
    ADD CONSTRAINT "_StrategicOutcome_CourseOfAction_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY pm."Deliverable"
    ADD CONSTRAINT "Deliverable_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY pm."ProjectIdea"
    ADD CONSTRAINT "ProjectIdea_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY pm."ProjectPlan"
    ADD CONSTRAINT "ProjectPlan_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY pm."ProjectProposal"
    ADD CONSTRAINT "ProjectProposal_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY pm."Project"
    ADD CONSTRAINT "Project_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY pm."WorkPackage"
    ADD CONSTRAINT "WorkPackage_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."RecruitmentProposalComment"
    ADD CONSTRAINT "RecruitmentProposalComment_id_key" UNIQUE (id);
ALTER TABLE ONLY public."RecruitmentProposalComment"
    ADD CONSTRAINT "RecruitmentProposalComment_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."RecruitmentProposal"
    ADD CONSTRAINT "RecruitmentProposal_id_key" UNIQUE (id);
ALTER TABLE ONLY public."RecruitmentProposal"
    ADD CONSTRAINT "RecruitmentProposal_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY readonly."ContractType"
    ADD CONSTRAINT "ContractType_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY readonly."OrganisationUnit"
    ADD CONSTRAINT "OrganisationUnit_id_key" UNIQUE (id);
ALTER TABLE ONLY readonly."PerformanceCategory"
    ADD CONSTRAINT "PerformanceCategory_id_key" UNIQUE (id);
ALTER TABLE ONLY readonly."PerformanceCategory"
    ADD CONSTRAINT "PerformanceCategory_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY readonly."Person"
    ADD CONSTRAINT "Person_id_key" UNIQUE (id);
ALTER TABLE ONLY readonly."Person"
    ADD CONSTRAINT "Person_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY readonly."PersonnelGroup"
    ADD CONSTRAINT "PersonnelGroup_id_key" UNIQUE (id);
ALTER TABLE ONLY readonly."PersonnelGroup"
    ADD CONSTRAINT "PersonnelGroup_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY readonly."Project"
    ADD CONSTRAINT "Project_id_key" UNIQUE (id);
ALTER TABLE ONLY readonly."Project"
    ADD CONSTRAINT "Project_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY readonly."RequirementLevel"
    ADD CONSTRAINT "RequirementLevel_id_key" UNIQUE (id);
ALTER TABLE ONLY readonly."RequirementLevel"
    ADD CONSTRAINT "RequirementLevel_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY readonly."SalaryTable"
    ADD CONSTRAINT "SalaryTable_id_key" UNIQUE (id);
ALTER TABLE ONLY readonly."SalaryTable"
    ADD CONSTRAINT "SalaryTable_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY readonly."WorkType"
    ADD CONSTRAINT "WorkType_id_key" UNIQUE (id);
ALTER TABLE ONLY readonly."WorkType"
    ADD CONSTRAINT "WorkType_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY readonly."_Person_OrganisationUnit"
    ADD CONSTRAINT "_Person_OrganisationUnit_id_key" UNIQUE (id);
ALTER TABLE ONLY readonly."_Person_OrganisationUnit"
    ADD CONSTRAINT "_Person_OrganisationUnit_pkey" PRIMARY KEY ("PersonId", "OrganisationUnitId");
ALTER TABLE ONLY readonly."_Person_PersonnelGroup"
    ADD CONSTRAINT "_Person_PersonnelGroup_id_key" UNIQUE (id);
ALTER TABLE ONLY readonly."_Person_PersonnelGroup"
    ADD CONSTRAINT "_Person_PersonnelGroup_pkey" PRIMARY KEY ("PersonId", "PersonnelGroupId");
ALTER TABLE ONLY readonly."OrganisationUnit"
    ADD CONSTRAINT organisation_unit_pkey PRIMARY KEY (id);
ALTER TABLE ONLY vasara.entity_crud_form
    ADD CONSTRAINT entity_crud_form_pkey PRIMARY KEY (id);
ALTER TABLE ONLY vasara.user_task_form
    ADD CONSTRAINT user_task_form_pkey PRIMARY KEY (id);
ALTER TABLE ONLY ea."BusinessFunction"
    ADD CONSTRAINT "BusinessFunction_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."BusinessFunction"
    ADD CONSTRAINT "BusinessFunction_IsPartOfCapabilityId_fkey" FOREIGN KEY ("IsPartOfCapabilityId") REFERENCES ea."Capability"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."BusinessFunction"
    ADD CONSTRAINT "BusinessFunction_ModifiedPersonId_fkey" FOREIGN KEY ("ModifiedPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."BusinessProcess"
    ADD CONSTRAINT "BusinessProcess_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."BusinessProcess"
    ADD CONSTRAINT "BusinessProcess_IsPartOfBusinessFunctionId_fkey" FOREIGN KEY ("IsPartOfBusinessFunctionId") REFERENCES ea."Capability"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."BusinessProcess"
    ADD CONSTRAINT "BusinessProcess_ModifiedByPersonId_fkey" FOREIGN KEY ("ModifiedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."BusinessService"
    ADD CONSTRAINT "BusinessService_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."BusinessService"
    ADD CONSTRAINT "BusinessService_IsPartOfBusinessFunctionId_fkey" FOREIGN KEY ("IsPartOfBusinessFunctionId") REFERENCES ea."BusinessFunction"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."BusinessService"
    ADD CONSTRAINT "BusinessService_ModifiedByPersonId_fkey" FOREIGN KEY ("ModifiedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."Capability"
    ADD CONSTRAINT "Capability_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."Capability"
    ADD CONSTRAINT "Capability_ModifiedByPersonId_fkey" FOREIGN KEY ("ModifiedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."Capability"
    ADD CONSTRAINT "Capability_OwnerIsPersonId_fkey" FOREIGN KEY ("OwnerIsPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."DataObject"
    ADD CONSTRAINT "DataObject_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."DataObject"
    ADD CONSTRAINT "DataObject_IsPartOfDataObjectId_fkey" FOREIGN KEY ("IsPartOfDataObjectId") REFERENCES ea."DataObject"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."DataObject"
    ADD CONSTRAINT "DataObject_ModifiedByPersonId_fkey" FOREIGN KEY ("ModifiedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."StrategicGoal"
    ADD CONSTRAINT "StrategicGoal_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."StrategicGoal"
    ADD CONSTRAINT "StrategicGoal_IsPartOfStrategicGoalId_fkey" FOREIGN KEY ("IsPartOfStrategicGoalId") REFERENCES ea."StrategicGoal"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."StrategicGoal"
    ADD CONSTRAINT "StrategicGoal_ModifiedByPersonId_fkey" FOREIGN KEY ("ModifiedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."StrategicOutcome"
    ADD CONSTRAINT "StrategicOutcome_CreationByPersonId_fkey" FOREIGN KEY ("CreationByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."StrategicOutcome"
    ADD CONSTRAINT "StrategicOutcome_ModifiedByPersonId_fkey" FOREIGN KEY ("ModifiedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."StrategicOutcome"
    ADD CONSTRAINT "StrategicOutcome_RelatedCapabilityId_fkey" FOREIGN KEY ("RelatedCapabilityId") REFERENCES ea."Capability"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_ApplicationComponent_DataObject"
    ADD CONSTRAINT "_ApplicationComponets_DataObjects_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_ApplicationComponent_DataObject"
    ADD CONSTRAINT "_ApplicationComponets_DataObjects_ModifiedPersonId_fkey" FOREIGN KEY ("ModifiedPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_ApplicationComponent_DataObject"
    ADD CONSTRAINT "_ApplicationComponets_DataObjects_RelatedApplicationComponen" FOREIGN KEY ("RelatedApplicationComponentId") REFERENCES ea."ApplicationComponent"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_ApplicationComponent_DataObject"
    ADD CONSTRAINT "_ApplicationComponets_DataObjects_RelatedDataObjectId_fkey" FOREIGN KEY ("RelatedDataObjectId") REFERENCES ea."DataObject"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_ApplicationIntegration_DataObject"
    ADD CONSTRAINT "_ApplicationIntegration_DataObject_ApplicationIntegrationId_" FOREIGN KEY ("ApplicationIntegrationId") REFERENCES ea."ApplicationIntegration"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_ApplicationIntegration_DataObject"
    ADD CONSTRAINT "_ApplicationIntegration_DataObject_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_ApplicationIntegration_DataObject"
    ADD CONSTRAINT "_ApplicationIntegration_DataObject_DataObjectId_fkey" FOREIGN KEY ("DataObjectId") REFERENCES ea."DataObject"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_ApplicationIntegration_DataObject"
    ADD CONSTRAINT "_ApplicationIntegration_DataObject_ModifiedByPersonId_fkey" FOREIGN KEY ("ModifiedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_ApplicationService_ApplicationComponent"
    ADD CONSTRAINT "_ApplicationService_ApplicationComp_ApplicationComponentId_fkey" FOREIGN KEY ("ApplicationComponentId") REFERENCES ea."ApplicationComponent"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_ApplicationService_ApplicationComponent"
    ADD CONSTRAINT "_ApplicationService_ApplicationCompon_ApplicationServiceId_fkey" FOREIGN KEY ("ApplicationServiceId") REFERENCES ea."ApplicationService"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_ApplicationService_BusinessProcess"
    ADD CONSTRAINT "_ApplicationService_BusinessProcess_ApplicationServiceId_fkey" FOREIGN KEY ("ApplicationServiceId") REFERENCES ea."ApplicationService"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_ApplicationService_BusinessProcess"
    ADD CONSTRAINT "_ApplicationService_BusinessProcess_BusinessProcessId_fkey" FOREIGN KEY ("BusinessProcessId") REFERENCES ea."BusinessProcess"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_BusinessService_BusinessProcess"
    ADD CONSTRAINT "_BusinessService_BusinessProcess_BusinessProcessId_fkey" FOREIGN KEY ("BusinessProcessId") REFERENCES ea."BusinessProcess"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_BusinessService_BusinessProcess"
    ADD CONSTRAINT "_BusinessService_BusinessProcess_BusinessServiceId_fkey" FOREIGN KEY ("BusinessServiceId") REFERENCES ea."BusinessService"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_StrategicGoal_Capability"
    ADD CONSTRAINT "_StrategicGoal_Capability_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_StrategicGoal_Capability"
    ADD CONSTRAINT "_StrategicGoal_Capability_ModifiedByPersonId_fkey" FOREIGN KEY ("ModifiedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_StrategicGoal_Capability"
    ADD CONSTRAINT "_StrategicGoal_Capability_RelatedCapabilityId_fkey" FOREIGN KEY ("RelatedCapabilityId") REFERENCES ea."Capability"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_StrategicGoal_Capability"
    ADD CONSTRAINT "_StrategicGoal_Capability_RelatedStrategicGoalId_fkey" FOREIGN KEY ("RelatedStrategicGoalId") REFERENCES ea."StrategicGoal"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_StrategicOutcome_CourseOfAction"
    ADD CONSTRAINT "_StrategicOutcome_CourseOfAction_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_StrategicOutcome_CourseOfAction"
    ADD CONSTRAINT "_StrategicOutcome_CourseOfAction_ModifiedByPersonId_fkey" FOREIGN KEY ("ModifiedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_StrategicOutcome_CourseOfAction"
    ADD CONSTRAINT "_StrategicOutcome_CourseOfAction_RelatedCourseOfActionId_fke" FOREIGN KEY ("RelatedCourseOfActionId") REFERENCES ea."CourseOfAction"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ea."_StrategicOutcome_CourseOfAction"
    ADD CONSTRAINT "_StrategicOutcome_CourseOfAction_RelatedStrategicOutcomeId_f" FOREIGN KEY ("RelatedStrategicOutcomeId") REFERENCES ea."StrategicOutcome"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."Deliverable"
    ADD CONSTRAINT "Deliverable_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."Deliverable"
    ADD CONSTRAINT "Deliverable_ModifiedByPersonId_fkey" FOREIGN KEY ("ModifiedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."Deliverable"
    ADD CONSTRAINT "Deliverable_RelatedWorkPackageId_fkey" FOREIGN KEY ("RelatedWorkPackageId") REFERENCES pm."WorkPackage"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."ProjectIdea"
    ADD CONSTRAINT "ProjectIdea_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."ProjectIdea"
    ADD CONSTRAINT "ProjectIdea_id_fkey" FOREIGN KEY (id) REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."ProjectPlan"
    ADD CONSTRAINT "ProjectPlan_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."ProjectPlan"
    ADD CONSTRAINT "ProjectPlan_ModifiedByPersonId_fkey" FOREIGN KEY ("ModifiedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."ProjectPlan"
    ADD CONSTRAINT "ProjectPlan_RelatedProjectProposalId_fkey" FOREIGN KEY ("RelatedProjectProposalId") REFERENCES pm."ProjectProposal"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."ProjectProposal"
    ADD CONSTRAINT "ProjectProposal_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."ProjectProposal"
    ADD CONSTRAINT "ProjectProposal_ModifiedByPersonId_fkey" FOREIGN KEY ("ModifiedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."ProjectProposal"
    ADD CONSTRAINT "ProjectProposal_OwnerIsPersonId_fkey" FOREIGN KEY ("OwnerIsPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."ProjectProposal"
    ADD CONSTRAINT "ProjectProposal_RelatedProjectIdeaId_fkey" FOREIGN KEY ("RelatedProjectIdeaId") REFERENCES pm."ProjectIdea"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."Project"
    ADD CONSTRAINT "Project_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."Project"
    ADD CONSTRAINT "Project_IsPartOfCourseOfActionId_fkey" FOREIGN KEY ("IsPartOfCourseOfActionId") REFERENCES ea."CourseOfAction"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."Project"
    ADD CONSTRAINT "Project_ModifiedByPersonId_fkey" FOREIGN KEY ("ModifiedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."Project"
    ADD CONSTRAINT "Project_OwnerIsPersonId_fkey" FOREIGN KEY ("OwnerIsPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."Project"
    ADD CONSTRAINT "Project_RelatedProjectPlanId_fkey" FOREIGN KEY ("RelatedProjectPlanId") REFERENCES pm."ProjectIdea"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."Project"
    ADD CONSTRAINT "Project_RelatedProjectProposalId_fkey" FOREIGN KEY ("RelatedProjectProposalId") REFERENCES pm."ProjectProposal"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."WorkPackage"
    ADD CONSTRAINT "WorkPackage_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."WorkPackage"
    ADD CONSTRAINT "WorkPackage_ModifiedByPersonId_fkey" FOREIGN KEY ("ModifiedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm."WorkPackage"
    ADD CONSTRAINT "WorkPackage_RelatedProjectId_fkey" FOREIGN KEY ("RelatedProjectId") REFERENCES pm."Project"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."RecruitmentProposalComment"
    ADD CONSTRAINT "RecruitmentProposalComment_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."RecruitmentProposalComment"
    ADD CONSTRAINT "RecruitmentProposalComment_RecruitmentProposalId_fkey" FOREIGN KEY ("RecruitmentProposalId") REFERENCES public."RecruitmentProposal"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."RecruitmentProposal"
    ADD CONSTRAINT "RecruitmentProposal_CreatedByPersonId_fkey" FOREIGN KEY ("CreatedByPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."RecruitmentProposal"
    ADD CONSTRAINT "RecruitmentProposal_OrganisationUnitId_fkey" FOREIGN KEY ("OrganisationUnitId") REFERENCES readonly."OrganisationUnit"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."RecruitmentProposal"
    ADD CONSTRAINT "RecruitmentProposal_RelatedProjectId_fkey" FOREIGN KEY ("RelatedProjectId") REFERENCES readonly."Project"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."RecruitmentProposal"
    ADD CONSTRAINT "RecruitmentProposal_SupervisorPersonId_fkey" FOREIGN KEY ("SupervisorPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."RecruitmentProposal"
    ADD CONSTRAINT contract_type_fkey FOREIGN KEY ("ContractTypeId") REFERENCES readonly."ContractType"(id);
ALTER TABLE ONLY public."RecruitmentProposal"
    ADD CONSTRAINT performance_category_fkey FOREIGN KEY ("SuggestionForTheSalaryPerformanceCategoryId") REFERENCES readonly."PerformanceCategory"(id);
ALTER TABLE ONLY public."RecruitmentProposal"
    ADD CONSTRAINT recruitment_proposal_work_type_fkey FOREIGN KEY ("WorkTypeId") REFERENCES readonly."WorkType"(id);
ALTER TABLE ONLY public."RecruitmentProposal"
    ADD CONSTRAINT requirement_level_fkey FOREIGN KEY ("SuggestionForTheSalaryRequirementLevelId") REFERENCES readonly."RequirementLevel"(id);
ALTER TABLE ONLY readonly."PersonnelGroup"
    ADD CONSTRAINT "PersonnelGroup_OrganisationUnitId_fkey" FOREIGN KEY ("OrganisationUnitId") REFERENCES readonly."OrganisationUnit"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY readonly."Project"
    ADD CONSTRAINT "Project_OwnerOrganisationUnitId_fkey" FOREIGN KEY ("OwnerOrganisationUnitId") REFERENCES readonly."OrganisationUnit"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY readonly."Project"
    ADD CONSTRAINT "Project_OwnerPersonId_fkey" FOREIGN KEY ("OwnerPersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY readonly."_Person_OrganisationUnit"
    ADD CONSTRAINT "_Person_OrganisationUnit_OrganisationUnitId_fkey" FOREIGN KEY ("OrganisationUnitId") REFERENCES readonly."OrganisationUnit"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY readonly."_Person_OrganisationUnit"
    ADD CONSTRAINT "_Person_OrganisationUnit_PersonId_fkey" FOREIGN KEY ("PersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY readonly."_Person_PersonnelGroup"
    ADD CONSTRAINT "_Person_PersonnelGroup_PersonId_fkey" FOREIGN KEY ("PersonId") REFERENCES readonly."Person"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY readonly."_Person_PersonnelGroup"
    ADD CONSTRAINT "_Person_PersonnelGroup_PersonnelGroupId_fkey" FOREIGN KEY ("PersonnelGroupId") REFERENCES readonly."PersonnelGroup"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY readonly."OrganisationUnit"
    ADD CONSTRAINT organisation_unit_parent_organisation_unit_fkey FOREIGN KEY ("ParentOrganisationUnitId") REFERENCES readonly."OrganisationUnit"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY readonly."SalaryTable"
    ADD CONSTRAINT performance_category_fkey FOREIGN KEY ("PerformanceCategoryId") REFERENCES readonly."PerformanceCategory"(id);
ALTER TABLE ONLY readonly."SalaryTable"
    ADD CONSTRAINT requirement_level_fkey FOREIGN KEY ("RequirementLevelId") REFERENCES readonly."RequirementLevel"(id);
--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: ContractType; Type: TABLE DATA; Schema: readonly; Owner: postgres
--

INSERT INTO readonly."ContractType" VALUES ('new', 'New contract');
INSERT INTO readonly."ContractType" VALUES ('ongoing', 'On-going contract');


--
-- Data for Name: PerformanceCategory; Type: TABLE DATA; Schema: readonly; Owner: postgres
--

INSERT INTO readonly."PerformanceCategory" VALUES ('other1', 'Other personnel I');
INSERT INTO readonly."PerformanceCategory" VALUES ('other2', 'Other personnel II');
INSERT INTO readonly."PerformanceCategory" VALUES ('other3', 'Other personnel III');
INSERT INTO readonly."PerformanceCategory" VALUES ('other4', 'Other personnel IV');
INSERT INTO readonly."PerformanceCategory" VALUES ('edu1', 'Teaching and research personnel I');
INSERT INTO readonly."PerformanceCategory" VALUES ('edu2', 'Teaching and research personnel II');
INSERT INTO readonly."PerformanceCategory" VALUES ('edu3', 'Teaching and research personnel III');
INSERT INTO readonly."PerformanceCategory" VALUES ('edu4', 'Teaching and research personnel IV');


--
-- Data for Name: RequirementLevel; Type: TABLE DATA; Schema: readonly; Owner: postgres
--

INSERT INTO readonly."RequirementLevel" VALUES ('other2', 'Other personnel 2');
INSERT INTO readonly."RequirementLevel" VALUES ('other3', 'Other personnel 3');
INSERT INTO readonly."RequirementLevel" VALUES ('other4', 'Other personnel 4');
INSERT INTO readonly."RequirementLevel" VALUES ('other5', 'Other personnel 5');
INSERT INTO readonly."RequirementLevel" VALUES ('other6', 'Other personnel 6');
INSERT INTO readonly."RequirementLevel" VALUES ('other7', 'Other personnel 7');
INSERT INTO readonly."RequirementLevel" VALUES ('other8', 'Other personnel 8');
INSERT INTO readonly."RequirementLevel" VALUES ('other9', 'Other personnel 9');
INSERT INTO readonly."RequirementLevel" VALUES ('other10', 'Other personnel 10');
INSERT INTO readonly."RequirementLevel" VALUES ('other11', 'Other personnel 11');
INSERT INTO readonly."RequirementLevel" VALUES ('other12', 'Other personnel 12');
INSERT INTO readonly."RequirementLevel" VALUES ('other13', 'Other personnel 13');
INSERT INTO readonly."RequirementLevel" VALUES ('other14', 'Other personnel 14');
INSERT INTO readonly."RequirementLevel" VALUES ('other15', 'Other personnel 15');
INSERT INTO readonly."RequirementLevel" VALUES ('edu1', 'Teaching and research personnel 1');
INSERT INTO readonly."RequirementLevel" VALUES ('edu2', 'Teaching and research personnel 2');
INSERT INTO readonly."RequirementLevel" VALUES ('edu3', 'Teaching and research personnel 3');
INSERT INTO readonly."RequirementLevel" VALUES ('edu4', 'Teaching and research personnel 4');
INSERT INTO readonly."RequirementLevel" VALUES ('edu5', 'Teaching and research personnel 5');
INSERT INTO readonly."RequirementLevel" VALUES ('edu6', 'Teaching and research personnel 6');
INSERT INTO readonly."RequirementLevel" VALUES ('edu7', 'Teaching and research personnel 7');
INSERT INTO readonly."RequirementLevel" VALUES ('edu8', 'Teaching and research personnel 8');
INSERT INTO readonly."RequirementLevel" VALUES ('edu9', 'Teaching and research personnel 9');
INSERT INTO readonly."RequirementLevel" VALUES ('edu10', 'Teaching and research personnel 10');
INSERT INTO readonly."RequirementLevel" VALUES ('edu11', 'Teaching and research personnel 11');


--
-- Data for Name: SalaryTable; Type: TABLE DATA; Schema: readonly; Owner: postgres
--

INSERT INTO readonly."SalaryTable" VALUES ('edu1-edu1', 'Teaching and research personnel 1 - 1', 'edu1', 'edu1', 1938.3900000000001, 2176.11999999999989);
INSERT INTO readonly."SalaryTable" VALUES ('edu1-edu2', 'Teaching and research personnel 1 - 2', 'edu1', 'edu2', 2176.13000000000011, 2395.55999999999995);
INSERT INTO readonly."SalaryTable" VALUES ('edu1-edu3', 'Teaching and research personnel 1 - 3', 'edu1', 'edu3', 2395.57000000000016, 2596.71000000000004);
INSERT INTO readonly."SalaryTable" VALUES ('edu1-edu4', 'Teaching and research personnel 1 - 4', 'edu1', 'edu4', 2596.7199999999998, 2743.01000000000022);
INSERT INTO readonly."SalaryTable" VALUES ('edu2-edu1', 'Teaching and research personnel 2 - 1', 'edu2', 'edu1', 2131.84000000000015, 2393.28999999999996);
INSERT INTO readonly."SalaryTable" VALUES ('edu2-edu2', 'Teaching and research personnel 2 - 2', 'edu2', 'edu2', 2393.30000000000018, 2634.63000000000011);
INSERT INTO readonly."SalaryTable" VALUES ('edu2-edu3', 'Teaching and research personnel 2 - 3', 'edu2', 'edu3', 2634.63999999999987, 2855.86000000000013);
INSERT INTO readonly."SalaryTable" VALUES ('edu2-edu4', 'Teaching and research personnel 2 - 4', 'edu2', 'edu4', 2855.86999999999989, 3016.76000000000022);
INSERT INTO readonly."SalaryTable" VALUES ('edu3-edu1', 'Teaching and research personnel 3 - 1', 'edu3', 'edu1', 2341.7199999999998, 2628.90999999999985);
INSERT INTO readonly."SalaryTable" VALUES ('edu3-edu2', 'Teaching and research personnel 3 - 2', 'edu3', 'edu2', 2628.92000000000007, 2894.01000000000022);
INSERT INTO readonly."SalaryTable" VALUES ('edu3-edu3', 'Teaching and research personnel 3 - 3', 'edu3', 'edu3', 2894.01999999999998, 3137.01999999999998);
INSERT INTO readonly."SalaryTable" VALUES ('edu3-edu4', 'Teaching and research personnel 3 - 4', 'edu3', 'edu4', 3137.0300000000002, 3313.76000000000022);
INSERT INTO readonly."SalaryTable" VALUES ('edu4-edu1', 'Teaching and research personnel 4 - 1', 'edu4', 'edu1', 2658.07999999999993, 2984.07000000000016);
INSERT INTO readonly."SalaryTable" VALUES ('edu4-edu2', 'Teaching and research personnel 4 - 2', 'edu4', 'edu2', 2984.07999999999993, 3284.98000000000002);
INSERT INTO readonly."SalaryTable" VALUES ('edu4-edu3', 'Teaching and research personnel 4 - 3', 'edu4', 'edu3', 3284.98999999999978, 3560.82000000000016);
INSERT INTO readonly."SalaryTable" VALUES ('edu4-edu4', 'Teaching and research personnel 4 - 4', 'edu4', 'edu4', 3560.82999999999993, 3761.42999999999984);
INSERT INTO readonly."SalaryTable" VALUES ('edu5-edu1', 'Teaching and research personnel 5 - 1', 'edu5', 'edu1', 3077.82999999999993, 3455.30000000000018);
INSERT INTO readonly."SalaryTable" VALUES ('edu5-edu2', 'Teaching and research personnel 5 - 2', 'edu5', 'edu2', 3455.30999999999995, 3803.73000000000002);
INSERT INTO readonly."SalaryTable" VALUES ('edu5-edu3', 'Teaching and research personnel 5 - 3', 'edu5', 'edu3', 3803.73999999999978, 4123.13000000000011);
INSERT INTO readonly."SalaryTable" VALUES ('edu5-edu4', 'Teaching and research personnel 5 - 4', 'edu5', 'edu4', 4123.14000000000033, 4355.42000000000007);
INSERT INTO readonly."SalaryTable" VALUES ('edu6-edu1', 'Teaching and research personnel 6 - 1', 'edu6', 'edu1', 3589.15999999999985, 4029.34000000000015);
INSERT INTO readonly."SalaryTable" VALUES ('edu6-edu2', 'Teaching and research personnel 6 - 2', 'edu6', 'edu2', 4029.34999999999991, 4435.65999999999985);
INSERT INTO readonly."SalaryTable" VALUES ('edu6-edu3', 'Teaching and research personnel 6 - 3', 'edu6', 'edu3', 4435.67000000000007, 4808.11999999999989);
INSERT INTO readonly."SalaryTable" VALUES ('edu6-edu4', 'Teaching and research personnel 6 - 4', 'edu6', 'edu4', 4808.13000000000011, 5079);
INSERT INTO readonly."SalaryTable" VALUES ('edu7-edu1', 'Teaching and research personnel 7 - 1', 'edu7', 'edu1', 4137.77000000000044, 4645.23999999999978);
INSERT INTO readonly."SalaryTable" VALUES ('edu7-edu2', 'Teaching and research personnel 7 - 2', 'edu7', 'edu2', 4645.25, 5113.65999999999985);
INSERT INTO readonly."SalaryTable" VALUES ('edu7-edu3', 'Teaching and research personnel 7 - 3', 'edu7', 'edu3', 5113.67000000000007, 5543.0600000000004);
INSERT INTO readonly."SalaryTable" VALUES ('edu7-edu4', 'Teaching and research personnel 7 - 4', 'edu7', 'edu4', 5543.06999999999971, 5855.34000000000015);
INSERT INTO readonly."SalaryTable" VALUES ('edu8-edu1', 'Teaching and research personnel 8 - 1', 'edu8', 'edu1', 5002.96000000000004, 5616.52999999999975);
INSERT INTO readonly."SalaryTable" VALUES ('edu8-edu2', 'Teaching and research personnel 8 - 2', 'edu8', 'edu2', 5616.53999999999996, 6182.89999999999964);
INSERT INTO readonly."SalaryTable" VALUES ('edu8-edu3', 'Teaching and research personnel 8 - 3', 'edu8', 'edu3', 6182.90999999999985, 6702.06999999999971);
INSERT INTO readonly."SalaryTable" VALUES ('edu8-edu4', 'Teaching and research personnel 8 - 4', 'edu8', 'edu4', 6702.07999999999993, 7079.65999999999985);
INSERT INTO readonly."SalaryTable" VALUES ('edu9-edu1', 'Teaching and research personnel 9 - 1', 'edu9', 'edu1', 5635.06999999999971, 6326.15999999999985);
INSERT INTO readonly."SalaryTable" VALUES ('edu9-edu2', 'Teaching and research personnel 9 - 2', 'edu9', 'edu2', 6326.17000000000007, 6964.09000000000015);
INSERT INTO readonly."SalaryTable" VALUES ('edu9-edu3', 'Teaching and research personnel 9 - 3', 'edu9', 'edu3', 6964.10000000000036, 7548.85999999999967);
INSERT INTO readonly."SalaryTable" VALUES ('edu9-edu4', 'Teaching and research personnel 9 - 4', 'edu9', 'edu4', 7548.86999999999989, 7974.14999999999964);
INSERT INTO readonly."SalaryTable" VALUES ('edu10-edu1', 'Teaching and research personnel 10 - 1', 'edu10', 'edu1', 6377.80000000000018, 7159.97999999999956);
INSERT INTO readonly."SalaryTable" VALUES ('edu10-edu2', 'Teaching and research personnel 10 - 2', 'edu10', 'edu2', 7159.98999999999978, 7881.98999999999978);
INSERT INTO readonly."SalaryTable" VALUES ('edu10-edu3', 'Teaching and research personnel 10 - 3', 'edu10', 'edu3', 7882, 8543.84000000000015);
INSERT INTO readonly."SalaryTable" VALUES ('edu10-edu4', 'Teaching and research personnel 10 - 4', 'edu10', 'edu4', 8543.85000000000036, 9025.19000000000051);
INSERT INTO readonly."SalaryTable" VALUES ('edu11-edu1', 'Teaching and research personnel 11 - 1', 'edu11', 'edu1', 7372.15999999999985, 8276.29999999999927);
INSERT INTO readonly."SalaryTable" VALUES ('edu11-edu2', 'Teaching and research personnel 11 - 2', 'edu11', 'edu2', 8276.30999999999949, 9110.8799999999992);
INSERT INTO readonly."SalaryTable" VALUES ('edu11-edu3', 'Teaching and research personnel 11 - 3', 'edu11', 'edu3', 9110.88999999999942, 9875.92000000000007);
INSERT INTO readonly."SalaryTable" VALUES ('edu11-edu4', 'Teaching and research personnel 11 - 4', 'edu11', 'edu4', 9875.93000000000029, 10432.3099999999995);
INSERT INTO readonly."SalaryTable" VALUES ('other2-other1', 'Other personnel 2 - 1', 'other2', 'other1', 1722.24000000000001, 1933.46000000000004);
INSERT INTO readonly."SalaryTable" VALUES ('other2-other2', 'Other personnel 2 - 2', 'other2', 'other2', 1933.47000000000003, 2128.42999999999984);
INSERT INTO readonly."SalaryTable" VALUES ('other2-other3', 'Other personnel 2 - 3', 'other2', 'other3', 2128.44000000000005, 2307.15000000000009);
INSERT INTO readonly."SalaryTable" VALUES ('other2-other4', 'Other personnel 2 - 4', 'other2', 'other4', 2307.15999999999985, 2437.13000000000011);
INSERT INTO readonly."SalaryTable" VALUES ('other3-other1', 'Other personnel 3 - 1', 'other3', 'other1', 1871.40000000000009, 2100.90999999999985);
INSERT INTO readonly."SalaryTable" VALUES ('other3-other2', 'Other personnel 3 - 2', 'other3', 'other2', 2100.92000000000007, 2312.76999999999998);
INSERT INTO readonly."SalaryTable" VALUES ('other3-other3', 'Other personnel 3 - 3', 'other3', 'other3', 2312.7800000000002, 2506.9699999999998);
INSERT INTO readonly."SalaryTable" VALUES ('other3-other4', 'Other personnel 3 - 4', 'other3', 'other4', 2506.98000000000002, 2648.21000000000004);
INSERT INTO readonly."SalaryTable" VALUES ('other4-other1', 'Other personnel 4 - 1', 'other4', 'other1', 1967.84999999999991, 2209.19000000000005);
INSERT INTO readonly."SalaryTable" VALUES ('other4-other2', 'Other personnel 4 - 2', 'other4', 'other2', 2209.19999999999982, 2431.96000000000004);
INSERT INTO readonly."SalaryTable" VALUES ('other4-other3', 'Other personnel 4 - 3', 'other4', 'other3', 2431.9699999999998, 2636.17000000000007);
INSERT INTO readonly."SalaryTable" VALUES ('other4-other4', 'Other personnel 4 - 4', 'other4', 'other4', 2636.17999999999984, 2784.69000000000005);
INSERT INTO readonly."SalaryTable" VALUES ('other5-other1', 'Other personnel 5 - 1', 'other5', 'other1', 2075.28999999999996, 2329.80999999999995);
INSERT INTO readonly."SalaryTable" VALUES ('other5-other2', 'Other personnel 5 - 2', 'other5', 'other2', 2329.82000000000016, 2564.73999999999978);
INSERT INTO readonly."SalaryTable" VALUES ('other5-other3', 'Other personnel 5 - 3', 'other5', 'other3', 2564.75, 2780.09999999999991);
INSERT INTO readonly."SalaryTable" VALUES ('other5-other4', 'Other personnel 5 - 4', 'other5', 'other4', 2780.11000000000013, 2936.73000000000002);
INSERT INTO readonly."SalaryTable" VALUES ('other6-other1', 'Other personnel 6 - 1', 'other6', 'other1', 2193.36999999999989, 2462.36999999999989);
INSERT INTO readonly."SalaryTable" VALUES ('other6-other2', 'Other personnel 6 - 2', 'other6', 'other2', 2462.38000000000011, 2710.67999999999984);
INSERT INTO readonly."SalaryTable" VALUES ('other6-other3', 'Other personnel 6 - 3', 'other6', 'other3', 2710.69000000000005, 2938.28999999999996);
INSERT INTO readonly."SalaryTable" VALUES ('other6-other4', 'Other personnel 6 - 4', 'other6', 'other4', 2938.30000000000018, 3103.82999999999993);
INSERT INTO readonly."SalaryTable" VALUES ('other7-other1', 'Other personnel 7 - 1', 'other7', 'other1', 2364.69000000000005, 2654.69999999999982);
INSERT INTO readonly."SalaryTable" VALUES ('other7-other2', 'Other personnel 7 - 2', 'other7', 'other2', 2654.71000000000004, 2922.40000000000009);
INSERT INTO readonly."SalaryTable" VALUES ('other7-other3', 'Other personnel 7 - 3', 'other7', 'other3', 2922.40999999999985, 3167.78999999999996);
INSERT INTO readonly."SalaryTable" VALUES ('other7-other4', 'Other personnel 7 - 4', 'other7', 'other4', 3167.80000000000018, 3346.26000000000022);
INSERT INTO readonly."SalaryTable" VALUES ('other8-other1', 'Other personnel 8 - 1', 'other8', 'other1', 2669.17999999999984, 2996.5300000000002);
INSERT INTO readonly."SalaryTable" VALUES ('other8-other2', 'Other personnel 8 - 2', 'other8', 'other2', 2996.53999999999996, 3298.69999999999982);
INSERT INTO readonly."SalaryTable" VALUES ('other8-other3', 'Other personnel 8 - 3', 'other8', 'other3', 3298.71000000000004, 3575.69000000000005);
INSERT INTO readonly."SalaryTable" VALUES ('other8-other4', 'Other personnel 8 - 4', 'other8', 'other4', 3575.69999999999982, 3777.13999999999987);
INSERT INTO readonly."SalaryTable" VALUES ('other9-other1', 'Other personnel 9 - 1', 'other9', 'other1', 3076.86000000000013, 3454.21000000000004);
INSERT INTO readonly."SalaryTable" VALUES ('other9-other2', 'Other personnel 9 - 2', 'other9', 'other2', 3454.2199999999998, 3802.53999999999996);
INSERT INTO readonly."SalaryTable" VALUES ('other9-other3', 'Other personnel 9 - 3', 'other9', 'other3', 3802.55000000000018, 4121.82999999999993);
INSERT INTO readonly."SalaryTable" VALUES ('other9-other4', 'Other personnel 9 - 4', 'other9', 'other4', 4121.84000000000015, 4354.05000000000018);
INSERT INTO readonly."SalaryTable" VALUES ('other10-other1', 'Other personnel 10 - 1', 'other10', 'other1', 3549.26999999999998, 3984.55999999999995);
INSERT INTO readonly."SalaryTable" VALUES ('other10-other2', 'Other personnel 10 - 2', 'other10', 'other2', 3984.57000000000016, 4386.35999999999967);
INSERT INTO readonly."SalaryTable" VALUES ('other10-other3', 'Other personnel 10 - 3', 'other10', 'other3', 4386.36999999999989, 4754.6899999999996);
INSERT INTO readonly."SalaryTable" VALUES ('other10-other4', 'Other personnel 10 - 4', 'other10', 'other4', 4754.69999999999982, 5022.5600000000004);
INSERT INTO readonly."SalaryTable" VALUES ('other11-other1', 'Other personnel 11 - 1', 'other11', 'other1', 4048.30000000000018, 4544.78999999999996);
INSERT INTO readonly."SalaryTable" VALUES ('other11-other2', 'Other personnel 11 - 2', 'other11', 'other2', 4544.80000000000018, 5003.09000000000015);
INSERT INTO readonly."SalaryTable" VALUES ('other11-other3', 'Other personnel 11 - 3', 'other11', 'other3', 5003.10000000000036, 5423.1899999999996);
INSERT INTO readonly."SalaryTable" VALUES ('other11-other4', 'Other personnel 11 - 4', 'other11', 'other4', 5423.19999999999982, 5728.72999999999956);
INSERT INTO readonly."SalaryTable" VALUES ('other12-other1', 'Other personnel 12 - 1', 'other12', 'other1', 4585.15999999999985, 5147.48999999999978);
INSERT INTO readonly."SalaryTable" VALUES ('other12-other2', 'Other personnel 12 - 2', 'other12', 'other2', 5147.5, 5666.5600000000004);
INSERT INTO readonly."SalaryTable" VALUES ('other12-other3', 'Other personnel 12 - 3', 'other12', 'other3', 5666.56999999999971, 6142.38000000000011);
INSERT INTO readonly."SalaryTable" VALUES ('other12-other4', 'Other personnel 12 - 4', 'other12', 'other4', 6142.39000000000033, 6488.43000000000029);
INSERT INTO readonly."SalaryTable" VALUES ('other13-other1', 'Other personnel 13 - 1', 'other13', 'other1', 5184.90999999999985, 5820.78999999999996);
INSERT INTO readonly."SalaryTable" VALUES ('other13-other2', 'Other personnel 13 - 2', 'other13', 'other2', 5820.80000000000018, 6407.76000000000022);
INSERT INTO readonly."SalaryTable" VALUES ('other13-other3', 'Other personnel 13 - 3', 'other13', 'other3', 6407.77000000000044, 6945.81999999999971);
INSERT INTO readonly."SalaryTable" VALUES ('other13-other4', 'Other personnel 13 - 4', 'other13', 'other4', 6945.82999999999993, 7337.13000000000011);
INSERT INTO readonly."SalaryTable" VALUES ('other14-other1', 'Other personnel 14 - 1', 'other14', 'other1', 5905.72999999999956, 6630.01000000000022);
INSERT INTO readonly."SalaryTable" VALUES ('other14-other2', 'Other personnel 14 - 2', 'other14', 'other2', 6630.02000000000044, 7298.59000000000015);
INSERT INTO readonly."SalaryTable" VALUES ('other14-other3', 'Other personnel 14 - 3', 'other14', 'other3', 7298.60000000000036, 7911.4399999999996);
INSERT INTO readonly."SalaryTable" VALUES ('other14-other4', 'Other personnel 14 - 4', 'other14', 'other4', 7911.44999999999982, 8357.15999999999985);
INSERT INTO readonly."SalaryTable" VALUES ('other15-other1', 'Other personnel 15 - 1', 'other15', 'other1', 6601.78999999999996, 7411.4399999999996);
INSERT INTO readonly."SalaryTable" VALUES ('other15-other2', 'Other personnel 15 - 2', 'other15', 'other2', 7411.44999999999982, 8158.8100000000004);
INSERT INTO readonly."SalaryTable" VALUES ('other15-other3', 'Other personnel 15 - 3', 'other15', 'other3', 8158.81999999999971, 8843.89999999999964);
INSERT INTO readonly."SalaryTable" VALUES ('other15-other4', 'Other personnel 15 - 4', 'other15', 'other4', 8843.90999999999985, 9342.14999999999964);


--
-- Data for Name: WorkType; Type: TABLE DATA; Schema: readonly; Owner: postgres
--

INSERT INTO readonly."WorkType" VALUES ('full_time', 'Full-time work');
INSERT INTO readonly."WorkType" VALUES ('hourly', 'Hourly work');
INSERT INTO readonly."WorkType" VALUES ('part_time', 'Part-time work');


--
-- PostgreSQL database dump complete
--

