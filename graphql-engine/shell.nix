{ pkgs ? import (builtins.fetchTarball {
    # branches nixos-20.03
    url = "https://github.com/NixOS/nixpkgs-channels/archive/99a3d7a86fce9e9c9f23b3e304d7d2b1270a12b8.tar.gz";
    sha256 = "0i40cl3n6600z2lkwrpiy28dcnv2r63fcgfswj91aaf1xfn2chql";
  }) {}
}:

with pkgs;

let
  hasura-cli = stdenv.mkDerivation rec {
    name = "cli-hasura-${version}-${system}";
    version = "v1.2.0-beta.2";
    system = "linux-amd64";
    src = fetchurl {
      url = "https://github.com/hasura/graphql-engine/releases/download/${version}/cli-hasura-${system}";
      sha256 = "1nv1n0svznnl5rb234sbx5kxhffjkr3bgx1jgdmiqwwiqbbcvbab";
    };
    builder = builtins.toFile "builder.sh" ''
      source $stdenv/setup;
      mkdir -p $out/bin;
      cp $src $out/bin/hasura
      chmod ugo+x $out/bin/hasura
    '';
    buildInputs = with pkgs; [
      curl
    ];
  };

in mkShell {
  buildInputs = [
    hasura-cli
  ];
}
