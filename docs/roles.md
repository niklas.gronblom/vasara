App roles
=========

The app will support three technical user roles:

* **designer** will be able to create and update processes

* **user** will be able to start processes and perform user tasks

* **admin** will be able to look and manage stored data

Even admins may not be able to view all data, but visible data will be filtered with Hasura access rules.
