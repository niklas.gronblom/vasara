Getting started
===============

* Add `/etc/hosts` alias for `localhost` as `keycloak` (for OIDC login)
* `docker-compose up` (may take a while)
* `cp vasara-app/.env.example vasara-app/.env`
* `cd vasara-app && yarn && yarn start` (with credentials camunda-admin:admin)

Exposed services
================

* Keycloack at  http://localhost:8080/ (with admin:admin)
* Camunda at  http://localhost:8800/ (with camunda-admin:admin)
* Mailhog at http://localhost:8025/
* Hasura at http://localhost:8900/
* Node RED at http://localhost:1880/
* React-Admin at http://localhost:3000/

Drafting schemata with Hasura
=============================

The root of the project contains a `Makefile` to help drafting custom schemata with Hasura.

Set up the environment with:
```bash
$ make
```

Once everything is up and running, Hasura is available at http://lcoalhost:8900/

Drafted schemata can be exported into filesystem schema by schema, e.g. for `public`, by:
```bash
$ make export SCHEMA=public
```

Drafts are exported into folder `./drafts`.

Whenever required, the environment can be reset with:
```bash
$ make purge
```

This will remove all local changes into schemata and the environment will be reset back to defaults.

Resetting the environment
=========================

```bash
$ docker-compose stop
$ docker-compose rm -f
$ docker volume rm vasara_db_data
```

Troubleshooting
===============

Camunda GraphQL resources are missing and Hasura displays error message: **You have been redirected because your GraphQL Engine metadata is in an inconsistent state**

* Reason: Hasura fails to refresh GraphQL introspection data from Camunda after all containers have been restarted, because of Hasura starts before Camunda GraphQL. Unfortunately, "Reload metadata"-button seem tobe unable to refresh the metadata.

* Solution A: Restart graphql-engine:

  ```bash
  docker-compose restart graphql-engine
  ```

* Solution B: Re-configure Camunda remote schema:

  1. Click **Delete all** at the bottom of the error page
  1. Click **Remote schemas** at the top toolbar
  2. Click **Add** and enter:
  3. Remote Schema name: **Camunda**
  4. GraphQL Server URL: **http://camunda:8080/graphql**
  5. **Check** Forward all headers from client
  6. Additional headers: **X-Hasura-Camunda-Secret**, From env var: **HASURA_CAMUNDA_SECRET**
  7. GraphQL server timeout: **60**
  8. Click **Add Remote Schema**
